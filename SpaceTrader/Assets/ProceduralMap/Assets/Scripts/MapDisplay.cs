﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapDisplay : MonoBehaviour {

	public Renderer textureRender;
	public MeshFilter meshFilter;
	public MeshRenderer meshRenderer;
    public ParticleSystem particlSystem;
    public GameObject windArrow;

    public float gizmoScale = 5;
    public bool flipX;
    public bool flipY;
    private GameObject[,] windArrows;



	public void DrawTexture(Texture2D texture) {
		textureRender.sharedMaterial.mainTexture = texture;
		textureRender.transform.localScale = new Vector3 (texture.width, 1, texture.height);
	}

	public void DrawMesh(MeshData meshData, Texture2D texture) {
		meshFilter.sharedMesh = meshData.CreateMesh ();
		meshRenderer.sharedMaterial.mainTexture = texture;
	}

    public void DrawPercipitation(float[,] levelMap, float[,] lowPercipitationMap, float[,] highPercipitationMap, int mapWidth, int mapHeight, float mapDepth)
    {
        List<ParticleSystem.Particle> points = new List<ParticleSystem.Particle>();

        if (lowPercipitationMap != null)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {

                    float lPerc = lowPercipitationMap[x, y];
                    float hPerc = highPercipitationMap[x, y];

                    if (lPerc > .05f)
                    {
                        if (lPerc > 20)
                            lPerc = 20;
                        float cX = 0;
                        float cY = 0;
                        if (flipX)
                        {
                            cX = -x + mapWidth * .5f;
                        }
                        else
                        {
                            cX = x - mapWidth * .5f;
                        }

                        if (flipY)
                        {
                            cY = -y + mapWidth * .5f;
                        }
                        else
                        {
                            cY = y - mapWidth * .5f;
                        }
                        ParticleSystem.Particle particle = new ParticleSystem.Particle();
                        particle.startColor = Color.Lerp(Color.white, Color.gray, lPerc / 20);
                        particle.startSize = lPerc * 10;
                        particle.position = (new Vector3(cX, levelMap[x,y], cY)) * gizmoScale;
                        points.Add(particle);
                    }

                    if (hPerc > .05f)
                    {
                        if (hPerc > 20)
                            hPerc = 20;
                        float cX = 0;
                        float cY = 0;
                        if (flipX)
                        {
                            cX = -x + mapWidth * .5f;
                        }
                        else
                        {
                            cX = x - mapWidth * .5f;
                        }

                        if (flipY)
                        {
                            cY = -y + mapWidth * .5f;
                        }
                        else
                        {
                            cY = y - mapWidth * .5f;
                        }
                        ParticleSystem.Particle particle = new ParticleSystem.Particle();
                        particle.startColor = Color.Lerp(Color.white, Color.gray, hPerc / 20);
                        particle.startSize = hPerc * 10;
                        particle.position = (new Vector3(cX, mapDepth, cY)) * gizmoScale;
                        points.Add(particle);
                    }
                }
            }

            particlSystem.SetParticles(points.ToArray(), points.Count);
        }


    }

    public void DrawCondensation(float[,] levelMap, float[,] lowPercipitationMap, float[,] highPercipitationMap, float[,] lowTemperatureMap, float[,] highTemperatureMap, int mapHeight, int mapWidth, float mapDepth)
    {
        List<ParticleSystem.Particle> points = new List<ParticleSystem.Particle>();

        if (lowPercipitationMap != null)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {

                    float lPerc = lowPercipitationMap[x, y];
                    float hPerc = highPercipitationMap[x, y];

                    if (lPerc < 0)
                    {
                        float cX = 0;
                        float cY = 0;
                        if (flipX)
                        {
                            cX = -x + mapWidth * .5f;
                        }
                        else
                        {
                            cX = x - mapWidth * .5f;
                        }

                        if (flipY)
                        {
                            cY = -y + mapWidth * .5f;
                        }
                        else
                        {
                            cY = y - mapWidth * .5f;
                        }
                        ParticleSystem.Particle particle = new ParticleSystem.Particle();
                        particle.startColor = Color.blue;
                        particle.startSize = 10;
                        particle.position = (new Vector3(cX, levelMap[x, y], cY)) * gizmoScale;
                        points.Add(particle);
                        continue;
                    }
                    float currentTemp = lowTemperatureMap[x,y];

                    if (currentTemp <= 0)
                    {
                        currentTemp = .00001f;
                    }
                    float cond = lPerc / currentTemp;

                    if (cond > .05f)
                    {
                        if (cond > 1)
                            cond = 1;
                        float cX = 0;
                        float cY = 0;
                        if (flipX)
                        {
                            cX = -x + mapWidth * .5f;
                        }
                        else
                        {
                            cX = x - mapWidth * .5f;
                        }

                        if (flipY)
                        {
                            cY = -y + mapWidth * .5f;
                        }
                        else
                        {
                            cY = y - mapWidth * .5f;
                        }
                        ParticleSystem.Particle particle = new ParticleSystem.Particle();
                        particle.startColor = Color.Lerp(Color.white, Color.gray, cond);
                        particle.startSize = cond * 50;
                        particle.position = (new Vector3(cX, levelMap[x, y], cY)) * gizmoScale;
                        points.Add(particle);
                    }

                    if (hPerc < 0)
                    {
                        float cX = 0;
                        float cY = 0;
                        if (flipX)
                        {
                            cX = -x + mapWidth * .5f;
                        }
                        else
                        {
                            cX = x - mapWidth * .5f;
                        }

                        if (flipY)
                        {
                            cY = -y + mapWidth * .5f;
                        }
                        else
                        {
                            cY = y - mapWidth * .5f;
                        }
                        ParticleSystem.Particle particle = new ParticleSystem.Particle();
                        particle.startColor = Color.blue;
                        particle.startSize = 10;
                        particle.position = (new Vector3(cX, mapDepth, cY)) * gizmoScale;
                        points.Add(particle);
                        continue;
                    }
                    currentTemp = highTemperatureMap[x,y];

                    if (currentTemp <= 0)
                    {
                        currentTemp = .00001f;
                    }
                    cond = hPerc / currentTemp;

                    if (cond > .05f)
                    {
                        if (cond > 1)
                            cond = 1;
                        float cX = 0;
                        float cY = 0;
                        if (flipX)
                        {
                            cX = -x + mapWidth * .5f;
                        }
                        else
                        {
                            cX = x - mapWidth * .5f;
                        }

                        if (flipY)
                        {
                            cY = -y + mapWidth * .5f;
                        }
                        else
                        {
                            cY = y - mapWidth * .5f;
                        }
                        ParticleSystem.Particle particle = new ParticleSystem.Particle();
                        particle.startColor = Color.Lerp(Color.white, Color.gray, cond);
                        particle.startSize = cond * 50;
                        particle.position = (new Vector3(cX, mapDepth, cY)) * gizmoScale;
                        points.Add(particle);
                    }
                }
            }

            particlSystem.SetParticles(points.ToArray(), points.Count);
        }


    }

    public void DrawTemp(float[,] levelMap, float[,] lowTempMap, float[,] highTempMap, int mapHeight, int mapWidth, int mapDepth)
    {
        List<ParticleSystem.Particle> points = new List<ParticleSystem.Particle>();
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {

                float cX = 0;
                float cY = 0;
                if (flipX)
                {
                    cX = -x + mapWidth * .5f;
                }
                else
                {
                    cX = x - mapWidth * .5f;
                }

                if (flipY)
                {
                    cY = -y + mapWidth * .5f;
                }
                else
                {
                    cY = y - mapWidth * .5f;
                }
                float z = levelMap[x, y];
                float currentTemp = lowTempMap[x, y];
                ParticleSystem.Particle particle = new ParticleSystem.Particle();
                particle.startColor = Color.Lerp(Color.blue, Color.red, currentTemp / 100);
                particle.startSize = 10;
                particle.position = (new Vector3(cX, z, cY)) * gizmoScale;
                points.Add(particle);

                z = mapDepth;
                currentTemp = highTempMap[x, y];
                particle = new ParticleSystem.Particle();
                particle.startColor = Color.Lerp(Color.blue, Color.red, currentTemp / 100);
                particle.startSize = 10;
                particle.position = (new Vector3(cX, z, cY)) * gizmoScale;
                points.Add(particle);
            }
        }
        particlSystem.SetParticles(points.ToArray(), points.Count);
    }

    public void DrawPressure(float[,] levelMap, float[,] lowPressureMap, float[,] highPressureMap, int mapHeight, int mapWidth, float mapDepth)
    {
        List<ParticleSystem.Particle> points = new List<ParticleSystem.Particle>();

        if (lowPressureMap != null)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {

                    float lPress = lowPressureMap[x, y];
                    float hPress = highPressureMap[x, y];

                    if (lPress > .05f)
                    {
                        float cX = 0;
                        float cY = 0;
                        if (flipX)
                        {
                            cX = -x + mapWidth * .5f;
                        }
                        else
                        {
                            cX = x - mapWidth * .5f;
                        }

                        if (flipY)
                        {
                            cY = -y + mapWidth * .5f;
                        }
                        else
                        {
                            cY = y - mapWidth * .5f;
                        }
                        ParticleSystem.Particle particle = new ParticleSystem.Particle();
                        particle.startColor = Color.Lerp(Color.blue, Color.red, lPress / 200);
                        particle.startSize = lPress;
                        particle.position = (new Vector3(cX, levelMap[x, y], cY)) * gizmoScale;
                        points.Add(particle);
                    }

                    if (hPress > .05f)
                    {
                        float cX = 0;
                        float cY = 0;
                        if (flipX)
                        {
                            cX = -x + mapWidth * .5f;
                        }
                        else
                        {
                            cX = x - mapWidth * .5f;
                        }

                        if (flipY)
                        {
                            cY = -y + mapWidth * .5f;
                        }
                        else
                        {
                            cY = y - mapWidth * .5f;
                        }
                        ParticleSystem.Particle particle = new ParticleSystem.Particle();
                        particle.startColor = Color.Lerp(Color.blue, Color.red, hPress / 200);
                        particle.startSize = hPress;
                        particle.position = (new Vector3(cX, mapDepth, cY)) * gizmoScale;
                        points.Add(particle);
                    }
                }
            }

            particlSystem.SetParticles(points.ToArray(), points.Count);
        }


    }

    public void DrawWind(float[,] levelMap, float[,] lowPressureMap, float[,] highPressureMap, int mapHeight, int mapWidth, float mapDepth)
    {
        bool startWind = false;
        if (windArrows == null)
        {
            windArrows = new GameObject[mapWidth, mapHeight];
            startWind = true;
        }
           

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {

                //Wind lWind = lowWindMap[x, y];
                float hPress = highPressureMap[x, y];
                float lPress = lowPressureMap[x, y];
                float cX = 0;
                float cY = 0;
                if (flipX)
                {
                    cX = -x + mapWidth * .5f;
                }
                else
                {
                    cX = x - mapWidth * .5f;
                }

                if (flipY)
                {
                    cY = -y + mapWidth * .5f;
                }
                else
                {
                    cY = y - mapWidth * .5f;
                }
                

                GameObject newWindArrow = null;

                //if (startWind)
                //{
                //    newWindArrow = Instantiate(windArrow);
                    
                //}
                //else
                //{
                //    newWindArrow = windArrows[x,y];
                //}
                //if (hPress.Speed > 0)
                //{
                //    float hDirection = Mathf.Deg2Rad * hPress.Direction;
                //    Vector3 velocity = new Vector3(hPress.Speed * Mathf.Cos(hDirection), 0, hPress.Speed * Mathf.Sin(hDirection));
                //    newWindArrow.transform.rotation = Quaternion.LookRotation(velocity);
                //    newWindArrow.transform.localScale = Vector3.one * hPress.Speed * 10;
                //}
                //else
                //{
                //    newWindArrow.transform.localScale = Vector3.one * .01f;
                //}
                
                //newWindArrow.transform.position = (new Vector3(cX, mapDepth, cY)) * gizmoScale;
                
                //windArrows[x, y] = newWindArrow;
            }
        }
    }

    internal void Reset(int mapHeight, int mapWidth)
    {
        if (windArrows != null)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    Destroy(windArrows[x, y]);
                }
            }

            windArrows = null;
        }
       
    }
}
