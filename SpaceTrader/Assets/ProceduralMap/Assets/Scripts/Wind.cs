﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Wind {
    private float speed;
    /// <summary>
    /// 0 to 360
    /// </summary>
    private float direction;
	
    /// <summary>
    /// In degrees
    /// </summary>
    public float Direction {
        get { return direction; }
        set { direction = value;
            while (direction < 0)
                direction += 360;
            while (direction >= 360)
                direction -= 360;
        } }

    public float Speed
    {
        get { return speed; }
        set
        {
            if (value < 0)
            {
                speed = -value;
                Direction += 180;
            }
            else
            {
                speed = value;
            }
            if (speed < .1f)
                speed = .1f;

        }
    }
}
