﻿using UnityEngine;
using System.Collections;

public class MapGenerator : MonoBehaviour {

	public enum DrawMode {NoiseMap, ColourMap, Mesh, Water, Perc, Cond, Temp, Wind, Pressure };
	public DrawMode drawMode;

	public int mapWidth;
	public int mapHeight;
    public float depthScale;
	public float noiseScale;

	public int octaves;
	[Range(0,1)]
	public float persistance;
	public float lacunarity;
    //[Range (0,50)]
    public int iterations;

    //For runtime genertation
    public float iterationSpeed;

    public int seed;
	public Vector2 offset;

	public bool autoUpdate;
    public float waterMoveSpeed;
    public float waterAlpha;
    public float percipitationSpeed;
    public float verticalPercipitationSpeed;
    public float temperatureScale;
    public float temperatureStart;
    public float temperatureOffset;

    public float pressureChangeSpeed;
    public float pressureConstant;
    public float temperaturePressureEffect;
    public float waterPressureEffect;
    public float percipitationPressureEffect;
    public float windEffect;

	public TerrainTypeInfo[] regions;


    private float[,] noiseMap;
    private float[,] waterMap;
    private float[,] lowPercipitationMap;
    private float[,] highPercipitationMap;
    private float[,] lowPressureMap;
    private float[,] highPressureMap;
    private float[,] lowTempMap;
    private float[,] highTempMap;
    private float mapDepth;


    public void GenerateMap() {
        //Reset();
		noiseMap = Noise.GenerateNoiseMap(mapWidth, mapHeight, seed, noiseScale, octaves, persistance, lacunarity, offset);
        System.Random prng = new System.Random (seed);
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                noiseMap[x, y] *= depthScale;
            }
        }
        lowPercipitationMap = new float[mapWidth, mapHeight];
        highPercipitationMap = new float[mapWidth, mapHeight];
        highPressureMap = new float[mapWidth, mapHeight];
        lowPressureMap = new float[mapWidth, mapHeight];
        lowTempMap = new float[mapWidth, mapHeight];
        highTempMap = new float[mapWidth, mapHeight];
        mapDepth = temperatureStart / temperatureScale - 1;

        waterMap = new float[mapWidth, mapHeight];
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                if (prng.NextDouble() < .001)
                    waterMap[x, y] = 20;
                else
                    waterMap[x, y] = 0;
                float totalHeight = noiseMap[x, y] + waterMap[x, y];
                lowPercipitationMap[x, y] = getPercipitation(100, mapDepth + temperatureOffset - totalHeight, pressureConstant, (mapDepth + temperatureOffset - totalHeight * temperatureScale));
                highPercipitationMap[x, y] = getPercipitation(100, temperatureOffset, pressureConstant, temperatureOffset);
                if (highPressureMap[x,y] <= 0)
                {
                    highPressureMap[x, y] = .001f;
                }
                highPressureMap[x, y] = 100;
                lowPressureMap[x, y] = 100;
            }
        }

        

        for (int i = 0; i < iterations; i++)
        {
            waterMap = mapIterator();
        }

        float[,] levelMap = new float[mapWidth, mapHeight];
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                levelMap[x,y] = waterMap[x, y] + noiseMap[x,y];
            }
        }

        Color[] colourMap = generateColorMap();

		
        CreateDisplays(colourMap,levelMap);

    }

    private void CreateDisplays(Color[] colourMap, float[,] levelMap)
    {
        MapDisplay display = FindObjectOfType<MapDisplay> ();
        if (drawMode == DrawMode.NoiseMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(noiseMap));
        }
        else if (drawMode == DrawMode.ColourMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
        }
        else if (drawMode == DrawMode.Mesh)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(noiseMap), TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
        }
        else if (drawMode == DrawMode.Water)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(levelMap), TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
        }
        else if (drawMode == DrawMode.Perc)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(levelMap), TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
            display.DrawPercipitation(levelMap, lowPercipitationMap, highPercipitationMap, mapHeight, mapWidth, mapDepth);
        } else if (drawMode == DrawMode.Cond)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(levelMap), TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
            display.DrawCondensation(levelMap, lowPercipitationMap, highPercipitationMap, lowTempMap, highTempMap, mapHeight, mapWidth, mapDepth);
        }
        else if (drawMode == DrawMode.Temp)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(levelMap), TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
            display.DrawTemp(levelMap, lowTempMap, highTempMap, mapHeight, mapWidth, Mathf.CeilToInt(mapDepth));
        }
        else if (drawMode == DrawMode.Wind)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(levelMap), TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
            display.DrawWind(levelMap, lowPressureMap, highPressureMap, mapHeight, mapWidth, mapDepth);
        }
        else if (drawMode == DrawMode.Pressure)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(levelMap), TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
            display.DrawPressure(levelMap, lowPressureMap, highPressureMap, mapHeight, mapWidth, mapDepth);
        }
    }

    public void GenerateMapIter() {
        if (noiseMap == null)
        {
            GenerateMap();
        }

        

        waterMap = mapIterator();

        float[,] levelMap = new float[mapWidth, mapHeight];
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                levelMap[x,y] = waterMap[x, y] + noiseMap[x,y];
            }
        }

        Color[] colourMap = generateColorMap();

		CreateDisplays(colourMap,levelMap);

        iterations++;
    }

    Color[] generateColorMap()
    {
        Color[] colourMap = new Color[mapWidth * mapHeight];
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                float currentHeight = noiseMap[x, y];
                float waterAmount = waterMap[x, y];

                for (int i = 0; i < regions.Length; i++)
                {
                    if (regions[i].type == TerrainTypeInfo.TerrainType.land)
                    {
                        if (currentHeight <= regions[i].height * depthScale)
                        {
                            colourMap[y * mapWidth + x] = regions[i].colour;
                            break;
                        }
                    }
                }

                if (waterAmount > 0)
                {
                    for (int i = 0; i < regions.Length; i++)
                    {
                        if (regions[i].type == TerrainTypeInfo.TerrainType.water)
                            if (waterAmount >= regions[i].height)
                            {
                                Color regionColor = colourMap[y * mapWidth + x];
                                Color waterColor = regions[i].colour;
                                float a = waterAmount * waterAlpha;
                                colourMap[y * mapWidth + x] = Color.Lerp(regionColor,waterColor,a);
                                break;
                            }
                    }
                }
            }
        }

        return colourMap;
    }
    float[,] mapIterator()
    {
        float[,] newWaterMap = new float[mapWidth, mapHeight];
        float[,] newLowPerc = new float[mapWidth, mapHeight];
        float[,] newHighPerc = new float[mapWidth, mapHeight];
        float[,] newHighPressureMap = new float[mapWidth, mapHeight];
        float[,] newLowPressureMap = new float[mapWidth, mapHeight];
        //iterate over map
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                float currentHeight = noiseMap[x, y];
                float lowPerc = lowPercipitationMap[x, y];
                float highPerc = highPercipitationMap[x, y];
                float waterAmount = waterMap[x, y];
                float totalHeight = currentHeight + waterAmount;
                float highPressure = highPressureMap[x, y];
                float lowPressure = lowPressureMap[x, y];
                float highPressureChange = 0;
                float lowPressureChange = 0;
                float waterChange = 0;
                float lowPercChange = 0;
                float highPercChange = 0;

                
                

                //add water percipitation
                //add percipitation
                if (waterAmount > 0)
                {
                    lowPercChange += .01f;
                    waterChange -= .001f;
                }

                //percipitation transfer
                float percChange = (lowPerc - highPerc);
                float dist = mapDepth - totalHeight;
                float lowTemp = getTemperature(lowPressure, temperatureOffset + mapDepth - totalHeight, pressureConstant, lowPerc); //(temperatureStart - totalHeight * temperatureScale);
                float highTemp = getTemperature(highPressure, temperatureOffset, pressureConstant, highPerc);  //(temperatureStart - mapDepth * temperatureScale);
                highPercChange += Mathf.Clamp(percChange * lowTemp * percipitationSpeed / dist * verticalPercipitationSpeed, 0, (lowPerc < 0)? 0: lowPerc ) ;
                highPercChange -= Mathf.Clamp(percChange * highTemp * percipitationSpeed / dist * verticalPercipitationSpeed, 0, (highPerc < 0)? 0: highPerc);
                lowPercChange -= Mathf.Clamp(percChange * lowTemp * percipitationSpeed / dist * verticalPercipitationSpeed, 0, (lowPerc < 0)? 0: lowPerc);
                lowPercChange += Mathf.Clamp(percChange * highTemp * percipitationSpeed / dist * verticalPercipitationSpeed, 0, (highPerc < 0)? 0: highPerc);

                //temperature for graphing later
                lowTempMap[x, y] = lowTemp;
                highTempMap[x, y] = highTemp;

                //initial pressure change
                float verticalPressureChange = highPressure - lowPressure;
                highPressureChange -= verticalPressureChange / dist * pressureChangeSpeed;
                lowPressureChange += verticalPressureChange / dist * pressureChangeSpeed;
                //effects of temperature
                //using ideal gas law PV = nRT, P = nRT /
                //float idealHighPressure = highPerc * pressureConstant * highTemp; *******************
                //float exHighTempEffect = temperaturePressureEffect * highTemp;
                //highPressureChange += exHighTempEffect;

                //float idealLowPressure = lowPerc * pressureConstant * lowTemp / dist;
                //float lowTempPressureChange = lowPressure - idealLowPressure; *******************
                //float exLowTempEffect = temperaturePressureEffect * lowTemp;
                //lowPressureChange += exLowTempEffect;
                //Check neighbors
                for (int nY = y - 1; nY <= y + 1; nY++)
                {
                    for (int nX = x - 1; nX <= x + 1; nX++)
                    {
                        //Skip self
                        if (nY == y && nX == x)
                        {
                            continue;
                        }
                        //Check if exists
                        if (nY < 0 || nX < 0 || nY >= mapWidth || nX >= mapHeight)
                        {
                            continue;
                        }

                        float nLowPerc = lowPercipitationMap[nX, nY];
                        float nHighPerc = highPercipitationMap[nX, nY];
                        float nCurrentHeight = noiseMap[nX, nY];
                        float nWaterAmount = waterMap[nX, nY];
                        float nTotalHeight = nCurrentHeight + nWaterAmount;
                        float nHighPressure = highPressureMap[nX, nY];
                        float nLowPressure = lowPressureMap[nX, nY];
                        //Apply rules
                        
                        //water rules
                        if (nWaterAmount > 0)
                        {
                            float heightChange = nTotalHeight - totalHeight;
                            heightChange = heightChange * waterMoveSpeed / 9;
                            waterChange += heightChange;
                            //effects pressure
                            lowPressureChange += heightChange * waterPressureEffect;
                        }

                        if (nLowPerc > 0 )
                        {
                            float change = (lowPerc - nLowPerc) / 9;
                            float currentTemp = getTemperature(nLowPressure, temperatureOffset + mapDepth - nTotalHeight, pressureConstant, nLowPerc);   //(temperatureStart - nTotalHeight * temperatureScale);
                            change = change * currentTemp * percipitationSpeed;
                            lowPercChange -= change;
                            //effects pressure
                            lowPressureChange -= change * percipitationPressureEffect;
                        }

                        if (nHighPerc > 0)
                        {
                            float change = (highPerc - nHighPerc) / 9;
                            float currentTemp = getTemperature(nHighPressure, temperatureOffset, pressureConstant, nHighPerc); //(temperatureStart - mapDepth * temperatureScale);
                            change = change * currentTemp * percipitationSpeed;
                            highPercChange -= change;
                            //effects pressure
                            highPressureChange -= change * percipitationPressureEffect;
                        }

                        //pressure comparisons
                        if (nHighPressure > 0)
                        {
                            float change = (highPressure - nHighPressure) / 9;
                            highPressureChange -= change * pressureChangeSpeed;
                        }
                        if (nLowPressure > 0)
                        {
                            float change = (lowPressure - nLowPressure) / 9;
                            lowPressureChange -= change * pressureChangeSpeed;
                        }

                        if (float.IsNaN(lowPressureChange) || float.IsNaN(highPressureChange) || float.IsNaN(lowPercChange) || float.IsNaN(highPercChange))
                        {
                            throw new System.Exception("There is a NaN");
                        }
                    }
                }

                //Apply perc changes
                float newHighPercAmount = highPerc + highPercChange;
                float newLowPercAmount = lowPerc + lowPercChange;
                
                //Stop over pulling from forming rain drops
                if(newHighPercAmount < 0 && highPerc >= 0)
                {
                    newHighPercAmount = 0;
                }
                if (newLowPercAmount  < 0 && lowPerc >= 0)
                {
                    newLowPercAmount = 0;
                }
                //Check for raindrop
                if (newHighPercAmount > highTemp && newHighPercAmount > 0)
                {
                    newHighPercAmount = -newHighPercAmount;
                }
                //if (newLowPercAmount > lowTemp && newLowPercAmount > 0)
                //{
                //    newHighPercAmount = -newLowPercAmount;
                //}

                //waterDrop
                if (lowPercipitationMap[x, y] < 0)
                {
                    waterChange += .1f;
                    newLowPercAmount = .1f * - lowPerc;
                }

                if(highPercipitationMap[x,y] < 0)
                {
                    newHighPercAmount = newLowPercAmount * .1f;
                    newLowPercAmount = highPerc - newLowPercAmount * .9f;
                }

                float newWaterAmount = waterAmount + waterChange;
                if (newWaterAmount < 0)
                    newWaterAmount = 0;

                //pressure
                float newHighPressureAmount = highPressure + highPressureChange;
                float newLowPressureAmount = lowPressure + lowPressureChange;

                if (newHighPressureAmount < 0)
                    newHighPercAmount = 0;
                if (newLowPressureAmount < 0)
                {
                    newLowPressureAmount = 0;
                }

                newHighPressureMap[x, y] = newHighPressureAmount;
                newLowPressureMap[x, y] = newLowPressureAmount;
                newHighPerc[x, y] = newHighPercAmount;
                newLowPerc[x, y] = newLowPercAmount;
                newWaterMap[x, y] = newWaterAmount;
            }
        }

        highPressureMap = (float[,]) newHighPressureMap.Clone();
        lowPressureMap = (float[,]) newLowPressureMap.Clone();
        highPercipitationMap = (float[,])newHighPerc.Clone();
        lowPercipitationMap = (float[,]) newLowPerc.Clone();
        return (float[,]) newWaterMap.Clone();
    }

    float getTemperature( float pressure, float height, float R, float percipitation)
    {
        //using ideal gas law PV = nRT, P = nRT / V, T = PV / nR
        //float idealHighPressure = highPerc * pressureConstant * highTemp;
        if (percipitation < 0)
            //return temperatureOffset;
            percipitation = -.1f * percipitation;
        if (percipitation == 0)
            throw new System.Exception("Percipitation == 0");
        float temperature = (pressure * height) / (percipitation * R);
        if (float.IsNaN(temperature))
        {
            throw new System.Exception("Temperature is NaN");
        }
        return temperature;
    }

    float getPercipitation(float pressure, float height, float R, float temperature)
    {
        float n = (pressure * height) / (R * temperature);
        return n;
    }

    float distanceBetweenTwoAngles(float a, float b)
    {
        bool flipped = false;

        while (a > 360)
        {
            a -= 360;
        }
        while (a < 0)
        {
            a += 360;
        }
        while (b > 360)
        {
            b -= 360;
        }
        while (b < 0)
        {
            b += 360;
        }

        if (a - b > 180){
            b += 180;
            a -= 180;
            flipped = !flipped;
        }
        if (a -b < -180)
        {
            b += 180;
            a -= 180;
            flipped = !flipped;
        }

        while (a > 360)
        {
            a -= 360;
        }
        while (a < 0)
        {
            a += 360;
        }
        while (b > 360)
        {
            b -= 360;
        }
        while (b < 0)
        {
            b += 360;
        }

        if (flipped)
            return b - a;
        return a - b;
    }

    void OnValidate() {
		if (mapWidth < 1) {
			mapWidth = 1;
		}
		if (mapHeight < 1) {
			mapHeight = 1;
		}
        if (depthScale < 1)
        {
            depthScale = 1;
        }
        if (lacunarity < 1) {
			lacunarity = 1;
		}
		if (octaves < 0) {
			octaves = 0;
		}
        if (iterations < 0)
        {
            iterations = 0;
        }
        if (iterationSpeed < 0)
        {
            iterationSpeed = 0;
        }
        if (waterAlpha < 0)
        {
            waterAlpha = 0;
        }
    }

    public void Reset()
    {
        MapDisplay display = FindObjectOfType<MapDisplay> ();
        display.Reset(mapHeight, mapWidth);
    }
}



[System.Serializable]
public struct TerrainTypeInfo {
    public enum TerrainType { land, water }
	public string name;
    public TerrainType type;
	public float height;
	public Color colour;
}