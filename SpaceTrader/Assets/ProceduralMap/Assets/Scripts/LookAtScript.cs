﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtScript : MonoBehaviour {

    public GameObject obj;
    public float rotation;
    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (speed > 0)
        {
            float hDirection = Mathf.Deg2Rad * rotation;
            Vector3 velocity = new Vector3(speed * Mathf.Cos(hDirection), 0, speed * Mathf.Sin(hDirection));
            obj.transform.rotation = Quaternion.LookRotation(velocity,Vector3.up);
            obj.transform.localScale = Vector3.one * speed;
        }
        else
        {
            obj.transform.localScale = Vector3.one * .01f;
        }
    }
}
