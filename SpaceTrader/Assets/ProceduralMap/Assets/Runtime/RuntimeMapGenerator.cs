﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuntimeMapGenerator : MonoBehaviour {

    public MapGenerator target;
    private float time = 0;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if (time > target.iterationSpeed)
        {
            target.GenerateMapIter();
            time = 0;
        }
	}
}
