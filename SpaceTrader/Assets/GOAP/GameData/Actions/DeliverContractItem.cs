﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliverContractItem : GoapAction {

    private bool itemUnloaded = false;
    private IdentityModel owner;
    private Ship ship;

    private double startTime = 0;
    public double workDuration = 30; // seconds

    public DeliverContractItem(Ship _ship)
    {
        addPrecondition("hasTradeItems", true); // if we have items we don't want more
        addPrecondition("hasContract", true);
        //addPrecondition("hasFuel", true);
        addEffect("profit", true);
        owner = _ship.owner.Model;
        ship = _ship;
    }


    public override void reset()
    {
        itemUnloaded = false;
        startTime = 0;
    }

    public override bool isDone()
    {
        return itemUnloaded;
    }

    public override bool requiresInRange()
    {
        return true; // yes
    }

    public override bool checkProceduralPrecondition(PositionEntity agent)
    {
        // Check to make sure contract has destinationId

        
        Ship ship = agent as Ship;
        if (ship.contractId != null)
        {
            try
            {
                Contract contract = GameManager.instance.contracts[ship.contractId];

                if (contract.contractState == ContractState.Active)
                {
                    if (contract.originId != null)
                    {
                        target = GameManager.instance.locations[contract.destinationId];
                        //var structure = target as SupplyStructure;

                        //Item item = ship.storage.Find(contract.itemId, x => x.id == contract.itemId && x.destinationId == contract.destinationId);

                        //if (item == null)
                        //{
                        //    //Debug.Log("Could not find item " + contract.itemId + " in " + structure.name + " with destination " + contract.destinationId);
                        //    return false;
                        //}
                        //else
                        //{
                        //}
                        return true;
                    }
                }
            }
            catch (KeyNotFoundException e)
            {
                return false;
            }


        }

        return false;
    }

    public override bool perform(PositionEntity agent)
    {
        if (startTime == 0)
        {
            startTime = GameManager.instance.data.date.time;

            Ship ship = agent as Ship;
            if (ship.contractId == null)
            {
                Debug.Log("No contract, it ended");
                //ship.storage.RemoveAll();
                return false;
            }
            Contract contract = GameManager.instance.contracts[ship.contractId];
            Item item = ship.storage.Find(contract.itemId, x => x.id == contract.itemId && x.destinationId == contract.destinationId);

            if (item == null)
            {
                Debug.Log("Could not find item " + contract.itemId + " in " + ship.GetName() + " with destination " + contract.destinationId);
                //ship.storage.RemoveAll();
                return false;
            }
            workDuration = (item.AmountSize > ship.storage.TotalCapacity(item.id)) ? ship.storage.TotalCapacity(item.id) * Dated.Minute : Dated.Minute * item.AmountSize;
            ship.Info = "Unloading " + item.Name + ", estimated time " + Dated.ReadTime(workDuration);
        }


        if (GameManager.instance.data.date.time - startTime > workDuration)
        {
            // Finish item unload
            Ship ship = agent as Ship;
            
            ship.RefuelShip();
            SupplyStructure structure = GameManager.instance.locations[ship.GetReferenceId()] as SupplyStructure;
            
            if (ship.contractId == null)
            {
                Debug.Log("No contract, it ended");
                return false;
            }
            Contract contract = GameManager.instance.contracts[ship.contractId];
            Item item = ship.storage.Find(contract.itemId, x => x.id == contract.itemId && x.destinationId == contract.destinationId);
            //Remove amount enroute
            contract.itemAmountEnroute -= item.amount;
            contract.ItemBlueprint.unitsSold += item.amount; //TODO: Move to when item is actually sold
            ship.ShipTargetId = null;
            ship.Info = "Done unloading " + item.Name;
            if (item.id == structure.structureBlueprintId)
            {
                //Construct structure
                structure.Count += (int)item.amount;
                structure.workers = ((ProductionBlueprint)structure.GetStructureModel()).workers * structure.Count;

                //Update Storage Capacity

                if (structure as ProductionStructure != null)
                {
                    structure.storage.itemCapacity[((ProductionStructure)structure).Product.research][1] = Mathd.Floor(((ProductionStructure)structure).ProductionRateOptimal * Dated.Month * 2 + 2);

                    foreach (KeyValuePair<string, int> construction in structure.materialBlueprints)
                    {
                        var blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(construction.Key);

                        structure.storage.itemCapacity[blueprint.research][1] = Mathd.Floor(Mathd.Floor(((ProductionStructure)structure).ProductionRateOptimal * Dated.Month * 2 + 2) * construction.Value + 2);
                    }
                }

                Debug.Log(item.Name + "Construction at " + structure.GetName());

                ship.storage.Remove(item);
            }
            else
            {
                //Add to storage
                structure.storage.AddItem(item);
                ship.storage.Remove(item);
            }

            if (contract.itemAmount <= 0 && contract.itemAmountEnroute <= 0)
            {
                //End contract
                contract.contractState = ContractState.Rejected;
                //Contract has ended
            }
            ship.contractId = null;
            if (contract.contractState == ContractState.Rejected)
            {
                GameManager.instance.contracts.Remove(contract.id);
                ship.owner.Model.Government.postedContractIds.Remove(contract.id);
                ship.owner.Model.contracts.Remove(contract.id);
                if (contract.destinationId != null)
                {
                    //Remove destination supply listing
                    SupplyStructure destination = GameManager.instance.locations[contract.destinationId] as SupplyStructure;
                    destination.supplierContractIds.Remove(contract.id);
                }
            }
            ship.ShipTargetId = null;
            itemUnloaded = true;
        }
        return true;
    }

    private double CalculateProfit(Item item, Ship ship)
    {
        //double fuelMarketPrice = owner.KnownStarIds[0].GetMarketPrice(ship.fuel.id);
        //double itemAmount = item.amount;
        //if (ship.itemCapacity < itemAmount)
        //    itemAmount = ship.itemCapacity;

        //return item.price * itemAmount - (item.position[0] - ship.position[0]).magnitude * fuelMarketPrice / (ship.speed * ship.fuelEfficiency); //- ship.items[0].amount * ship.items[0].price;
        return 1;
    }
}
