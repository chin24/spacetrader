﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadContractItems : GoapAction {

    private bool itemLoaded = false;
    private IdentityModel owner;
    private Ship ship;
    //private ItemBlueprint itemBlueprint;
    private double startTime = 0;
    public double workDuration = Dated.Hour; // seconds

    public LoadContractItems(Ship _ship)
    {
        addPrecondition("hasStorage", true); // we need storage for items
        addPrecondition("hasTradeItems", false); // if we have items we don't want more
        addPrecondition("hasContract", true);
        //addPrecondition("hasFuel", true);
        addEffect("hasTradeItems", true);

        ship = _ship;
        owner = _ship.owner.Model;      

    }


    public override void reset()
    {
        itemLoaded = false;
        startTime = 0;
    }

    public override bool isDone()
    {
        return itemLoaded;
    }

    public override bool requiresInRange()
    {
        return true; // yes
    }

    public override bool checkProceduralPrecondition(PositionEntity agent)
    {
        // Check to make sure contract exists, is active, and has an origin

        Ship ship = agent as Ship;
        if (ship.contractId != null)
        {
            try
            {
                Contract contract = GameManager.instance.contracts[ship.contractId];

                if (contract.contractState == ContractState.Active)
                {
                    if (contract.originId != null)
                    {
                        target = GameManager.instance.locations[contract.originId];
                        var structure = target as SupplyStructure;

                        Item item = structure.storage.Find(contract.itemId, x => x.id == contract.itemId && x.destinationId == contract.destinationId);

                        if (item == null)
                        {
                            //Debug.Log("Could not find item " + contract.itemId + " in " + structure.name + " with destination " + contract.destinationId);
                            return false;
                        }
                        else
                        {
                        }
                        return true;
                    }
                }
            }
            catch(KeyNotFoundException e)
            {
                return false;
            }

            
        }

        return false;
    }

    public override bool perform(PositionEntity agent)
    {
        
        
        if (startTime == 0)
        {
            startTime = GameManager.instance.data.date.time;

            Ship ship = agent as Ship;
            SupplyStructure structure = GameManager.instance.locations[ship.GetReferenceId()] as SupplyStructure;
            if (ship.contractId == null || !GameManager.instance.contracts.ContainsKey(ship.contractId))
            {
                ship.contractId = null;
                return false;
            }
            Contract contract = GameManager.instance.contracts[ship.contractId];

            Item item = structure.storage.Find(contract.itemId, x => x.id == contract.itemId && x.destinationId == contract.destinationId);

            if (item == null)
            {
                //Debug.Log("Could not find item " + contract.itemId + " in " + structure.name + " with destination " + contract.destinationId);
                return false;
            }
            if (item.amount * item.Size > ship.storage.RemainingCapacity(item.id))
            {
                workDuration = Dated.Minute * ship.storage.RemainingCapacity(item.id);
            }
            else
                workDuration = Dated.Minute * item.amount * item.Size;

            ship.Info = "Loading " + ship.GetName() + ", duration: " + Dated.ReadTime(workDuration);
            if (GameManager.instance.debugLog)
                GameManager.instance.timeScale = 1;
        }
            

        if (GameManager.instance.data.date.time - startTime > workDuration)
        {
            // Finish item load
            Ship ship = agent as Ship;

            ship.RefuelShip();
            SupplyStructure structure = GameManager.instance.locations[ship.GetReferenceId()] as SupplyStructure;
            Contract contract = GameManager.instance.contracts[ship.contractId];
            Item item = structure.storage.Find(contract.itemId, x => x.id == contract.itemId && x.destinationId == contract.destinationId);
            if (item == null)
            {
                //Debug.Log("Could not find item " + contract.itemId + " in " + structure.name + " with destination " + contract.destinationId);
                return false;
            }
            if (item.amount < 1)
            {
                Debug.Log("Remaining item too small: " + item.amount);
                ship.Info = "Remaining item too small: " + item.amount;
                return false;
            }
                
            if (item.amount > ship.storage.RemainingCapacity(item.id))
            {
                double remaining = Mathd.Floor(ship.storage.RemainingCapacity(item.id));
                if (remaining >= 1)
                {
                    structure.storage.RemoveAmount(item.id, remaining, item.destinationId);
                    ship.storage.AddItem(item.id, ship.GetId(), item.destinationId, remaining);
                    contract.itemAmount -= remaining;
                    contract.itemAmountEnroute += remaining;
                }
                else throw new System.Exception("Not enough space: " + ship.storage.RemainingCapacity(item.id));
                
            }
            else
            {
                ship.storage.AddItem(item);
                contract.itemAmount -= item.amount;
                contract.itemAmountEnroute += item.amount;
                structure.storage.Remove(item);
            }
            itemLoaded = true;
            ship.Info = ("Loading " + ship.GetName() + " Finished. Destination: " + GameManager.instance.locations[contract.destinationId].GetName());

        }
        return true;
    }
}

public class FindFuel : GoapAction
{

    private bool shipFueled = false;
    private IdentityModel owner;
    private Ship ship;
    //private ItemBlueprint itemBlueprint;
    private double startTime = 0;
    public double workDuration = Dated.Hour; // seconds

    public FindFuel(Ship _ship)
    {
        addPrecondition("hasFuel", false); // if we have items we don't want more
        addEffect("hasFuel", true);

        ship = _ship;
        owner = _ship.owner.Model;

    }


    public override void reset()
    {
        shipFueled = false;
        startTime = 0;
    }

    public override bool isDone()
    {
        return shipFueled;
    }

    public override bool requiresInRange()
    {
        return true; // yes
    }

    public override bool checkProceduralPrecondition(PositionEntity agent)
    {
        // Find Fuel Station

        Ship ship = agent as Ship;

        if (ship.contractId != null)
        {
            //Make current contract is for fuel
            Contract contract = GameManager.instance.contracts[ship.contractId];
            if (contract.itemId != ship.fuel.id)
                throw new System.Exception("Not correct fuel contract: " + contract.itemName + " instead of " + ship.fuel.Name);
            if (contract.contractState == ContractState.Review)
            {
                target = GameManager.instance.locations[contract.originId];
                //Verify that contract will work, accept or reject
                contract.contractState = ContractState.Accepted;
                return true;
            }
            else if (contract.contractState == ContractState.Rejected)
            {
                ship.contractId = null;
            }
        }
        else
        {
            var contract = ship.SearchContracts(ship.fuel.id, ship.fuelCapacity - ship.fuel.amount);
            if (contract == null)
                return false;
            ship.contractId = contract.id;
        }

        return false;
    }

    public override bool perform(PositionEntity agent)
    {


        if (startTime == 0)
        {
            startTime = GameManager.instance.data.date.time;

            Ship ship = agent as Ship;
            SupplyStructure structure = GameManager.instance.locations[ship.GetReferenceId()] as SupplyStructure;
            if (ship.contractId == null)
                return false;
            Contract contract = GameManager.instance.contracts[ship.contractId];
            Item item = structure.storage.Find(contract.itemId, x => x.id == contract.itemId && x.destinationId == contract.destinationId);

            if (item == null)
            {
                //Debug.Log("Could not find item " + contract.itemId + " in " + structure.name + " with destination " + contract.destinationId);
                return false;
            }
            workDuration = Dated.Minute * item.amount;

            ship.Info = "Loading " + ship.GetName() + ", duration: " + Dated.ReadTime(workDuration);
            if (GameManager.instance.debugLog)
                GameManager.instance.timeScale = 1;
        }


        if (GameManager.instance.data.date.time - startTime > workDuration)
        {
            // Finish item load
            Ship ship = agent as Ship;
            SupplyStructure structure = GameManager.instance.locations[ship.GetReferenceId()] as SupplyStructure;
            Contract contract = GameManager.instance.contracts[ship.contractId];
            Item item = structure.storage.Find(contract.itemId, x => x.id == contract.itemId && x.destinationId == contract.destinationId);
            if (item == null)
            {
                //Debug.Log("Could not find item " + contract.itemId + " in " + structure.name + " with destination " + contract.destinationId);
                return false;
            }
            ship.fuel.amount += item.amount;
            structure.storage.Remove(item);
            shipFueled = true;
            ship.Info = (ship.GetName() + " fueled");
            ship.contractId = null;

        }
        return true;
    }


}

