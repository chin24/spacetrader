﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrillLocationAction : GoapAction {

    private IdentityModel owner;
    private Ship ship;
    //private ItemBlueprint itemBlueprint;
    private double startTime = 0;
    public double workDuration = Dated.Hour; // seconds

    public bool drillingComplete;
    public string solarBodyId;
    public string rawResourceId;
    public string blueprintId;

    public DrillLocationAction(Ship _ship)
    {
        addPrecondition("hasStorage", true); // we need storage for items
        addPrecondition("hasTradeItems", false); // if we have items we don't want more
        addPrecondition("hasContract", false);
        //addPrecondition("hasFuel", true);
        addEffect("hasTradeItems", true);

        ship = _ship;
        owner = _ship.owner.Model;      

    }


    public override void reset()
    {
        solarBodyId = null;
        rawResourceId = null;
        startTime = 0;
        drillingComplete = false;
    }

    public override bool isDone()
    {
        return drillingComplete;
    }

    public override bool requiresInRange()
    {
        return true; // yes
    }

    public override bool checkProceduralPrecondition(PositionEntity agent)
    {
        // Find potential asteroid/commet

        Ship ship = agent as Ship;
        var solarBodies = ship.GetReferenceBody().GetStar().GetAllSolarBodies();
        List<SolarBody> possibleBodies = new List<SolarBody>();

        foreach(SolarBody body in solarBodies)
        {
            if (body.GetSolarType() == SolarTypes.Asteroid || body.GetSolarType() == SolarTypes.Comet)
            {
                if (body.GetRawResources().Count > 0 && body.GetRawResources().Exists(x => ship.owner.Model.Owner.GetOwnedBlueprints().Exists(y=> y.research == x.GetId())))
                {
                    possibleBodies.Add(body);
                }
            }
        }
        if (possibleBodies.Count == 0)
            return false;
        var chosenBody = possibleBodies[Random.Range(0, possibleBodies.Count)];
        solarBodyId = chosenBody.GetId();
        target = chosenBody;
        var possibleResources = chosenBody.GetRawResources().FindAll(x => ship.owner.Model.Owner.GetOwnedBlueprints().Exists(y => y.research == x.GetId()));
        rawResourceId = possibleResources[Random.Range(0, possibleResources.Count)].GetId();
        blueprintId = ship.owner.Model.Owner.GetOwnedBlueprints().Find(x => x.research == rawResourceId).Id;
        return true;
    }

    public override bool perform(PositionEntity agent)
    {
        
        
        if (startTime == 0)
        {
            startTime = GameManager.instance.data.date.time;
        }
        else
        {
            //Drill Resource
            double drillAmount = ship.deltaTime * .001; //TODO: Better drilling behavior
            if (drillAmount > ship.storage.RemainingCapacity(rawResourceId))
                drillAmount = ship.storage.RemainingCapacity(rawResourceId);
            var body = ((SolarBody)GameManager.instance.locations[solarBodyId]);
            var resource = body.GetRawResources().Find(x => x.GetId() == rawResourceId);
            if (resource == null)
            {
                return false;
            }
            if (drillAmount > resource.GetAmount())
            {
                drillAmount = resource.GetAmount();
                if (resource.GetAmount() <= 0)
                {
                    body.GetRawResources().Remove(resource);
                }
            }
                

            ship.storage.AddItem(blueprintId, ship.GetId(), null, drillAmount);
            resource.RemoveAmount(drillAmount);
            ship.Info = ("Drilling  for" + ship.GetName() + " In progress. Amount drilled: " + drillAmount);
            GameManager.instance.data.blueprintsModel.Model.GetItem(blueprintId).unitsCreated += drillAmount;
            if (ship.storage.RemainingCapacity(rawResourceId) <= 0 || resource.GetAmount() <= 0)
                drillingComplete = true;
            ship.Info = ("Drilling for " + ship.GetName() + " Finished");
            //TODO: Need a check to delete depleted body resources
        }
        return true;
    }
}
