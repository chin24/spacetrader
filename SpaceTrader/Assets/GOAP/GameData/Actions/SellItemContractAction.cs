﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellItemContractAction : GoapAction {

    private bool contractSetup = false;
    private IdentityModel owner;
    private Ship ship;

    private double startTime = 0;
    public double workDuration = 30; // seconds

    public SellItemContractAction(Ship _ship)
    {
        addPrecondition("hasTradeItems", true); // if we have items we don't want more
        addPrecondition("hasContract", false);
        //addPrecondition("hasFuel", true);
        addEffect("profit", true);
        owner = _ship.owner.Model;
        ship = _ship;
    }


    public override void reset()
    {
        contractSetup = false;
        startTime = 0;
    }

    public override bool isDone()
    {
        return contractSetup;
    }

    public override bool requiresInRange()
    {
        return false; // no
    }

    public override bool checkProceduralPrecondition(PositionEntity agent)
    {
        // Check to make sure contract has destinationId

        return true;
    }

    public override bool perform(PositionEntity agent)
    {
        Ship ship = agent as Ship;
        if (ship.contractId == null)
        {
            //create contract
            Item item = ship.storage.itemsStorage["All"].Find(x => x.destinationId == null);

            if (item == null)
            {
                Debug.Log("Could not find any free item in " + ship.GetName() + " with no destination ");
                ship.Info = "Could not find any free item in with no destination ";
                return false;
            }

            Contract contract = new Contract(ship.owner.Model, item.id, item.amount, ship.GetReferenceId(), ship.structureBlueprintId, item.Blueprint.EstimatedValue * 2);
            ship.contractId = contract.id;
            ship.owner.Model.Government.postedContractIds.Add(contract.id);
            ship.owner.Model.contracts.Add(contract.id);
            contract.shipIds.Add(ship.GetId());
            
            workDuration = (item.AmountSize > ship.storage.TotalCapacity(item.id)) ? ship.storage.TotalCapacity(item.id) * Dated.Minute : Dated.Minute * item.AmountSize;
        }
        else
        {
            //look for a buyer
            var contract = GameManager.instance.contracts[ship.contractId];
            ship.Info = "Looking for buyer";
            if (contract.contractState != ContractState.Active)
            {
                if (contract.contractState == ContractState.Rejected)
                {
                    ship.contractId = null;
                    GameManager.instance.contracts.Remove(contract.id);
                    ship.owner.Model.Government.postedContractIds.Remove(contract.id);
                    ship.owner.Model.contracts.Remove(contract.id);
                    if (contract.destinationId != null)
                    {
                        //Remove destination supply listing
                        SupplyStructure destination = GameManager.instance.locations[contract.destinationId] as SupplyStructure;
                        destination.supplierContractIds.Remove(contract.id);
                    }
                }
                else if (contract.contractState == ContractState.Sent)
                {
                    //Make sure that the contract is profitable and then set to review, check that ship can deliver the amount of goods needed per month, etc.
                    contract.itemAmount = contract.alternateItemAmount;

                    //Setup Costs
                    double distance = Vector3d.Distance(GameManager.instance.locations[contract.originId].SystemPosition, GameManager.instance.locations[contract.destinationId].SystemPosition);
                    contract.CalculateCost(distance, 1, contract.itemAmount);

                    //Set due date
                    contract.contractEndDate = new Dated(GameManager.instance.data.date.time + contract.duration + 15 * Dated.Day);

                    contract.contractState = ContractState.Review;
                }
                else if (contract.contractState == ContractState.Accepted)
                {
                    //Payment
                    contract.client.Model.money.SubtractMoney(contract.ItemBlueprint.research, contract.cost);
                    ship.owner.Model.money.AddMoney("Income", contract.cost);

                    if (GameManager.instance.locations[contract.destinationId].GetType() == typeof(DistributionCenter))
                    {
                        //Undo payment if going to DC
                        contract.client.Model.money.SubtractMoney(contract.ItemBlueprint.research, -contract.cost);
                    }

                    //Designate items
                    Item item = new Item(contract.itemId, contract.itemAmount) { destinationId = contract.destinationId };

                    ship.storage.RemoveAmount(item.id, item.amount);
                    ship.storage.AddItem(item);

                    contract.contractState = ContractState.Active;
                    contractSetup = true;

                    //Set item enroute
                    contract.itemAmount -= item.amount;
                    contract.itemAmountEnroute += item.amount;

                    ship.Info = "Found buyer";
                }
                else if (contract.contractState == ContractState.Initial)
                {
                    //Update initial contract
                    contract.unitPrice -= contract.unitPrice * ship.deltaTime / Dated.Year;
                    if (contract.unitPrice < .001)
                    {
                        contract.unitPrice = .001;
                    }
                }
            }
            else
            {
                if (contract.itemAmount <= 0 && contract.itemAmountEnroute <= 0)
                {
                    //End contract
                    contract.contractState = ContractState.Rejected;
                    //Contract has ended
                }
            }
        }
        return true;
    }

    private double CalculateProfit(Item item, Ship ship)
    {
        //double fuelMarketPrice = owner.KnownStarIds[0].GetMarketPrice(ship.fuel.id);
        //double itemAmount = item.amount;
        //if (ship.itemCapacity < itemAmount)
        //    itemAmount = ship.itemCapacity;

        //return item.price * itemAmount - (item.position[0] - ship.position[0]).magnitude * fuelMarketPrice / (ship.speed * ship.fuelEfficiency); //- ship.items[0].amount * ship.items[0].price;
        return 1;
    }
}
