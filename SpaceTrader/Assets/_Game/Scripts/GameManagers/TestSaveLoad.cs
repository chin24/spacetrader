﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TestSaveLoad : MonoBehaviour
{
    public GameDataModel data;
    string dataPath;

    void Start()
    {
        dataPath = Path.Combine(Application.persistentDataPath, "Data.json");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
            SaveData(dataPath);

        if (Input.GetKeyDown(KeyCode.L))
            data = LoadData(dataPath);
    }

    static void SaveData(string path)
    {
        GameDataModel data = GameManager.instance.data;
        string jsonString = JsonUtility.ToJson(data);

        using (StreamWriter streamWriter = File.CreateText(path))
        {
            streamWriter.Write(jsonString);
        }
    }

    static GameDataModel LoadData(string path)
    {
        using (StreamReader streamReader = File.OpenText(path))
        {
            string jsonString = streamReader.ReadToEnd();
            return JsonUtility.FromJson<GameDataModel>(jsonString);
        }
    }
}
