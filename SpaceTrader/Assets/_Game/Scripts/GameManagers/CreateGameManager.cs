﻿using CodeControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateGameManager : MonoBehaviour {

    public GameObject loadingPanel;
    public Text loadingText;
    public Slider loadingProgress;
    //public Text galaxyNameText;

    //Galaxy Parameters
    public int starCount;
    public Vector3 mapField;
    public Gradient sunSizeColor;
    public Gradient planetSizeColor;

    //Game Parameters
    public string galaxyName;
    public int numGov;
    public int numComp;
    public int numStation;
    public int numShip;
    public bool randomSeed;

    internal GameManager game;
    internal ViewManager galaxy;

    internal static float G = .01f;
    internal NameGen names;

    public void Awake()
    {
        transform.localPosition = Vector3.zero;
    }

    public void CreateGame()
    {
        galaxy = ViewManager.instance;
        game = GameManager.instance;
        names = game.names;
        GameManager.instance.setup = true;
        loadingPanel.SetActive(true);
        StartCoroutine("CreatingGame"); //Creates a new Game
    }

    IEnumerator CreatingGame()
    {
        loadingText.text = "Creating Game...";
        loadingProgress.value = 0;
        while (GameManager.instance.setup)
        {
            //CreateStarSystem
            loadingText.text = "Creating Stars...";
            game.data.galaxyName = galaxyName;
            //galaxyNameText.text = galaxyName;

            foreach (RawResourceBlueprint raw in game.data.rawResources.Model.rawResources)
            {
                game.research.Add(new Research(raw.name, raw.description, (int)((Mathd.Pow(raw.miningTime, .5) > 10) ? Mathd.Pow(raw.miningTime, .5) : 10), (int)raw.miningTime, ResearchCategory.Raw)
                {
                    baseMass = raw.mass,
                    size = .5f,
                    baseArmor = raw.armor
                }
                    );
            }

            game.research.Add(new Research("Basic AI", "A basic AI unit", 100, Dated.Day, ResearchCategory.Intermediate)
            {
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 3, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    {defaultRawResources.Coppode.ToString(),new ResearchPart(){ minAmount = 1, maxAmount = 5, armorContributionPercent = .5f, workCost=500, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } }
                }
            });

            game.research.Add(new Research("Basic Machinery", "Factory Machinery", 50, Dated.Day, ResearchCategory.Intermediate)
            {
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 5, maxAmount = 10, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    {"Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .2f, workCost=500, minMassContributionPercent = 1, maxMassContributionPercent = 1f } }
                }
            });

            game.research.Add(new Research("Large Drill Bit", "Used in Drillers and Large Ships", 50, Dated.Day, ResearchCategory.Intermediate)
            {
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 10, maxAmount = 20, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { defaultRawResources.Goldium.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 10, armorContributionPercent = 1, workCost=200, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } }
                }
            });

            game.research.Add(new ProductionResearch("Basic Factory", "Creates items using blueprints on Planets", 100, 30 * Dated.Day)
            {
                tags = new List<ResearchTags> { ResearchTags.ProducesIntermediateProducts, ResearchTags.LowAtmosphere, ResearchTags.ProducesStructures },
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 100, maxAmount = 200, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    //{"Basic Machinery", new int[4] {1,200,30,9} },
                    //{"Basic AI", new int[4] {1,1,10,1} }
                }
            });
            game.research.Add(new ProductionResearch("Station Factory", "Creates items using blueprints on stations", 500, 40 * Dated.Day)
            {
                tags = new List<ResearchTags> { ResearchTags.ProducesIntermediateProducts, ResearchTags.Station, ResearchTags.ProducesStructures },
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 100, maxAmount = 200, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { "Basic Machinery", new ResearchPart(){ minAmount = 10, maxAmount = 200, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new ProductionResearch("Basic Shipyard", "Creates ships", 100, 30 * Dated.Day)
               {
                tags = new List<ResearchTags> { ResearchTags.ProducesShips, ResearchTags.LowAtmosphere },
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 200, maxAmount = 400, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { "Basic Machinery", new ResearchPart(){ minAmount = 10, maxAmount = 200, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });
            game.research.Add(new ProductionResearch("Station Shipyard", "Creates ships on stations", 500, 40 * Dated.Day)
            {
                tags = new List<ResearchTags> { ResearchTags.ProducesShips, ResearchTags.Station },
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 100, maxAmount = 400, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { "Basic Machinery", new ResearchPart(){ minAmount = 10, maxAmount = 200, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new ProductionResearch("Distribution Center", "Distributes items to the general population", 100, 20 * Dated.Day)
            {
                tags = new List<ResearchTags> { ResearchTags.Distribution, ResearchTags.Atmosphere },
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 200, maxAmount = 400, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { "Basic Machinery", new ResearchPart(){ minAmount = 10, maxAmount = 200, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new ProductionResearch("Research Center", "Allows for research to be done", 100, 20 * Dated.Day)
            {
                tags = new List<ResearchTags> { ResearchTags.Research, ResearchTags.LowAtmosphere, ResearchTags.Atmosphere },
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 200, maxAmount = 400, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { "Basic Machinery", new ResearchPart(){ minAmount = 10, maxAmount = 200, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new ProductionResearch("Station Research Center", "Allows for research to be done in space", 1000, 30 * Dated.Day)
            {
                tags = new List<ResearchTags> { ResearchTags.Research, ResearchTags.Station},
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 100, maxAmount = 400, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { "Basic Machinery", new ResearchPart(){ minAmount = 10, maxAmount = 200, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new ProductionResearch("Station Base", "Used to Connect other stations together", 500, 30 * Dated.Day)
            {
                tags = new List<ResearchTags> { ResearchTags.BaseStation},
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 50, maxAmount = 100, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new ProductionResearch("Basic Driller", "Mines rocky planets", 100, 30 * Dated.Day)
            {
                tags = new List<ResearchTags> { ResearchTags.ProducesRawProducts, ResearchTags.LowAtmosphere, ResearchTags.Atmosphere }
            });

            game.research.Add(new ProductionResearch("Asteroid Driller", "Allows for direct mining of asteroids", 1000, 40 * Dated.Day)
            {
                tags = new List<ResearchTags> { ResearchTags.ProducesRawProducts, ResearchTags.NoAtmosphere},
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 50, maxAmount = 100, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { "Basic Machinery", new ResearchPart(){ minAmount = 1, maxAmount = 200, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Large Drill Bit", new ResearchPart(){ minAmount = 1, maxAmount = 5, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new Research("Small Drill Bit", "Used in small and medium sized driller ships", 50, 4 * Dated.Hour, ResearchCategory.Intermediate)
                {
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 20, maxAmount = 50, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new FuelResearch("Fuel Cell", "A normal fuel cell", 100, Dated.Hour)
            {
                size = 1,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 20, maxAmount = 50, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new Research("Basic Engine", "A usable engine", 100, 2 * Dated.Day, ResearchCategory.Intermediate)
            {
                size = 2,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 2, maxAmount = 10, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { defaultRawResources.Warium.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 10, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = .25f, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new Research("Rocket Engine", "A usable engine for rockets", 100, Dated.Day, ResearchCategory.Intermediate)
            {
                size = 1,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 5, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { defaultRawResources.Warium.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 10, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = .25f, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new Research("Ship Fueltank", "Stores ship fuel", 100, 3 * Dated.Hour, ResearchCategory.Intermediate)
            {
                size = 5,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 20, maxAmount = 50, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                },
                supportedResearch = new List<string> { "Fuel Cell" }
            });

            game.research.Add(new Research("Ship Sensor", "Allows ship to identify nearby objects", 100, Dated.Day, ResearchCategory.Intermediate)
            {
                size = 1,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 2, maxAmount = 5, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { defaultRawResources.Limoite.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 10, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = .25f, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new Research("Ship Storage Container", "Stores items", 100, 3 * Dated.Hour, ResearchCategory.Intermediate)
            {
                size = 25,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 50, maxAmount = 200, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new Research("Ship Basic Cannon", "Shoots shells", 100, 3 * Dated.Hour, ResearchCategory.Intermediate)
            {
                size = 10f,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 10, maxAmount = 15, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { defaultRawResources.Warium.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 10, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = .25f, maxMassContributionPercent = 1f } },
                }
            });
            game.research.Add(new Research("Ship Basic Missile Launcher", "Shoots missiles", 100, 3 * Dated.Hour, ResearchCategory.Intermediate)
                {
                size = 10,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 10, maxAmount = 20, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new Research("Ship Cannon Shell", "Damages close things", 100, 3 * Dated.Hour, ResearchCategory.Intermediate)
            {
                size = .5f,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 5, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { defaultRawResources.Warium.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 10, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = .25f, maxMassContributionPercent = 1f } },
                },
                supportedResearch = new List<string> { "Ship Basic Cannon" }
            });

            game.research.Add(new Research("Ship Missile", "Damages far things", 100, 3 * Dated.Hour, ResearchCategory.Intermediate)
            {
                size = 2,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 1, maxAmount = 5, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { "Rocket Engine", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = 1, workCost=100, minMassContributionPercent = 1f, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1f, maxMassContributionPercent = 1f } },
                },
                supportedResearch = new List<string> { "Ship Basic Missile Launcher" }
            });

            game.research.Add(new ShipResearch("Cargo Ship", "Transports items from point A to point B", 1000, Dated.Month)
            {
                tags = new List<ResearchTags> { ResearchTags.HoldsCargo},
                size = 50,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 20, maxAmount = 150, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { defaultRawResources.Glassitum.ToString(), new ResearchPart(){ minAmount = 2, maxAmount = 50, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = .25f, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Storage Container", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Sensor", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Fueltank", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic Engine", new ResearchPart(){ minAmount = 1, maxAmount = 5, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new ShipResearch("Combat Ship", "Attack things", 1000, Dated.Month)
            {
                tags = new List<ResearchTags> { ResearchTags.CombatShip},
                size = 10,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 20, maxAmount = 50, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { defaultRawResources.Glassitum.ToString(), new ResearchPart(){ minAmount = 2, maxAmount = 10, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = .25f, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Basic Missile Launcher", new ResearchPart(){ minAmount = 1, maxAmount = 2, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Basic Cannon", new ResearchPart(){ minAmount = 1, maxAmount = 2, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Sensor", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Fueltank", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic Engine", new ResearchPart(){ minAmount = 1, maxAmount = 5, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            game.research.Add(new ShipResearch("Driller Ship", "Drills for resources on Asteroids", 1000, Dated.Month)
            {
                tags = new List<ResearchTags> { ResearchTags.ProducesRawProducts},
                size = 50,
                requiredResearch = new Dictionary<string, ResearchPart>() {
                    { defaultRawResources.Armoroid.ToString(), new ResearchPart(){ minAmount = 20, maxAmount = 150, armorContributionPercent = 1, workCost=100, minMassContributionPercent = .5f, maxMassContributionPercent = 1f } },
                    { defaultRawResources.Glassitum.ToString(), new ResearchPart(){ minAmount = 2, maxAmount = 50, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = .25f, maxMassContributionPercent = 1f } },
                    { "Basic AI", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Small Drill Bit", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Storage Container", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Sensor", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Ship Fueltank", new ResearchPart(){ minAmount = 1, maxAmount = 1, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                    { "Basic Engine", new ResearchPart(){ minAmount = 1, maxAmount = 5, armorContributionPercent = .1f, workCost=100, minMassContributionPercent = 1, maxMassContributionPercent = 1f } },
                }
            });

            if (randomSeed)
            {
                CreateStars(starCount, System.DateTime.Now.ToString());
            }
            else CreateStars(starCount, game.data.galaxyName);
            loadingProgress.value = .5f;

            GameManager.instance.setup = false;
            yield return null;
            for (int a = 0; a < numGov; a++)
            {
                //Create Government
                loadingText.text = "Creating Governments...";


                GovernmentModel gov = new GovernmentModel(names.GenerateWorldName() + " Government");
                var parent = game.locations[gov.referenceId] as SolarBody;
                //DistributionCenter center = new DistributionCenter(gov, parent);

                var star = parent.GetStar();

                for (int c = 0; c < numComp; c++)
                {
                    var random = new System.Random((gov.Id + c + gov.name).GetHashCode());
					var possibleSatellites = star.GetAllSolarBodies();
                    var body = possibleSatellites[random.Next(possibleSatellites.Count)];
                    int count = 0;
					while (body.GetSolarSubType() == SolarSubTypes.GasGiant || body.GetSolarType() == SolarTypes.Asteroid || body.GetSolarType() == SolarTypes.Comet || body.GetSolarType() == SolarTypes.Star)
                    {
                        body = possibleSatellites[random.Next(possibleSatellites.Count)];
                        count++;
                        if (count > 1000)
                        {
                            throw new System.Exception("Finding body loop detected");
                        }
                    }
						

                    //Pick random company research
                    var companyResearch = game.research.allResearch[game.research.researchNames[random.Next(0,game.research.researchNames.Count)]];

                    Creature captain = new Creature(names.GenerateMaleFirstName() + " " + names.GenerateWorldName(), 100000, body.GetId(), game.data.date, new Dated(30 * Dated.Year), CreatureType.Human);
                    CompanyModel comp = new CompanyModel(names.GenerateRegionName() + " Company",companyResearch.name, body, gov, captain);
                    //if (c == 0)
                    //{
                    //    owner.isPlayer = true;
                    //    GameManager.instance.data.playerCreatureId = owner.id;
                    //}

                    

                    //--------------Create Ships---------------------------//
                    //for (int i = 0; i < numShip; i++)
                    //{
                    //    if (i != 0)
                    //    {
                    //        captain = new Creature(names.GenerateMaleFirstName() + " " + names.GenerateWorldName(), 10000, station, game.data.date, new Dated(30 * Dated.Year), CreatureType.Human);
                    //    }

                    //    if (comp.structureIds.Count > 0)
                    //    {
                    //        Ship ship = new Ship(comp.name + " Ship " + (i + 1), comp, captain, comp.structureIds[0]);
                    //    }
                        
                    //    //loadingText.text = string.Format("Creating ships: {0} of {1}", i, numShip);

                    //}
                    yield return null;
                }
                loadingProgress.value = .5f + a / numGov * .5f;
                yield return null;
            }
            //Sets menu and loading panel to inactive and starts the game
            loadingText.text = "Done";
            loadingPanel.SetActive(false);
            gameObject.SetActive(false);
            game.StartGame();
        }
    }

    public void CreateStars(int count, string seed)
    {
        Random.InitState(seed.GetHashCode());
        var random = new System.Random(seed.GetHashCode());
        for (int i = 0; i < count; i++)
        {
            // Create stars with distance in lightyears

            // position = new Vector3((float) random.NextDouble() * mapField.x * 2 - mapField.x * .5f, (float)random.NextDouble() * mapField.y * 2 - mapField.y * .5f, (float)random.NextDouble() * mapField.z * 2 - mapField.z * .5f);
            Vector3 position = new Vector3((float)NormalizedRandom(-mapField.x * .5f, mapField.x * .5f, random.NextDouble()), (float)NormalizedRandom(-mapField.y * .5f, mapField.y * .5f, random.NextDouble()), (float)NormalizedRandom(-mapField.z * .5f, mapField.z * .5f, random.NextDouble()));
            CreateStar(names.GenerateWorldName(random.Next().ToString()) + " " + (i + 1), position);
            
        }

    }

    private void CreateStar(string _name, Vector3 starPosition)
    {
        name = _name;

        // Create star

        SolarSubTypes starSubType;
        double sunMass;
        double temp;
        float sunDensity;

        float star = Random.value;
        if (star < .5f)
        {
            starSubType = SolarSubTypes.MainSequence;
            sunMass = Random.Range(.1f, 60);
            temp = (42000f / 599f) * sunMass + (1755000 / 599);
            sunMass *= GameDataModel.sunMassMultiplication; // Sun Mass in solar masses
            sunDensity = Random.Range(.5f, 4) * Units.k;
        }
        else if (star < .75f)
        {
            starSubType = SolarSubTypes.GasGiant;
            sunMass = Random.Range(60f, 100);
            temp = Random.Range(4000, 45000);
            sunMass *= GameDataModel.sunMassMultiplication; // Sun Mass in solar masses
            sunDensity = Random.Range(.1f, 2) * Units.k;
        }
        else
        {
            starSubType = SolarSubTypes.WhiteDwarf;
            sunMass = Random.Range(.01f, 1);
            temp = Random.Range(6000, 30000);
            sunMass *= GameDataModel.sunMassMultiplication; // Sun Mass in solar masses
            sunDensity = Random.Range(10f, 2000) * Units.k;
        }
        //float sunColorValue = (float) (sunMass / 100 / GameDataModel.sunMassMultiplication);
        //Color color = sunSizeColor.Evaluate(sunColorValue);
        Color color = ColorTempToRGB.TempToRGB((float)temp);
        double sunRadius = Mathd.Pow(((sunMass / sunDensity) * 3) / (4 * Mathd.PI), 1 / 3d) / Units.convertToMeters; // In km

        SolarBody solar = new SolarBody(_name + " " + starSubType.ToString(), (Vector3d) starPosition, starSubType, sunMass , sunRadius, color, temp);

        int numPlanets = (int)Random.Range(0, (float)Mathd.Pow(sunMass / GameDataModel.sunMassMultiplication, .25f) * 20);
        double sunSoi = solar.GetSOI();

        for (int i = 0; i < numPlanets; i++)
        {
            double sma = Random.Range((float) (solar.GetBodyRadius()) * 1.1f, (float) (sunSoi * .1));
            float lpe = Random.Range(0, 2 * Mathf.PI);
            float lap = Random.Range(0, 2 * Mathf.PI);
            float inc = Random.Range(0, .027f * Mathf.PI);
            float satType = Random.value;
            // planet mass in kg
            double planetMass;
            float planetDensity;
            SolarTypes planetType = SolarTypes.Planet;
            SolarSubTypes planetSubType = SolarSubTypes.Rocky;
            float ecc = Random.Range(0, .01f);

            if (satType < .3f) //Rock planets
            {
                inc = Random.Range(0, .027f * Mathf.PI);
                planetMass = Random.Range(1f, 100f) * 1e+23;
                planetDensity = Random.Range(3.5f, 7) * Units.k * Units.convertToMeters;
            }
            else if (satType < .4f) //Gas Giants
            {
                inc = Random.Range(0, .0167f * Mathf.PI);
                planetDensity = Random.Range(.5f, 3) * Units.k * Units.convertToMeters;
                planetSubType = SolarSubTypes.GasGiant;
                planetMass = Random.Range(1, 400) * 1e+25;
            }
            else if (satType < .6f) //Dwarf Planets
            {
                inc = Random.Range(0, .25f * Mathf.PI);
                planetType = SolarTypes.DwarfPlanet;
                planetDensity = Random.Range(3.5f, 7) * Units.k * Units.convertToMeters;
                planetMass = Random.Range(1, 40) * 1e+21;
                ecc = Random.Range(0.01f, .5f);
            }
            else //Comets
            {
                inc = Random.Range(0, .94f * Mathf.PI);
                planetDensity = Random.Range(3.5f, 7) * Units.k * Units.convertToMeters;
                planetType = SolarTypes.Comet;
                planetMass = Random.Range(1, 100) * 1e+13;
                ecc = Random.Range(0.5f, 1);
            }

            double planetRadius = Mathd.Pow((((planetMass) / planetDensity) * 3) / (4 * Mathd.PI), 1 / 3d) / Units.convertToMeters; // In meters

            color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));

            solar.Satelites.Add(new SolarBody(name + " " + planetType.ToString() + " " + (i + 1), solar.GetId(), planetType, planetSubType, planetMass, planetRadius,sma,ecc,lpe,0,lpe,inc,lap,color, solar));

            int numMoonRange = 0;
            float moonChance = 0;

            if (planetSubType == SolarSubTypes.GasGiant)
            {
                numMoonRange = 30;
                moonChance = 1;
            }
            else if (planetType == SolarTypes.DwarfPlanet)
            {
                numMoonRange = 2;
                moonChance = .1f;
            }
            else if (planetSubType == SolarSubTypes.Rocky)
            {
                numMoonRange = 5;
                moonChance = .5f;
            }
            int numMoon = Random.Range(0, numMoonRange);

            double soi = solar.Satelites[i].GetSOI();

            if (Random.value < moonChance)
            {
                for (int m = 0; m < numMoon; m++)
                {
                    double moonMass = Random.Range(.0001f, 50) * 1e+22;

                    sma = Random.Range((float)solar.Satelites[i].GetBodyRadius() * 1.1f, (float)(soi * .1f));
                    lpe = Random.Range(0, 2 * Mathf.PI);
                    lap = Random.Range(0, 2 * Mathf.PI);
                    inc = Random.Range(0, .06f * Mathf.PI);
                    color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
                    float moonDensity = Random.Range(3.5f, 7) * Units.k * Units.convertToMeters;
                    double moonRadius = Mathd.Pow((((moonMass) / moonDensity) * 3) / (4 * Mathd.PI), 1 / 3d) / Units.convertToMeters; // In meters
                    SolarTypes moonType = SolarTypes.Moon;
                    SolarSubTypes moonSubType = SolarSubTypes.Rocky;
                    ecc = Random.Range(0, .02f);
                    if (moonMass < 1e+16)
                    {
                        moonType = SolarTypes.Asteroid;
                        ecc = Random.Range(0.02f, .5f);
                    }

                    solar.Satelites[i].Satelites.Add(new SolarBody(name + "Moon " + (i + 1), solar.Satelites[i].GetId(), moonType, moonSubType, moonMass, moonRadius, sma, ecc, lpe, 0, lpe, inc, lap, color, solar));
                }
            }
            
        }

        game.data.stars.Add(solar);
    }
    
    
    public static double NextGaussianDouble(double seed)
    {
        double U, u, v, S;
        var random = new System.Random(seed.GetHashCode());
        do
        {
            u = 2.0 * random.NextDouble() - 1.0;
            v = 2.0 * random.NextDouble() - 1.0;
            S = u * u + v * v;
        }
        while (S >= 1.0);

        double fac = Mathd.Sqrt(-2.0 * Mathd.Log(S) / S);
        return u * fac;
    }

    public static double NormalizedRandom(double minValue, double maxValue, double seed)
    {
        var mean = (minValue + maxValue) / 2;
        var sigma = (maxValue - mean) / 3;
        return nextRandom(mean, sigma, seed);
    }

    private static double nextRandom(double mean, double sigma, double seed)
    {
        var standard = NextGaussianDouble(seed) + mean;

        var value = standard * sigma;

        if (value < mean - 3 * sigma)
        {
            return mean - 3 * sigma;
        }
        if (value > mean + 3 * sigma)
        {
            return mean + 3 * sigma;
        }
        return value;
    }
}
