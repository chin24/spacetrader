﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlanetTile {
    [SerializeField]
    private PlanetTileType planetTileType;
    [SerializeField]
    private int count;
    //TODO: Explore if turning this into a strut would be more efficient

    public PlanetTile() { }


    public PlanetTile(SolarSubTypes solarSubType)
    {
        SetCount(1);
        if (solarSubType == SolarSubTypes.Rocky)
        {
            var rand = Random.value;
            if (rand < .85)
            {
                SetPlanetTileType(PlanetTileType.Rocky);
            }
            else if (rand < .9)
            {
                SetPlanetTileType(PlanetTileType.Ice);
            }
            else if (rand < .95)
            {
                SetPlanetTileType(PlanetTileType.Desert);
            }
            else
            {
                SetPlanetTileType(PlanetTileType.Volcanic);
            }
        }
        else if (solarSubType == SolarSubTypes.EarthLike)
        {
            var rand = Random.value;
            if (rand < .75)
            {
                SetPlanetTileType(PlanetTileType.Ocean);
            }
            else if (rand < .85)
            {
                SetPlanetTileType(PlanetTileType.Grasslands);
            }
            else if (rand < .88)
            {
                SetPlanetTileType(PlanetTileType.Desert);
            }
            else if (rand < .93)
            {
                SetPlanetTileType(PlanetTileType.Ice);
            }
            else if (rand < .96)
            {
                SetPlanetTileType(PlanetTileType.Rocky);
            }
            else
            {
                SetPlanetTileType(PlanetTileType.Volcanic);
            }
        }
        else if (solarSubType == SolarSubTypes.Ice)
        {
            var rand = Random.value;
            if (rand < .85)
            {
                SetPlanetTileType(PlanetTileType.Ice);
            }
            else if (rand < .95)
            {
                SetPlanetTileType(PlanetTileType.Rocky);
            }
            else
            {
                SetPlanetTileType(PlanetTileType.Ocean);
            }
        }
        else if (solarSubType == SolarSubTypes.Desert)
        {
            var rand = Random.value;
            if (rand < .85)
            {
                SetPlanetTileType(PlanetTileType.Desert);
            }
            else if (rand < .95)
            {
                SetPlanetTileType(PlanetTileType.Rocky);
            }
            else
            {
                SetPlanetTileType(PlanetTileType.Volcanic);
            }
        }
        else if (solarSubType == SolarSubTypes.Ocean)
        {
            var rand = Random.value;
            if (rand < .9)
            {
                SetPlanetTileType(PlanetTileType.Ocean);
            }
            else if (rand < .95)
            {
                SetPlanetTileType(PlanetTileType.Rocky);
            }
            else
            {
                SetPlanetTileType(PlanetTileType.Ice);
            }
        }
        else if (solarSubType == SolarSubTypes.Volcanic)
        {
            var rand = Random.value;
            if (rand < .70)
            {
                SetPlanetTileType(PlanetTileType.Volcanic);
            }
            else if (rand < .95)
            {
                SetPlanetTileType(PlanetTileType.Rocky);
            }
            else
            {
                SetPlanetTileType(PlanetTileType.Desert);
            }
        }
        else
        {
            throw new System.Exception("Unknown Subtype");
        }
    }

    public PlanetTileType GetPlanetTileType()
    {
        return planetTileType;
    }


    private void SetPlanetTileType(PlanetTileType value)
    {
        planetTileType = value;
    }



    public int GetCount()
    {
        return count;
    }

    public void SetCount(int value)
    {
        count = value;
    }

    public string GetInfo()
    {
        string resources = "Resources:\n";
        //foreach (RawResourceBlueprint resource in rawResources)
        //{
        //    resources += string.Format("{0} - {1} kg - {2}\n",
        //        resource.rawResourceType.ToString(),
        //        resource.amount.ToString("G"),
        //        resource.accesibility.ToString("0.00"));
        //}

        return string.Format("Tile Type: {0}\n{1}",
            GetPlanetTileType().ToString(),
            resources);
    }    
}


