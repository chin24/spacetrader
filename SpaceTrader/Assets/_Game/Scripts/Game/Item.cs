﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item
{
    public string id;
    public double amount;
    public string referenceId;
    public string destinationId;

    public string Name
    {
        get { return Blueprint.Name; }
    }
    public double EstimatedValue
    {
        get
        {
            return Blueprint.EstimatedValue;
        }
    }
    public float Size
    {
        get
        {
            return Blueprint.size;
        }
    }

    public double Mass
    {
        get
        {
            return Blueprint.mass;
        }
    }

    public ItemBlueprint Blueprint
    {
        get
        {
            return GameManager.instance.data.blueprintsModel.Model.GetItem(id);
        }
    }

    public double AmountSize { get { return amount * Size; } }
    /// <summary>
    /// Item size in meters squared.
    /// </summary>
    public SizeClass ItemSize
    {
        get
        {
            if (Size < 10) return SizeClass.S;
            if (Size < 100) return SizeClass.M;
            if (Size < 1000) return SizeClass.L;
            if (Size < 10000) return SizeClass.XL;
            else return SizeClass.T;
        }
    }

    public Item() { }

    public Item(string _itemId, double _amount, string referenceId, string _destinationId)
        : this(_itemId, _amount, referenceId)
    {
        this.destinationId = _destinationId;
    }

    public Item(string _itemId, double _amount, string referenceId)
        : this(_itemId, _amount)
    {
        this.referenceId = referenceId;
    }
    public Item(string _itemId, double _amount)
    {
        id = _itemId;
        amount = _amount;
    }
    public void SetAmount(double _amount)
    {
        amount = _amount;
    }

    public void AddAmount(double _amount)
    {
        if (_amount <= 0)
            return;
        amount += _amount;
    }

    public double RemoveAmount(double _amount)
    {

        if (amount - _amount < 0)
        {
            double removedAmount = amount;
            amount = 0;
            return removedAmount;
        }
        amount -= _amount;
        return _amount;
    }

    public double GetBaseArmor()
    {
        return GameManager.instance.data.blueprintsModel.Model.GetItem(id).baseArmor;
    }
}

public enum ItemType
{
    BuildingMaterial,
    Fuel,
    Engine,
    Shield,
    Weapon,
    Container,
    FuelTank,
    WarpDrive,
    CommandCapsule,
    Sensor,
    FactoryMachinery,
    AI,
    RawMaterial,
    Storage,
    ProductionStructure,
    SpaceStation,
    SpaceShip,
    DistributionCenter,
    StorageContainer
}

public enum SizeClass
{
    S,
    M,
    L,
    XL,
    T
}