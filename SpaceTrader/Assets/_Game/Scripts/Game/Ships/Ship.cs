﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Ship : Structure, IWorkers, IGoap
{
    [SerializeField]
    private MoveDel moveFunction;
    [SerializeField]
    private string _shipTargetId;
    private bool hasRoute;
    private RouteNode[] shipRoute;
    private int shipRouteIndex;
    private bool requestSent;
    private bool redo;
    [SerializeField]
    private bool _docked;
    [SerializeField]
    private int _workers;
    [SerializeField]
    private double maxUpForce = 10;
    [SerializeField]
    private double maxDownForce = 10;
    [SerializeField]
    private double maxForwardForce = 1000;
    [SerializeField]
    private double maxBackwardForce = 100;
    [SerializeField]
    /// <summary>
    /// Velocity in Km/s
    /// </summary>
    private Vector3d velocity = Vector3d.zero;
    [SerializeField]
    private Quaternion rotation = Quaternion.identity;
    [SerializeField]
    /// <summary>
    /// Acceleration in Km/s^2
    /// </summary>
    private Vector3d acceleration = Vector3d.zero;
    [SerializeField]
    /// <summary>
    /// Force in N
    /// </summary>
    private Vector3d force = Vector3d.zero;
    [SerializeField]
    private double _workerPayRate;

    public Fuel fuel;
    public int fuelCapacity = 100;
    public double idleTime;
    public ShipAction shipAction = ShipAction.Nothing;
    public bool hyperSpace;
    public Vector3d dockPosition;
    public string contractId;
    public GoapAgent agent;
    public List<int> passengerIds;

    public string ShipTargetId { get => _shipTargetId; set => _shipTargetId = value; }
    public ShipType shipType { get { return GetShipBlueprint().shipType; } }
    public double Mass { get { return GetShipBlueprint().mass + storage.Mass; } }
    /// <summary>
    /// Is the entity docked?
    /// </summary>
    public bool Docked { get => _docked; set => _docked = value; }
    /// <summary>
    /// returns the reference Id as a Supply Structure
    /// </summary>
    public SupplyStructure ReferenceStructure { get { return GameManager.instance.locations[GetReferenceId()] as SupplyStructure; } }



    public int workers { get => _workers; set => _workers = value; }

    public double workerPayRate { get => _workerPayRate; set => _workerPayRate = value; }

    //Ship Properties

    public int PassangerCapacity { get { return GetShipBlueprint().passangerCapacity; } }
    

    /// <summary>
    /// Sub light speed in km/s
    /// </summary>
    public float SubLightSpeed { get { return GetShipBlueprint().subLightSpeed; } }
    /// <summary>
    /// Rotate speed in degrees per second;
    /// </summary>
    public float rotateSpeed { get { return GetShipBlueprint().rotateSpeed; } }

    /// <summary>
    /// Number of km for one unit of fuel;
    /// </summary>
    public double fuelRange { get { return GetShipBlueprint().fuelRange; } }
    

    /// <summary>
    /// Speed of ship in km/s
    /// </summary>
    public double Speed { get { return velocity.magnitude; } }




    //Ship Properties
    public double ApproxFuelCostPerKm
    {
        get { return fuel.EstimatedValue / fuelRange; }
    }

    public Ship() { }

    public Ship(string shipBlueprintId, IdentityModel owner, Creature captain, string _referenceId, int _count = 1) :
        base(owner, shipBlueprintId, _referenceId, Vector3d.zero)
    {
        this.ManagerId = captain.GetId();
        this.Count = _count;
        //this.captain.Model.location = this;
        this.owner.Model.ownedShipIds.Add(GetId());
        GameManager.instance.data.ships.Add(this);

        //Get Blueprint

        ShipBlueprint ship = GetShipBlueprint();
        SetName(ship.Name + " " + ship.count++);


        //workers
        this.workers = ship.workers;
        workerPayRate = ship.workerPayRate;



        //Fuel
        this.fuelCapacity = 100;
        this.fuel = new Fuel(GetShipBlueprint().fuelBlueprintId, fuelCapacity, GetId());

        //Agent
        if (GetShipBlueprint().research == "Cargo Ship")
        {
            //Ship Storage
            storage = new Storage(StorageType.All, new Dictionary<string, double>() { { "All", 1000 } });

            agent = new GoapAgent(this, new GoapAction[] {
                new LoadContractItems(this),
                new DeliverContractItem(this)
            });
        }

        else if (GetShipBlueprint().research == "Driller Ship")
        {
            //Ship Storage
            storage = new Storage(StorageType.All, new Dictionary<string, double>() { { "All", 1000 } });

            agent = new GoapAgent(this, new GoapAction[] { //TODO: Create actions for driller ship. Including creating and fulfilling a contract
                new DrillLocationAction(this),
                new SellItemContractAction(this),
                new DeliverContractItem(this)
        });
        }
    }

    public PositionEntity GetShipTarget() { return (ShipTargetId != null) ? GameManager.instance.locations[ShipTargetId] : null; }

    public ShipBlueprint GetShipBlueprint()
    { return GameManager.instance.data.blueprintsModel.Model.GetItem(structureBlueprintId) as ShipBlueprint; }

    public void SetShipAction(ShipAction action)
    {
        shipAction = action;
    }

    public RouteNode[] GetShipRoute()
    {
        return shipRoute;
    }

    public int GetShipRouteIndex()
    {
        return shipRouteIndex;
    }

    public void RefuelShip()
    {
        Structure structure = GameManager.instance.locations[GetReferenceId()] as Structure;
        if (structure != null && structure.storage.itemsStorage.ContainsKey("Fuel Cell"))
        {
            while (structure.storage.RemainingCapacity("Fuel Cell") < structure.storage.itemCapacity["Fuel Cell"][1])
            {
                var fuels = structure.storage.itemsStorage["Fuel Cell"];
                var fuel = fuels[0];
                if (fuel.amount >= fuelCapacity - this.fuel.amount)
                {
                    structure.storage.RemoveAmount(fuel.id, fuelCapacity - this.fuel.amount);
                    this.fuel.amount = fuelCapacity;
                    break;
                }
                else
                {
                    this.fuel.amount += fuel.amount;
                    structure.storage.Remove(fuel);
                }
            }
        }
    }

    public virtual HashSet<KeyValuePair<string, object>> getWorldState()
    {
        HashSet<KeyValuePair<string, object>> worldData = new HashSet<KeyValuePair<string, object>>
        {
            new KeyValuePair<string, object>("position", GetReferenceId()),
            new KeyValuePair<string, object>("hasStorage", true),
            new KeyValuePair<string, object>("hasTradeItems", (storage.itemCapacity["All"][0] > 0)),
            new KeyValuePair<string, object>("hasContract", contractId != null),
            new KeyValuePair<string, object>("hasFuel", fuel.amount > fuelCapacity * .25)
        };

        return worldData;
    }

    public virtual HashSet<KeyValuePair<string, object>> createGoalState()
    {
        HashSet<KeyValuePair<string, object>> goal = new HashSet<KeyValuePair<string, object>>();

        goal.Add(new KeyValuePair<string, object>("profit", true));
        return goal;
    }

    public virtual void planFailed(HashSet<KeyValuePair<string, object>> failedGoal)
    {
        // Not handling this here since we are making sure our goals will always succeed.
        // But normally you want to make sure the world state has changed before running
        // the same goal again, or else it will just fail.
    }

    public virtual void planFound(HashSet<KeyValuePair<string, object>> goal, Queue<GoapAction> actions)
    {
        // Yay we found a plan for our goal
        Debug.Log("<color=green>Plan found</color> " + GoapAgent.prettyPrint(actions));
    }

    public virtual void actionsFinished()
    {
        // Everything is done, we completed our actions for this gool. Hooray!
        Debug.Log("<color=blue>Actions completed</color>");
        ShipTargetId = null;
    }

    public virtual void planAborted(GoapAction aborter)
    {
        // An action bailed out of the plan.State has been reset to plan again.
        // Take note of what happened and make sure if you run the same goal again
        // that it can succeed.
        Debug.Log("<color=red>Plan Aborted</color> " + GoapAgent.prettyPrint(aborter));
        ShipTargetId = null;
    }

    public virtual bool moveAgent(GoapAction nextAction)
    {
        bool targetReached = false;

        if (!hasRoute && (!requestSent || redo))
        {
            //reset request
            if (redo)
            {
                redo = false;
                requestSent = false;
            }

            //Check to see if already at target
            SupplyStructure structure = GameManager.instance.locations[GetReferenceId()] as SupplyStructure;
            if (structure != null && structure.GetId() == nextAction.target.GetId())
            {
                targetReached = true;
            }
            else
            {
                ShipTargetId = nextAction.target.GetId();
                //Request a path
                if (RouteRequestManager.RequestRoute(this, StartShipRoute))
                {
                    requestSent = true;
                }

            }
        }

        if (hasRoute)
        {
            requestSent = false;
            moveFunction();
            if (shipRouteIndex >= shipRoute.Length)
            {
                //Finished running route, should be in the correct position
                targetReached = true;
                hasRoute = false;
            }

            //Apply movement to ship and fuel usage
            velocity += force * deltaTime * .001 / Mass;
            fuel.amount -= force.x;
            fuel.amount -= force.y;
            fuel.amount -= force.z;

            //Apply velocity to position
            SetReferencePosition(GetReferencePosition() + velocity * deltaTime);
        }

        //Reached Location

        if (targetReached)
        {
            if (GetReferenceId() != nextAction.target.GetId())
            {
                Debug.Log("Something went wrong with reference setting, should be " + nextAction.target.GetName() + " instead of " + GetReferenceObj().GetName());

                //Move to correct reference
                SetReferenceId(nextAction.target);
            }
            // we are at the target location, we are done
            if (GameManager.instance.debugLog)
                Debug.Log("Location Reached");

            ShipTargetId = null;
            nextAction.setInRange(true);
            // reset request
            requestSent = false;
            return true;
        }
        else
            return false;
    }

    private void UndockShip()
    {
        //Should be referenced to structure
        SupplyStructure structure = GameManager.instance.locations[GetReferenceId()] as SupplyStructure;
        SetShipAction(ShipAction.Undocking);
        //Undock
        if (Docked)
        {
            bool undocked = structure.UndockShip(this);
            //Debug.Log("Undocking attempt: " + undocked);
        }


        //Vertical lift off
        //if ((referencePosition - shipRoute[shipRouteIndex].referencePosition).sqrMagnitude < .1 * .1)
        //{
        //    if (Speed < ReferenceObj.speedLimit)
        //    VerticalThrust(1);
        //}
        //else if (StopMovement())
        //{
        //    //Check if station is the same station the target wants
        //    if (structure.StationHasStructure(shipRoute[shipRoute.Length - 1].currentNodeId))
        //    {
        //        shipRouteIndex = shipRoute.Length - 1;
        //        moveFunction = () => DockWithStructure();
        //    }
        //    else
        //    {
        //        // Undocking complete, move to next action
        //        SetNextMoveFunction();
        //    }
        //}
        // Temporary skip physical undock movement
        SetNextMoveFunction();
    }

    private void SetNextMoveFunction()
    {
        shipRouteIndex++;
        if (shipRouteIndex >= shipRoute.Length)
        {
            //Route complete, the ship should be where it needs to be
            return;
        }
        if (shipRoute[shipRouteIndex].nodeType == RouteNodeType.Dock)
        {
            moveFunction = () => DockWithStructure();
        }
        else if (shipRoute[shipRouteIndex].nodeType == RouteNodeType.Undock)
        {
            moveFunction = () => UndockShip();
        }
        else if (shipRoute[shipRouteIndex].nodeType == RouteNodeType.LeaveSOI)
        {
            moveFunction = () => LeaveSOI();
        }
        else if (shipRoute[shipRouteIndex].nodeType == RouteNodeType.EnterSOI)
        {
            moveFunction = () => EnterSOI();
        }
        else if (shipRoute[shipRouteIndex].nodeType == RouteNodeType.Arrive)
        {
            moveFunction = () => ShipArrive();
        }
        else
        {
            throw new Exception("Cannot find Node type function requested");
        }
    }

    /// <summary>
    /// Ship has arrived at location
    /// </summary>
    private void ShipArrive()
    {
        StopMovement();

        //Set Next Move function
        SetNextMoveFunction();
    }

    private void EnterSOI()
    {
        SetReferencePosition(Vector3d.MoveTowards(GetReferencePosition(), GameManager.instance.locations[shipRoute[shipRouteIndex].currentNodeId].GetReferencePosition(), SubLightSpeed * deltaTime));
        SetShipAction(ShipAction.Moving);
        //reach edge of SOI
        if ((GetReferencePosition() - GameManager.instance.locations[shipRoute[shipRouteIndex].currentNodeId].GetReferencePosition()).sqrMagnitude <= shipRoute[shipRouteIndex].distanceTarget * shipRoute[shipRouteIndex].distanceTarget)
        {
            //Move out of the SOI of the reference to into the target SOI
            SetReferenceId(shipRoute[shipRouteIndex].currentNodeId);
            SetNextMoveFunction();
        }
    }

    private void LeaveSOI()
    {
        SetReferencePosition(Vector3d.MoveTowards(GetReferencePosition(), shipRoute[shipRouteIndex].referencePosition, SubLightSpeed * deltaTime));

        //reach edge of SOI
        if (GetReferencePosition().Equals(shipRoute[shipRouteIndex].referencePosition))
        {
            //Move out of the SOI of the reference to the top level SOI
            SetReferenceId(GetReferenceObj().GetReferenceId());
            SetNextMoveFunction();
        }
    }

    private void DockWithStructure()
    {
        //Look for available dock
        SetShipAction(ShipAction.LookingForDock);

        if (ReferenceStructure.DockAvailable(shipRoute[shipRoute.Length - 1].currentNodeId))
        {
            // Found dock
            // Switch reference to station
            SetReferenceId(shipRoute[shipRoute.Length - 1].currentNodeId);
            // Request Dock Position
            dockPosition = ReferenceStructure.ClaimDock(this);
            SetShipAction(ShipAction.Docking);
            //Start Docking function;
            moveFunction = () => InitialDockingOrientation();

        }
    }

    private void InitialDockingOrientation()
    {
        Quaternion desiredRotation = Quaternion.LookRotation((Vector3)(dockPosition + Vector3d.up * 40 * .001 - GetReferencePosition()));
        rotation = Quaternion.RotateTowards(rotation, desiredRotation, (float)((10 + rotateSpeed) * deltaTime));
        if (rotation.Equals(desiredRotation))
        {
            moveFunction = () => ApproachDockingPad();
        }
    }

    private void ApproachDockingPad()
    {
        Vector3d desiredPostion = dockPosition + Vector3d.up * 40 * .001;
        //if (Speed < ReferenceObj.speedLimit)
        //{
        //    VerticalThrust(1);
        //}
        //Temporary fix for docking
        SetReferencePosition(Vector3d.MoveTowards(GetReferencePosition(), desiredPostion, SubLightSpeed * .1f * deltaTime));
        if ((GetReferencePosition() - desiredPostion).sqrMagnitude < .01 * .01)
        {
            StopMovement();
            moveFunction = () => VerticalLanding();
        }
    }

    private void VerticalLanding()
    {
        //if (Speed < 5 * .001)
        //{
        //    VerticalThrust(-1);
        //}
        // Temporary fix for docking
        SetReferencePosition(Vector3d.MoveTowards(GetReferencePosition(), dockPosition, SubLightSpeed * .1f * deltaTime));
        if ((GetReferencePosition() - dockPosition).sqrMagnitude < .01 * .01)
        {
            StopMovement();
            ReferenceStructure.DockShip(this);
            SetShipAction(ShipAction.Docked);
            //Landing complete Set Next Move function
            SetNextMoveFunction();
        }
    }

    private bool StopMovement()
    {
        //TODO: create an actual stop movement function that decelerates the ship
        velocity = Vector3d.zero;
        force = Vector3d.zero;
        acceleration = Vector3d.zero;
        return true;
    }

    private void ForwardThrust(float power)
    {
        force.x += maxForwardForce * power;
    }
    private void VerticalThrust(float power)
    {
        force.z += maxUpForce * power;
    }

    protected delegate void MoveDel();
    /// <summary>
    /// Returns ETA in seconds to from current reference position to given reference position.
    /// ASSUMES THAT VELOCITY POINTS TOWARDS TARGET POISITION 
    /// </summary>
    /// <param name="targetReferencePosition"></param>
    /// <returns></returns>
    public double GetETATo(Vector3d targetReferencePosition)
    {
        return (Vector3d.Distance(targetReferencePosition, this.GetReferencePosition()) / velocity.magnitude);
        //TODO: More accurate ETA that removes assumption
    }



    private void StartShipRoute(Ship model, RouteNode[] route, bool success)
    {
        requestSent = false;
        if (success)
        {
            hasRoute = true;
            shipRoute = route;
            shipRouteIndex = 0;

            //Set the first moveFunction

            if (Docked)
            {
                if (model.ReferenceStructure == null)
                {
                    throw new Exception("Not referenced to a structure");
                }
                moveFunction = () => UndockShip();
            }
            else
            {
                //Allows it to start from beginning and select the correct move function
                shipRouteIndex = -1;
                SetNextMoveFunction();
            }
        }
        else
        {
            //throw new Exception("Could not find route for ship: " + Name);
            requestSent = false;
            redo = true;
            Debug.Log("ERROR: Could not find route for ship: " + GetName() + " - id: " + GetId());
        }
    }
}

public enum ShipAction
{
    Moving,
    Undocking,
    LookingForDock,
    Docking,
    Docked,
    Nothing,
    Working
}
