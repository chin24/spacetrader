﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrillerShip : Ship {



    public override HashSet<KeyValuePair<string, object>> getWorldState()
    {
        HashSet<KeyValuePair<string, object>> worldData = new HashSet<KeyValuePair<string, object>>
        {
            new KeyValuePair<string, object>("position", GetReferenceId()),
            new KeyValuePair<string, object>("hasStorage", true),
            new KeyValuePair<string, object>("hasContract", contractId != null),
            new KeyValuePair<string, object>("hasTradeItems", (storage.itemCapacity["All"][0] > 0))
        };

        return worldData;
    }

    public override HashSet<KeyValuePair<string, object>> createGoalState()
    {
        HashSet<KeyValuePair<string, object>> goal = new HashSet<KeyValuePair<string, object>>();

        goal.Add(new KeyValuePair<string, object>("profit", true));
        return goal;
    }
}
