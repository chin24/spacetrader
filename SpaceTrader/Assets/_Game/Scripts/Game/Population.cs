﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Population
{
    [SerializeField]
    private int totalPopulation;
    [SerializeField]
    private int young;
    [SerializeField]
    private int adult;
    [SerializeField]
    private int old;

    public Population() { }

    public Population(SolarBody body)
    {
        SetYoung(Random.Range(1000, 1000000));
        SetAdult(Random.Range(1000, 10000000));
        SetOld(Random.Range(1000, 1000000));
        SetTotalPopulation(GetYoung() + GetAdult() + GetOld());
        //requiredItems = new List<Item>() { new Item(defaultRawResources.Goldium.ToString(), (int)defaultRawResources.Goldium, ItemType.RawMaterial, (int)(adult * .0001f), body.GetMarketPrice((int)defaultRawResources.Goldium)) };
    }

    // Update is called once per frame
    public void Update(SolarBody body, double deltaTime)
    {

        var toAdult = GetYoung() * deltaTime / Dated.Day * .000137;
        var toOld = GetAdult() * deltaTime / Dated.Day * .000061;
        var toDeath = GetOld() * deltaTime / Dated.Day * .00011;
        var accidentYoung = GetYoung() * deltaTime / Dated.Day * .00001;
        var accidentAdult = GetAdult() * deltaTime / Dated.Day * .00002;
        var accidentOld = GetOld() * deltaTime / Dated.Day * .0001;
        var birth = GetAdult() * deltaTime / Dated.Day * .00015;

        SetYoung((int)(GetYoung() - toAdult - accidentYoung + birth));
        SetAdult((int)(GetAdult() + toAdult - toOld - accidentAdult));
        SetOld((int)(GetOld() + toOld - toDeath - accidentOld));

        SetTotalPopulation(GetYoung() + GetAdult() + GetOld());
        //SolarModel solar = GameManager.instance.data.stars[body.solarIndex[0]];
        //foreach (Item item in requiredItems)
        //{
        //    var found = false;
        //    foreach (Structure structure in body.groundStructures)
        //    {
        //        if (structure.structureType == Structure.StructureTypes.GroundStorage)
        //        {
        //            GroundStorage groundStruct = (GroundStorage)structure;
        //            if (groundStruct.owner.Model == solar.government.Model && groundStruct.RemoveItem((Item)item))
        //            {
        //                body.RemoveBuying((int)item.id, solar.government.Model, -1, (double)item.amount);
        //                body.RemoveSelling((int)item.id, solar.government.Model, -1, (double)item.amount);
        //                found = true;
        //            }
        //        }
        //    }
        //    //Runs if there are still needed Items
        //    if (!found)
        //    {
        //        body.SetBuying(item);
        //        item.price += item.price * GameManager.instance.marketPriceMod * deltaTime * 68;
        //        if (item.price < .1)
        //        {
        //            item.price = .1f;
        //        }
        //    }
        //    else
        //    {
        //        body.SetBuying(item);
        //        item.price = body.GetMarketPrice(item.id);
        //        if (item.price < .1)
        //        {
        //            item.price = .1f;
        //        }
        //    }
        //}

    }

    public int GetOld()
    {
        return old;
    }

    private void SetOld(int value)
    {
        old = value;
    }

    public int GetAdult()
    {
        return adult;
    }

    private void SetAdult(int value)
    {
        adult = value;
    }

    public int GetTotalPopulation()
    {
        return totalPopulation;
    }

    private void SetTotalPopulation(int value)
    {
        totalPopulation = value;
    }

    public int GetYoung()
    {
        return young;
    }

    private void SetYoung(int value)
    {
        young = value;
    }
}
