﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBlueprint : ItemBlueprint, IWorkers
{
    public ShipType shipType; //TODO: Move to ship research class

    public int passangerCapacity;

    /// <summary>
    /// Sub light speed in km/s
    /// </summary>
    public float subLightSpeed;
    /// <summary>
    /// Rotate speed in degrees per second;
    /// </summary>
    public float rotateSpeed;

    public string fuelBlueprintId;
    public double fuelRange;
    public int fuelCapacity = 100;
    internal double cargoCapacity = 1000;

    //Ship Properties
    public double ApproxFuelCostPerKm
    {
        get { return Fuel.EstimatedValue / fuelRange; }
    }

    public FuelBlueprint Fuel { get { return GameManager.instance.data.blueprintsModel.Model.GetItem(fuelBlueprintId) as FuelBlueprint; } }

    public int workers { get; set; }

    public double workerPayRate { get; set; }

    public ShipBlueprint() { }

    public ShipBlueprint(string name, string research, IdentityModel createdBy, int workAmount, float mass, float _size, float _armor, StringIntDictionary _constructionBlueprints, List<string> _supportedBlueprints = null)
        :base(name,research, createdBy, workAmount, mass, _size,_armor,_constructionBlueprints, _supportedBlueprints)
    {
        this.workers = 10;
        workerPayRate = 15 / Dated.Hour;

        this.passangerCapacity = 10;
        cargoCapacity = 1000; //TODO: Base ship capacity on the size of the ship/the size of the storage compartment in the ship

        this.subLightSpeed = 1000;

        this.fuelRange = 100 * Units.G;
        this.fuelCapacity = 100;
        FuelBlueprint fuelBlueprint = GameManager.instance.data.blueprintsModel.Model.blueprints.Find(x => x.GetResearch().name == "Fuel Cell") as FuelBlueprint;
        fuelBlueprintId = fuelBlueprint.Id;
        fuelRange *= fuelBlueprint.fuelRangeMod;
    }
}

public enum ShipType
{
    Cargo,
    Explorer,
    Combat,
    Capital,
}
