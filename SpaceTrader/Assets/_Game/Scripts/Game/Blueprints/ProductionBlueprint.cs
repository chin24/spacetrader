﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductionBlueprint : ItemBlueprint, IWorkers
{
    public int workers { get; set; }

    public double workerPayRate { get; set; }
	
	public ProductionBlueprint(string name, string research, IdentityModel createdBy, int workAmount, float mass, float _size, float _armor, StringIntDictionary _constructionBlueprints, List<string> _supportedBlueprints = null)
        : base(name, research, createdBy, workAmount, mass, _size, _armor, _constructionBlueprints, _supportedBlueprints)
    {
		if (size < 10)
        {
            Debug.Log("Production structure too small " + size);
            size = 10;
        }
			
		workerPayRate = 10 / Dated.Hour;
        //workers = (int) (this.size / 10); //10 is the average space a person takes up + space for other amenities
        workers = 300;
	}
}