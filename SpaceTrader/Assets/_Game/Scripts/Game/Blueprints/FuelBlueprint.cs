﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelBlueprint : ItemBlueprint
{
    /// <summary>
    /// Percent modification of ship fuel range.
    /// </summary>
    public float fuelRangeMod;

    public FuelBlueprint() { }

    public FuelBlueprint(string _name, string _research, IdentityModel _createdBy, int workAmount, float mass, float _fuelRangeMod, float _size, float _armor, StringIntDictionary _constructionBlueprints, List<string> _supportedBlueprints = null) :
        base(_name,_research,_createdBy,workAmount,mass,_size,_armor, _constructionBlueprints, _supportedBlueprints)
    {
        this.fuelRangeMod = _fuelRangeMod;
    }
}

public class Fuel : Item
{
    /// <summary>
    /// Percent modification of ship fuel range.
    /// </summary>
    public double fuelRangeMod;

    public Fuel() { }
    public Fuel(string _itemId, double _amount, string _structureId) :
        base(_itemId, _amount, _structureId)
    {
        fuelRangeMod = ((FuelBlueprint)GameManager.instance.data.blueprintsModel.Model.GetItem(_itemId)).fuelRangeMod;
    }
}
