﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemBlueprint
{
    [SerializeField]
    private string _name;
    [SerializeField]
    private string _id;
    [SerializeField]
    private double _estimatedValue;
    [SerializeField]
    private StringIntDictionary _constructionBlueprints;

    public string research;
    public int count;

    public Color color;
    public string coloredName;

    public ModelRef<IdentityModel> createdBy;
    public ModelRef<IdentityModel> owner;
    public double createdDate;


    /// <summary>
    /// Work amount needed to create. On average, a worker does one work a day.
    /// </summary>
    public int workAmount;


    public List<string> supportedBlueprints = new List<string>();
    public List<string> blueprintsThatUseIt = new List<string>();
    public List<string> blueprintsThatSupportIt = new List<string>();
    /// <summary>
    /// Item Size in meters squared;
    /// </summary>
    public float size = 1;
    public float mass = 1;
    public float baseArmor = 1;

    //Production Stats
    public double unitsCreated = 0;
    public double unitsSold;
    /// <summary>
    /// Factory ids and item supply
    /// </summary>
    public StringDoubleDictionary productionFactoryIds = new StringDoubleDictionary();
    /// <summary>
    /// Factory ids and item demand
    /// </summary>
    public StringDoubleDictionary consumerFactoryIds = new StringDoubleDictionary();

    //Money 
    public Money money;
    

    public string Name { get => _name; private set => _name = value; }
    public string Id { get => _id; private set => _id = value; }

    public double EstimatedValue { get => _estimatedValue; private set => _estimatedValue = value; }

    /// <summary>
    /// Gives the blueprint id and amount of the required resource for one item.
    /// </summary>
    public StringIntDictionary ConstructionBlueprints { get => _constructionBlueprints; private set => _constructionBlueprints = value; }

    public double Supply { get { return Sum(productionFactoryIds); } }
    public double Demand { get { return Sum(consumerFactoryIds); } }

    public ItemBlueprint() { }

    public ItemBlueprint(string _name, string _research, IdentityModel _createdBy, int workAmount, float _mass, float _size, float _armor, StringIntDictionary constructionBlueprints, List<string> _supportedBlueprints = null)
    {
        Name = _name;
        research = _research;
        Id = GetType().ToString() + GameManager.instance.data.id++.ToString();
        this.workAmount = workAmount;
        ConstructionBlueprints = constructionBlueprints;
        supportedBlueprints = _supportedBlueprints;
        EstimatedValue = .0016 * this.workAmount * 3f;

        foreach (KeyValuePair<string, int> constructionBlueprintId in ConstructionBlueprints)
        {
            var constructionBlueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(constructionBlueprintId.Key);
            EstimatedValue += constructionBlueprint.EstimatedValue;
            constructionBlueprint.blueprintsThatUseIt.Add(Id);
        }

        if (supportedBlueprints != null)
        {
            foreach (string x in supportedBlueprints)
            {
                var blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(x);
                blueprint.blueprintsThatSupportIt.Add(Id);
            }
        }


        size = _size;
        baseArmor = _armor;
        mass = _mass;

        createdBy = new ModelRef<IdentityModel>(_createdBy);
        owner = new ModelRef<IdentityModel>(_createdBy.Owner);
        createdDate = GameManager.instance.data.date.time;

        //Add to the correct places in Identity model and government model
        GameManager.instance.data.blueprintsModel.Model.blueprints.Add(this);
        _createdBy.Owner.ownedBlueprintIds.Add(Id);
        _createdBy.Government.allBlueprintIds.Add(Id);

        System.Random rand = new System.Random(Id.GetHashCode());
        float a = (float)rand.NextDouble();
        float b = (float)rand.NextDouble();
        float c = (float)rand.NextDouble();
        color = new Color(a, b, c);
        coloredName = "<color=" + ColorTypeConverter.ToRGBHex(color) + ">" + Name + "</color>";
    }

    public Research GetResearch()
    {
        return GameManager.instance.research.allResearch[research];
    }

    public void BuyItemInsta(double amount, string catBuyer, ref MoneyBudget moneyBuyer, string catSeller, ref MoneyBudget moneySeller)
    {
        unitsSold += amount;
        unitsCreated += amount;
        moneyBuyer.SubtractMoney(catBuyer, EstimatedValue * amount);
        moneySeller.AddMoney(catSeller, EstimatedValue * amount);

        foreach (KeyValuePair<string, int> constructionBlueprintId in ConstructionBlueprints)
        {
            var constructionBlueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(constructionBlueprintId.Key);
            constructionBlueprint.BuyItemInsta(constructionBlueprintId.Value * amount, "Supplier Contracts", ref owner.Model.money, "Income", ref constructionBlueprint.owner.Model.money);
        }
    }
    /// <summary>
    /// Sums up all the double in a dictionary
    /// </summary>
    /// <param name="dict"></param>
    /// <returns></returns>
    private double Sum(Dictionary<string, double> dict)
    {
        double value = 0;
        foreach (KeyValuePair<string, double> keyValue in dict)
        {
            value += keyValue.Value;
        }
        return value;
    }
}

public class StringDoubleDictionary: Dictionary<string,double>
{

}

public class StringIntDictionary : Dictionary<string, int>
{

}
