﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storage
{
    //TODO: Attach Storage class to an actual object in the game
    public Dictionary<string, List<Item>> itemsStorage = new Dictionary<string, List<Item>>();
    /// <sumary>
    /// Stores items as itemId, and [itemAmount,maxItemAmount]
    /// </sumary>
    public Dictionary<string, double[]> itemCapacity = new Dictionary<string, double[]>();

    public double Mass { get
        {
            double mass = 0;
            foreach (KeyValuePair<string, List<Item>> keyValue in itemsStorage)
            {
                foreach (Item item in keyValue.Value)
                {
                    mass += item.Mass;
                }
            }
            return mass;
        } }

    public StorageType storageType;

    public Storage() { }

    public Storage(StorageType _storageType, Dictionary<string, double> storage)
    {
        storageType = _storageType;

        foreach (KeyValuePair<string, double> pair in storage)
        {
            if (itemsStorage.ContainsKey(pair.Key))
            {
                itemCapacity[pair.Key][1] += pair.Value;
            }
            else
            {
                itemsStorage.Add(pair.Key, new List<Item>());
                itemCapacity.Add(pair.Key, new double[2] { 0, pair.Value });
            }
            
        }
    }

    public Item Find(string id, System.Predicate<Item> predicate)
    {
        string cat = Category(id);
        return itemsStorage[cat].Find(predicate);
    }

    public void RemoveAll()
    {
        foreach (KeyValuePair<string, List<Item>> pair in itemsStorage)
        {
            itemsStorage[pair.Key] = new List<Item>();
            itemCapacity[pair.Key][0] = 0;
        }
    }

    public Dictionary<string, List<Item>> GetStorage()
    {
        return itemsStorage;
    }

    /// <summary>
    /// Total Capacity of a storage category
    /// </summary>
    /// <param name="id">Category of itemId</param>
    /// <returns></returns>
    public double TotalCapacity(string id)
    {
        string cat = Category(id);

        return itemCapacity[cat][1];
    }

    /// <summary>
    /// Item amount in storage category
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public double ItemCatAmount(string id)
    {
        string cat = Category(id);

        return itemCapacity[cat][0];
    }

    public double RemainingCapacity(string id)
    {
        string cat = Category(id);

        return itemCapacity[cat][1] - itemCapacity[cat][0];
    }

    public bool AddItem(string itemId, string referenceId, string destinationId, double amount)
    {

        int itemIndex = -1;
        string cat = Category(itemId);
        itemIndex = itemsStorage[cat].FindIndex(x => x.id == itemId && x.destinationId == destinationId);
        if (itemIndex >= 0)
        {

            itemsStorage[cat][itemIndex].AddAmount(amount);
            itemCapacity[cat][0] += amount;
        }
        else
        {
            itemsStorage[cat].Add(new Item(itemId, amount, referenceId, destinationId));
            itemCapacity[cat][0] += amount;
        }
        return true;
    }

    private string Category(string id)
    {
        string cat = "";
        if (id == "All" || storageType == StorageType.All)
        {
            cat = "All";
        }
        else
        {
            ItemBlueprint blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(id);
            if (blueprint == null)
            {
                cat = id;
                if (!itemCapacity.ContainsKey(cat))
                {
                    throw new System.Exception("Storage does not contain key " + cat);
                }
            }
            else if (storageType == StorageType.Group)
            {
                cat = blueprint.GetResearch().category.ToString();
            }
            else if (storageType == StorageType.Research)
            {
                cat = blueprint.research;
            }
            else
            {
                throw new System.Exception("storage type not set");
            }
        }
        return cat;
    }

    public bool AddItem(Item item)
    {
        return AddItem(item.id, item.referenceId, item.destinationId, item.amount);
    }

    public void Remove(Item item)
    {
        if (!RemoveAmount(item.id, item.amount, item.destinationId))
            throw new System.Exception("Item removal error");
    }

    public bool RemoveAmount(string itemId, double amount, string destinationId)
    {
        int itemIndex = -1;

        ItemBlueprint blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(itemId);
        string cat = "";
        if (storageType == StorageType.All)
        {
            cat = "All";
        }
        else if (storageType == StorageType.Group)
        {
            cat = blueprint.GetResearch().category.ToString();
        }
        else if (storageType == StorageType.Research)
        {
            cat = blueprint.research;
        }
        else
        {
            throw new System.Exception("storage type not set");
        }
        itemIndex = itemsStorage[cat].FindIndex(x => x.id == itemId && (x.destinationId == destinationId));
        if (itemIndex >= 0)
        {
            if (itemsStorage[cat][itemIndex].amount < amount)
            {
                return false;
            }
            itemsStorage[cat][itemIndex].RemoveAmount(amount);
            itemCapacity[cat][0] -= amount;
            if (itemsStorage[cat][itemIndex].amount == 0)
            {
                itemsStorage[cat].RemoveAt(itemIndex);
            }
            return true;
        }
        return false;
    }

    public bool RemoveAmount(string itemId, double amount)
    {
        int itemIndex = -1;

        ItemBlueprint blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(itemId);
        string cat = "";
        if (storageType == StorageType.All)
        {
            cat = "All";
        }
        else if (storageType == StorageType.Group)
        {
            cat = blueprint.GetResearch().category.ToString();
        }
        else if (storageType == StorageType.Research)
        {
            cat = blueprint.research;
        }
        else
        {
            throw new System.Exception("storage type not set");
        }
        itemIndex = itemsStorage[cat].FindIndex(x => x.id == itemId && (x.destinationId == null));
        if (itemIndex >= 0)
        {
            if (itemsStorage[cat][itemIndex].amount < amount)
            {
                return false;
            }
            itemsStorage[cat][itemIndex].RemoveAmount(amount);
            itemCapacity[cat][0] -= amount;
            if (itemsStorage[cat][itemIndex].amount == 0)
            {
                itemsStorage[cat].RemoveAt(itemIndex);
            }
            return true;
        }
        return false;
    }
    /// <summary>
    /// Uses as much of the item amount as it can from the storage, returns the left over item amount that was not used.
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    //public double UseAsMuchItem(string itemId, double amount)
    //{
    //    int itemIndex = -1;

    //    ItemBlueprint blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(itemId);
    //    string cat = "";
    //    if (storageType == StorageType.All)
    //    {
    //        cat = "All";
    //    }
    //    else if (storageType == StorageType.Group)
    //    {
    //        cat = blueprint.GetResearch().tags[0];
    //    }
    //    else if (storageType == StorageType.Research)
    //    {
    //        cat = blueprint.research;
    //    }
    //    else
    //    {
    //        throw new System.Exception("storage type not set");
    //    }

    //    itemIndex = itemsStorage[cat].FindIndex(x => x.id == itemId);

    //    if (itemIndex >= 0)
    //    {
    //        if (itemsStorage[cat][itemIndex].amount <= amount)
    //        {
    //            double remaining = amount - itemsStorage[cat][itemIndex].amount;
    //            itemsStorage[cat][itemIndex].RemoveAmount(itemsStorage[cat][itemIndex].amount);
    //            itemsStorage[cat].RemoveAt(itemIndex);
    //            itemCapacity[cat][0] = 0;
    //            return remaining;
    //        }
    //        else
    //        {
    //            itemsStorage[cat][itemIndex].RemoveAmount(amount);
    //            return 0;
    //        }
    //    }
    //    throw new System.System.Exception("Could not find item " + itemId);
    //}

    public bool ContainsItem(string itemId, double amount)
    {
        int itemIndex = -1;

        ItemBlueprint blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(itemId);
        string cat = "";
        if (storageType == StorageType.All)
        {
            cat = "All";
        }
        else if (storageType == StorageType.Group)
        {
            cat = blueprint.GetResearch().category.ToString();
        }
        else if (storageType == StorageType.Research)
        {
            cat = blueprint.research;
        }
        else
        {
            throw new System.Exception("storage type not set");
        }
        itemIndex = itemsStorage[cat].FindIndex(x => x.id == itemId && x.amount >= amount);

        return itemIndex != -1;
    }

    public bool ContainsItem(string itemId)
    {
        ItemBlueprint blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(itemId);
        string cat = "";
        if (storageType == StorageType.All)
        {
            cat = "All";
        }
        else if (storageType == StorageType.Group)
        {
            cat = blueprint.GetResearch().category.ToString();
        }
        else if (storageType == StorageType.Research)
        {
            cat = blueprint.research;
        }
        else
        {
            throw new System.Exception("storage type not set");
        }

        return itemsStorage[cat].Find(x => x.id == itemId) != null;
    }
    /// <summary>
    /// Finds item with the same id stored in ItemStorage.
    /// </summary>
    /// <param name="itemId">item to look for</param>
    /// <returns>item that was found, or null</returns>
    public Item Find(string itemId)
    {
        ItemBlueprint blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(itemId);
        string cat = "";
        if (storageType == StorageType.All)
        {
            cat = "All";
        }
        else if (storageType == StorageType.Group)
        {
            cat = blueprint.GetResearch().category.ToString();
        }
        else if (storageType == StorageType.Research)
        {
            cat = blueprint.research;
        }
        else
        {
            throw new System.Exception("storage type not set");
        }
        return itemsStorage[cat].Find(x => x.id == itemId);
    }
}

public enum StorageType
{
    All,
    Group,
    Research
}
