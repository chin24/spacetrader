﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductionStructure: SupplyStructure
{

    
    public double unitPrice
    {
        get
        {
            if (ProductionRateActual == 0)
                return 0;
            //Calculate Unit Price //TODO: Fix unitPriceCalculation
            double averageUnitPrice = workers * workerPayRate * ProductionTime;
            //Add cost of materials
            
            foreach(KeyValuePair<string,int> blue in materialBlueprints)
            {
                var blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(blue.Key);
                averageUnitPrice += blueprint.EstimatedValue * blue.Value; //TODO: change to the averageMarketprice when it is created.
            }

            foreach (Contract contract in GetSupplyContracts())
            {

                double rate = (contract.itemAmount + contract.itemAmountEnroute) / (Dated.Month * 2) * ProductionTime;

                averageUnitPrice += contract.unitPrice * rate;
            }

            foreach (Contract clientContract in clientContracts)
            {
                averageUnitPrice += clientContract.unitPrice;
            }
            averageUnitPrice /= clientContracts.Count + 1;
            return averageUnitPrice;
        }
    }
    public string productionItemName;
    public string productionItemId { get; protected set; }
	public ItemBlueprint Product { get{ return GameManager.instance.data.blueprintsModel.Model.GetItem(productionItemId);}}
    /// <summary>
    /// Amount of work that has to be done for a single item. Typically 1 work done per worker per hour.
    /// </summary>
    public double workAmount { get { return Product.workAmount; } }
    public bool isProducing;
    public bool on = true;
    /// <summary>
    /// Amount of production done, with 1 being 1 item.
    /// </summary>
    public double productionProgress = 0;
    public ProductionState productionState = ProductionState.LackMaterials;
    /// <summary>
    /// Stores information of the items that are needed or that have surpluses.
    /// </summary>
    public List<KeyValuePair<int, double>> connectionItems = new List<KeyValuePair<int, double>>();
    public List<Contract> clientContracts = new List<Contract>();
    
    
    public string shipContractId;
    public bool demandShip;
    
    /// <summary>
    /// Production rate per second.
    /// </summary>
    public double ProductionRateOptimal
    {
        get { return workers / (workAmount * Dated.Hour); }
    }
    public double ProductionRateActual
    {
        get
        {
            if (neededItemAmount.Count > 0)
            {
                double percentProductionRate = 1;
                foreach (KeyValuePair<string, double> rate in neededItemAmount)
                {
                    foreach (KeyValuePair<string,int> contrstructionBlueprint in materialBlueprints)
                    {
                        if (rate.Key == contrstructionBlueprint.Key)
                        {
                            double thisPercent = (ProductionRateOptimal * contrstructionBlueprint.Value - rate.Value) / ProductionRateOptimal;
                            if (thisPercent < percentProductionRate)
                                percentProductionRate = thisPercent;
                            break;
                        }
                    }

                }
                return ProductionRateOptimal * percentProductionRate;
            }
            else
                return ProductionRateOptimal;
        }
    }
    /// <summary>
    /// Time to produce one item in seconds.
    /// </summary>
    public double ProductionTime
    {
        get { return workAmount / workers; }
    }

    //Construction Update
    public double overSupplyDate;
    public double lastProductionDate;

    public ProductionStructure() { }

    public ProductionStructure(IdentityModel owner, string structureBlueprint, string _productionItemId, string referenceId, Vector3d _localPosition, int _count = 1, bool doConstruct = false):
        base( owner, structureBlueprint, referenceId, _localPosition, _count, doConstruct)
    {
        productionItemId = _productionItemId;
        var product = GameManager.instance.data.blueprintsModel.Model.GetItem(productionItemId);

        var structureResearch = GetStructureResearch();
        productionItemName = product.Name;
        SetName(productionItemName + " " + structureResearch.name);
        
        if (product == null) throw new System.SystemException("Could not find product: " + productionItemId);
        materialBlueprints = product.ConstructionBlueprints;

        //Workers
        if (product as ProductionBlueprint != null)
        {
            workers = ((ProductionBlueprint)product).workers;
        }
        else
        {
            workers = 100 * Count;
        }

        //Make sure fuel exists
        var fuels = GameManager.instance.data.blueprintsModel.Model.blueprints.FindAll(x => x.GetResearch().name == "Fuel Cell");
        if (fuels.Count == 0)
        {
            //Create company to manage blueprints related to the research
            Creature manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, owner.referenceId, GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);

            new CompanyModel(owner.Owner.name + " Fuel Cell Branch", "Fuel Cell", GameManager.instance.locations[owner.referenceId] as SolarBody, owner.Government, manager, owner.Owner);

            //Create blueprint
            var blueprint = owner.Owner.GetOwnedBlueprints().Find(x => x.research == "Fuel Cell");

            if (blueprint == null) throw new System.Exception("Could not find created structure blueprint for: " + "Fuel Cell");
            fuels = new List<ItemBlueprint>() {blueprint};
        }

        //Create storage
        Dictionary<string, double> storeCapacity = new Dictionary<string, double>();

        storeCapacity.Add(Product.research, Mathd.Floor(ProductionRateOptimal * Dated.Month * 2 + 2));

        foreach (KeyValuePair<string,int> construction in materialBlueprints)
        {
            var blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(construction.Key);

            storeCapacity.Add(blueprint.research, Mathd.Floor(Mathd.Floor(ProductionRateOptimal * Dated.Month * 2 + 2) * construction.Value + 2));
        }
        var fuel = fuels[Random.Range(0, fuels.Count)];
        if (storeCapacity.ContainsKey(fuel.research))
            storeCapacity[fuel.research] += 1000;
        else
        {
            storeCapacity.Add(fuel.research, 1000);
        }
        
        storage = new Storage(StorageType.Research, storeCapacity);

        //Find Suitable ship blueprint
        SetShipBlueprintId();

        //Fill product to half full

        if (!doConstruct)
        {
            double amount = Mathd.Floor(ProductionRateOptimal * Dated.Month * 2 + 2);
            product.BuyItemInsta(amount, "Supplier Contracts", ref owner.money, "Income", ref owner.money);
            storage.AddItem(product.Id, GetId(), null,amount);
        }

        //Update List
        updateList.Add( () => SetProductContract());
        updateList.Add( () => UpdateContract(ref shipContractId));
        updateList.Add( () => UpdateSupply());
        updateList.Add( () => UpdateFuel());
        updateList.Add( () => UpdateDemand());
        updateList.Add( () => CheckSupplyDemand());
    }

    public override void Update()
    {
        SetShipBlueprintId();
        base.Update();              
    }

    private void SetShipBlueprintId()
    {
        if (shipBlueprintId != null)
            return;
        //Get Blueprint
        var blueprints = GameManager.instance.data.blueprintsModel.Model.blueprints.FindAll(x => x.GetResearch().name == "Cargo Ship");

        if (blueprints.Count != 0)
        {
            shipBlueprintId = blueprints[Random.Range(0, blueprints.Count)].Id;
        }
        else
            return;
    }

    /// <summary>
    /// Makes sure that the factory is at the maximum production rate by finding contracts with known companies for needed materials
    /// </summary>
    /// <param name="itemId"></param>
    /// <param name="itemAmount">Needed production Rate per second</param>
    public override Contract SearchContracts(string itemId, double itemAmount, ContractType contractType = ContractType.Supply)
    {
        //Find a contract
        string contractId = "";
        contractId = owner.Model.Government.GetCheapestContractId(itemId, GetId(), itemAmount);

        if (contractId != "" && contractId != null)
        {
            supplierContractIds.Add(contractId);
            Contract contract = GameManager.instance.contracts[contractId];

            //Set wanted conditions;
            contract.client = new ModelRef<IdentityModel>(owner.Model);
            contract.destinationId = GetId();
            contract.contractType = contractType;
            contract.contractState = ContractState.Sent;
            
            if (itemAmount < contract.itemAmount)
            {
                contract.alternateItemAmount = itemAmount;
            }
            else
            {
                contract.alternateItemAmount = contract.itemAmount;
            }

            return contract;
        }
        return null;
    }

    protected bool SearchRequiredItems()
    {
        int neededItemCount = materialBlueprints.Count;
        int itemCount = 0;
        foreach (KeyValuePair<string,int> constructionBlueprint in materialBlueprints)
        {

            double neededAmount = constructionBlueprint.Value;
            if (storage.ContainsItem(constructionBlueprint.Key))
            {
                neededAmount -= storage.Find(constructionBlueprint.Key, x => x.destinationId == GetId()).amount;
            }
            if (neededAmount <= 0)
            {
                //item.price = ReferenceBody.GetMarketPrice(item.id);
                //ReferenceBody.RemoveBuying(constructionBlueprint.Key, id, constructionBlueprint.Value);
                itemCount++;
            }
            else
            {
                //var neededItem = new Item(constructionBlueprint.Key, neededAmount, id);
                return false;
            }

        }
        return true;
    }

    protected void UseRequiredItems()
    {
        foreach (KeyValuePair<string,int> constructionBlueprint in materialBlueprints)
        {
            var blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(constructionBlueprint.Key);
            var found = storage.RemoveAmount(constructionBlueprint.Key, constructionBlueprint.Value, GetId());
            if (!found)
                throw new System.Exception("Item is not found in correct amount or destination");
        }
        StoreCreatedItem(1);
    }

    protected bool StoreCreatedItem(double amount)
    {
        storage.AddItem(productionItemId, GetId(), null, amount);
        owner.Model.money.SubtractMoney("Workers", workers * workerPayRate * ProductionTime * amount);
        return true;
    }

    

    public void SetProductContract()
    {

        //Make sure there is a contract for extra production Rate
        bool newContract = true;
        if (shipBlueprintId == null)
            newContract = false;
        bool repeat = true;

        var product = storage.Find(productionItemId, x => x.id == productionItemId && x.destinationId == null);

        if (product == null)
            return;
        double productAmount = product.amount;

        while (repeat)
        {
            repeat = false;

            foreach (Contract contract in clientContracts)
            {
                if (contract.contractState != ContractState.Active)
                {
                    newContract = false;
                    if (contract.contractState == ContractState.Rejected)
                    {
                        newContract = false;
                        clientContracts.Remove(contract);
                        GameManager.instance.contracts.Remove(contract.id);
                        owner.Model.Government.postedContractIds.Remove(contract.id);
                        owner.Model.contracts.Remove(contract.id);
                        if (contract.destinationId != null)
                        {
                            //Remove destination supply listing
                            SupplyStructure destination = GameManager.instance.locations[contract.destinationId] as SupplyStructure;
                            destination.supplierContractIds.Remove(contract.id);
                        }
                        foreach (string shipId in contract.shipIds)
                        {
                            ((Ship)GameManager.instance.locations[shipId]).contractId = null;
                        }

                        repeat = true;
                        break;
                    }
                    else if (contract.contractState == ContractState.Sent)
                    {
                        if (contract.alternateItemAmount >= 1)
                        {
                            //Make sure that the contract is profitable and then set to review, check that ship can deliver the amount of goods needed per month, etc.
                            contract.itemAmount = contract.alternateItemAmount;

                            //Setup Costs
                            double distance = Vector3d.Distance(GameManager.instance.locations[contract.originId].SystemPosition, GameManager.instance.locations[contract.destinationId].SystemPosition);
                            contract.CalculateCost(distance, 1, contract.itemAmount);

                            //Set due date
                            contract.contractEndDate = new Dated(GameManager.instance.data.date.time + contract.duration + 15 * Dated.Day);


                            contract.contractState = ContractState.Review;
                        }
                        else
                        {
                            contract.contractState = ContractState.Rejected;
                        }
                           
                    }
                    else if (contract.contractState == ContractState.Accepted)
                    {
                        //Payment
                        contract.client.Model.money.SubtractMoney(product.Blueprint.research, contract.cost);
                        owner.Model.money.AddMoney("Income", contract.cost);

                        if (GameManager.instance.locations[contract.destinationId].GetType() == typeof(DistributionCenter))
                        {
                            //Undo payment if going to DC
                            contract.client.Model.money.SubtractMoney(product.Blueprint.research, -contract.cost);
                        }

                        //Designate items
                        Item item = new Item(contract.itemId, contract.itemAmount) { destinationId = contract.destinationId };

                        storage.RemoveAmount(item.id, item.amount);
                        storage.AddItem(item);

                        productAmount -= contract.itemAmount;

                        if (contract.contractType == ContractType.Ship)
                        {
                            for(int i = 0; i < item.amount; i++)
                            {
                                Ship ship = new Ship(contract.itemId, contract.client.Model, new Creature("Bob", 10000, GetId(), GameManager.instance.data.date, new Dated(Dated.Year * 30), CreatureType.Human), GetId());
                            }
                            storage.Remove(item);
                            contract.itemAmount = 0;
                            contract.itemAmountEnroute = 0;
                            contract.contractState = ContractState.Rejected;
                        }
                        else if (GameManager.instance.locations[contract.destinationId].GetReferenceBody() == GameManager.instance.locations[contract.originId].GetReferenceBody())
                        {
                            //Setup a within planet contract
                            contract.contractDistanceType = ContractDistanceType.WithinPlanet;
                            contract.shipCount = 0;
                            contract.contractEndDate = new Dated(GameManager.instance.data.date.time + 3 * Dated.Day);
                            contract.itemAmountEnroute = contract.itemAmount;
                            contract.itemAmount = 0;
                            //Remove item from storage
                            storage.Remove(storage.Find(contract.itemId, x => x.destinationId == contract.destinationId));
                            contract.contractState = ContractState.Active;
                        }
                        else if (clientContracts.Exists(x => x.destinationId == contract.destinationId && contract.id != x.id))
                        {
                            //Check to see if there is a contract to the same location already exists
                            var previousContract = clientContracts.Find(x => x.destinationId == contract.destinationId && contract.id != x.id);

                            previousContract.cost += contract.cost;
                            previousContract.contractEndDate.AddTime(previousContract.duration * .75);
                            previousContract.unitPrice = (previousContract.unitPrice * (previousContract.itemAmount + previousContract.itemAmountEnroute) + contract.unitPrice * contract.itemAmount)
                                / (previousContract.itemAmount + previousContract.itemAmountEnroute + contract.itemAmount);
                            previousContract.itemAmount += contract.itemAmount;

                            //Reject new contract
                            contract.contractState = ContractState.Rejected;
                        }
                        else
                        {
                            //Setup Ships
                            for (int i = 0; i < contract.shipCount; i++)
                            {
                                demandShip = true;
                                //Create a new ship if needed or assign ship
                                foreach (string shipId in owner.Model.ownedShipIds)
                                {
                                    Ship ship = GameManager.instance.locations[shipId] as Ship;
                                    if (ship.contractId == null && ship.structureBlueprintId == shipBlueprintId)
                                    {
                                        demandShip = false;
                                        ship.contractId = contract.id;
                                        contract.shipIds.Add(ship.GetId());
                                    }
                                }

                                if (demandShip && GameManager.instance.data.date.time < Dated.Year)
                                {
                                    Ship ship = new Ship(shipBlueprintId, owner.Model, new Creature("Bob", 10000, GetId(), GameManager.instance.data.date, new Dated(Dated.Year * 30), CreatureType.Human), GetId());
                                    ship.contractId = contract.id;
                                    contract.shipIds.Add(ship.GetId());
                                    //Pay for ship
                                    contract.GetShipBlueprint().BuyItemInsta(1, "Structures", ref owner.Model.money, "Income", ref contract.GetShipBlueprint().owner.Model.money);
                                }
                                else if (demandShip && shipContractId == null && shipBlueprintId != null)
                                {
                                    var shipContract = SearchContracts(shipBlueprintId, contract.shipCount, ContractType.Ship);
                                    if (shipContract != null)
                                        shipContractId = shipContract.id;
                                }
                            }
                            contract.contractState = ContractState.Active;
                        }

                        
                        
                    }
                    else if (contract.contractState == ContractState.Initial)
                    {
                        //Update initial contract
                        contract.itemAmount = productAmount;
                        contract.unitPrice -= contract.unitPrice * deltaTime / (Dated.Year * 10);
                        if (contract.unitPrice < .001)
                        {
                            contract.unitPrice = .001;
                        }
                    }
                }
                else
                {
                    if (contract.contractDistanceType != ContractDistanceType.SpaceShip)
                    {
                        if (GameManager.instance.data.date >= contract.contractEndDate)
                        {

                            SupplyStructure structure = GameManager.instance.locations[contract.destinationId] as SupplyStructure;

                            if (contract.itemId == structure.structureBlueprintId)
                            {
                                //Construct structure
                                structure.Count += (int)contract.itemAmountEnroute;
                                structure.workers = ((ProductionBlueprint)structure.GetStructureModel()).workers * structure.Count;
                                
                            }
                            else
                            {
                                //Add to storage
                                structure.storage.AddItem(contract.itemId, structure.GetId(), structure.GetId(), contract.itemAmountEnroute);
                            }
                            //Pay
                            owner.Model.money.SubtractMoney(product.Blueprint.research, .1 * contract.itemAmountEnroute * contract.unitPrice);
                            contract.itemAmountEnroute -= contract.itemAmountEnroute;
                        }
                    }
                    if (contract.itemAmount <= 0 && contract.itemAmountEnroute <=0)
                    {
                        //End contract
                        contract.contractState = ContractState.Rejected;
                        //Contract has ended
                    }
                    else if (contract.contractDistanceType == ContractDistanceType.SpaceShip && contract.shipIds.Count <= 0)
                    {
                        //Find or add ship
                        //Setup Ships
                        for (int i = 0; i < contract.shipCount; i++)
                        {
                            demandShip = true;
                            //Create a new ship if needed or assign ship
                            foreach (string shipId in owner.Model.ownedShipIds)
                            {
                                Ship ship = GameManager.instance.locations[shipId] as Ship;
                                if (ship.contractId == null && ship.structureBlueprintId == shipBlueprintId)
                                {
                                    demandShip = false;
                                    ship.contractId = contract.id;
                                    contract.shipIds.Add(ship.GetId());
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (string shipId in contract.shipIds)
                        {
                            Ship ship = GameManager.instance.locations[shipId] as Ship;
                            if (ship.contractId == null)
                            {
                                ship.contractId = contract.id;
                            }
                            
                        }
                    }
                }
            }
        }


        if (newContract && productAmount >= 1)
        {
            //Create New Contract 
            var contract = new Contract(owner.Model, productionItemId, productAmount, GetId(), shipBlueprintId, unitPrice * 1.25);
            clientContracts.Add(contract);
            owner.Model.Government.postedContractIds.Add(contract.id);
            owner.Model.contracts.Add(contract.id);
        }
    }

    public void CheckSupplyDemand()
    {
        if (Product.Supply > Product.Demand)
            overSupplyDate = GameManager.instance.data.date.time;
        if (isProducing)
            lastProductionDate = GameManager.instance.data.date.time;


        //Check to see if there is a greater demand
        
        if ( GameManager.instance.data.date.time - overSupplyDate > Dated.Month * 2 && GameManager.instance.data.date.time - lastProductionDate  < Dated.Month)
        {
            //Needs to increase production by creating more factories/drillers
            if (structureUpgradeContractId == null)
            {
                //Find Construction Contract
                var contract = SearchContracts(structureBlueprintId, 1);
                if (contract != null)
                    structureUpgradeContractId = contract.id;
            }
        }
    }

    

    public void UpdateSupply()
    {
        var product = storage.Find(productionItemId, x => x.id == productionItemId && x.destinationId == null);
        if (product != null)
            Product.productionFactoryIds[GetId()] = product.amount;
        else
            Product.productionFactoryIds[GetId()] = 0;
        
    }

    
}



public enum ProductionState
{
    LackMaterials,
    Inactive,
    Active
}



