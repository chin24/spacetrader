﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dock {
    [SerializeField]
    private string referenceId;
    public Vector3d referencePosition = Vector3d.zero;
    public bool shipDocked;
    public string shipId = null;

    

    public bool IsAvailable
    {
        get { return shipId == null; }
    }

    public string ReferenceId { get => referenceId; private set => referenceId = value; }

    public Dock() { }

    public Dock( string referenceId)
    {
        this.referenceId = referenceId;
    }

    public void DockShip(Ship ship)
    {
        shipDocked = true;
        shipId = ship.GetId();
        ship.SetReferenceId(this.referenceId);
        ship.SetReferencePosition(referencePosition);
        ship.Docked = true;
    }

    public void UndockShip(string shipId)
    {
        Ship ship = GameManager.instance.locations[shipId] as Ship;
        if (shipId == this.shipId)
        {
            shipDocked = false;
            this.shipId = null;
            ship.Docked = false;
        }
        else
            throw new System.Exception("Wrong ship docked: " + shipId + " vs " + ship.GetId());
    }

    public void ClaimDock(Ship ship) {
        shipId = ship.GetId();
    }

    

    public Ship GetShip()
    {
        if (!IsAvailable)
        {
            return GameManager.instance.locations[shipId] as Ship;
        }
        else return null;
    }
}
