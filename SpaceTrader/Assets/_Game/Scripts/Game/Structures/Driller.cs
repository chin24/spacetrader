﻿using UnityEngine;
using System.Collections.Generic;
using CodeControl;

public class Driller : ProductionStructure {

    public Driller() { }

    

    public Driller(IdentityModel owner, string structureBlueprint, string _productionItemId, string referenceId, int _count = 1):
        base(owner, structureBlueprint, _productionItemId, referenceId, (Vector3d)Random.onUnitSphere * ((SolarBody)GameManager.instance.locations[referenceId]).GetBodyRadius() * .5, _count)
    {
        //name = productionItemName + " Driller " + id;
    }

    /// <summary>
    /// Creates items and uses items based on the elapsed time
    /// </summary>
    /// <param name="elapsedTime">time elapsed (in seconds)</param>
    public override void Update()
    {
        base.Update();

        //var price = parentBody.GetMarketPrice(productionItemId) / productionTime;
        //var cost = workers * workerPayRate;
        Info = "Count: " + Count + "\nOn: " + on.ToString()
            + "\nIs Producing: " + isProducing.ToString()
            + "\nResources Mined: " + Units.ReadItem(productionProgress)+ "u";
        
        if (on && storage.RemainingCapacity(productionItemId) > 0)
        {
            isProducing = true;
            //if (GameManager.instance.timeScale > 2000)
            //{
            //    GameManager.instance.timeScale = 1;
            //    GameManager.instance.OpenInfoPanel(this);
            //}
            var productionDone = (float)(deltaTime * ProductionRateActual);
            productionProgress += productionDone;

            DrillResource(productionDone);
        }
        else
        {
            isProducing = false;
        }
    }

    private bool RequiredComponentsAvailable(SolarBody parentBody)
    {
        var resource = GameManager.instance.data.rawResources.Model.GetResource(productionItemId);
        var found = false;
        foreach (RawResource raw in parentBody.GetRawResources())
        {
            if (raw.GetId() == resource.id)
            {
                found = true;
                break;
            }
        }
        return found;
    }

    private void DrillResource(double amount)
    {
        var resource = GameManager.instance.data.rawResources.Model.GetResource(Product.GetResearch().name);

        foreach (RawResource raw in GetReferenceBody().GetRawResources())
        {
            if (raw.GetId() == resource.id)
            {

                double amountRemoved = raw.RemoveAmount(amount);
                StoreCreatedItem(amountRemoved);
                if (amount > amountRemoved)
                    on = false;
                break;
            }
        }
    }
    
}
