﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SupplyStructure : Structure, IWorkers
{
    public List<Dock> docks;
    public List<string> dockLocalPosition;
    public List<string> childStructureIds;

    public List<string> supplierContractIds = new List<string>();
    /// <summary>
    /// required blueprints and amounts to make one unit of the production item
    /// </summary>
    public Dictionary<string, int> materialBlueprints = new Dictionary<string, int>();

    //Ships
    /// <summary>
    /// The ship blueprint that the factory will be using as fleet
    /// </summary>
    public string shipBlueprintId;

    /// <summary>
    /// The item ids and rate of items the factory needs.
    /// </summary>
    internal StringDoubleDictionary neededItemAmount = new StringDoubleDictionary();

    //Updgrades
    public string structureUpgradeContractId;
    public string fuelContractId;
    public bool demandUpgrade;

    public List<SupplyStructure> ConnectedStructures
    {
        get
        {
            List<SupplyStructure> structures = new List<SupplyStructure>();
            if (childStructureIds != null)
            {
                foreach (string structureId in childStructureIds)
                {
                    structures.Add(GameManager.instance.locations[structureId] as SupplyStructure);
                }
            }

            return structures;
        }
    }

    //IWorkers
    public int workers { get; set; }

    public double workerPayRate { get; set; }

    public SupplyStructure() { }

    public SupplyStructure(IdentityModel owner, string structureBlueprint, string referenceId, Vector3d _localPosition, int _count = 1, bool doConstruct = false) :
        base(owner, structureBlueprint, referenceId, _localPosition, doConstruct)
    {
        //Add Structure
        var body = GetReferenceBody();
        body.Structures.Add(this);
        owner.AddStructure(GetId(), referenceId);

        var structureResearch = GetStructureResearch();
        SetName(structureResearch.name);
        Count = _count;

        workerPayRate = .00116;
        workers = 100 * Count;

        createDocks(10);

        //Add to Update List
        updateList.Add(() => FindSuppliers());
        updateList.Add(() => UpdateContract(ref structureUpgradeContractId));
        updateList.Add(() => UpdateDemand());
    }

    public override void Update()
    {
        base.Update();
    }

    private void createDocks(int count)
    {
        docks = new List<Dock>();
        for (int i = 0; i < count; i++)
        {
            docks.Add(new Dock(GetId()));
        }
    }
    /// <summary>
    /// Returns a list of ships that recursively reference structure
    /// </summary>
    /// <returns></returns>
    public List<Ship> GetAllCloseShips()
    {
        List<Ship> ships = new List<Ship>();
        foreach(string structureId in PositionEntityIds)
        {
            Ship ship = GameManager.instance.locations[structureId] as Ship;

            if (ship != null)
            {
                ships.Add(ship);
            }
        }
        foreach (SupplyStructure structure in ConnectedStructures)
        {
            ships.AddRange(structure.GetAllCloseShips());
        }
        return ships;
    }

    public void FindSuppliers()
    {
        //Objective: fill up all the supplier storage space

        //Figures out which materials that are not accounted for and creates a dictionary for the needed rates.
        neededItemAmount = new StringDoubleDictionary();
        foreach (KeyValuePair<string, int> constructionBlueprint in materialBlueprints)
        {
            neededItemAmount[constructionBlueprint.Key] = storage.RemainingCapacity(constructionBlueprint.Key);
        }

        if (supplierContractIds.Count > 0)
        {
            for (int i = supplierContractIds.Count - 1; i >= 0; i--)
            {
                string contractId = supplierContractIds[i];
                if (!GameManager.instance.contracts.ContainsKey(contractId))
                {
                    supplierContractIds.Remove(contractId);
                    continue;
                }

                Contract contract = GameManager.instance.contracts[contractId];

                if (neededItemAmount.ContainsKey(contract.itemId) && contract.contractState == ContractState.Active)
                {
                    try
                    {
                        neededItemAmount[contract.itemId] -= contract.itemAmount;
                        neededItemAmount[contract.itemId] -= contract.itemAmountEnroute;

                        if (neededItemAmount[contract.itemId] <= 0)
                            neededItemAmount.Remove(contract.itemId);
                    }
                    catch (KeyNotFoundException e)
                    {
                        Debug.LogError(e.Message);
                    }
                }

            }
        }

        //Find contracts for needed items
        foreach (KeyValuePair<string, double> item in neededItemAmount)
        {
            //Check to see if a contract exists to fill the the missing production has been created
            bool searchForContract = true;
            foreach (string contractId in supplierContractIds)
            {
                Contract contract = GameManager.instance.contracts[contractId];
                if (contract.itemId == item.Key)
                {
                    if (contract.contractState != ContractState.Active)
                    {
                        searchForContract = false;
                        if (contract.contractState == ContractState.Review)
                        {

                            //Verify that contract will work, accept or reject
                            contract.contractState = ContractState.Accepted;
                        }
                        else if (contract.contractState == ContractState.Renew)
                        {
                            throw new System.Exception("Renew not implemented");
                            ////Reset ships
                            //foreach (string shipId in contract.shipIds)
                            //{
                            //    ((Ship)GameManager.instance.locations[shipId]).contractId = null;
                            //}

                            ////Set wanted conditions;
                            //contract.destinationId = id;
                            //contract.contractState = ContractState.Sent;

                            //if (itemRate.Value < contract.itemRate)
                            //{
                            //    contract.alternateItemRate = itemRate.Value;
                            //}
                            //else
                            //{
                            //    contract.alternateItemRate = contract.itemRate;
                            //}
                        }
                    }

                }
            }

            if (searchForContract && item.Value >= 1)
            {
                var contract = SearchContracts(item.Key, Mathd.Floor(item.Value));
                if (contract != null)
                {
                    supplierContractIds.Add(contract.id);
                }
            }
        }

    }

    /// <summary>
    /// Used to update supply contracts
    /// </summary>
    /// <param name="contractId"></param>
    public void UpdateContract(ref string contractId)
    {
        if (contractId != null)
        {
            //Check to 
            if (GameManager.instance.contracts.ContainsKey(contractId))
            {
                Contract contract = GameManager.instance.contracts[contractId];
                if (contract.contractState != ContractState.Active)
                {
                    if (contract.contractState == ContractState.Rejected)
                    {
                        contractId = null;
                    }
                    else if (contract.contractState == ContractState.Review)
                    {

                        //Verify that contract will work, accept or reject
                        contract.contractState = ContractState.Accepted;
                    }
                }
            }
            else
                contractId = null;
        }
    }

    public void UpdateFuel()
    {
        if (fuelContractId == null && storage.RemainingCapacity("Fuel Cell") > 1)
        {
            var fuelBlueprint = FindFuel();
            if (fuelBlueprint != null)
            {
                var contract = SearchContracts(fuelBlueprint.Id, storage.RemainingCapacity(fuelBlueprint.research));
                if (contract != null)
                    fuelContractId = contract.id;
            }

        }

        UpdateContract(ref fuelContractId);
    }

    public void UpdateDemand()
    {
        foreach (KeyValuePair<string, int> constructionBlueprint in materialBlueprints)
        {
            var product = storage.Find(constructionBlueprint.Key, x => x.id == constructionBlueprint.Key && x.destinationId == GetId());
            if (product != null)
                product.Blueprint.consumerFactoryIds[GetId()] = storage.RemainingCapacity(product.id);
            else
                GameManager.instance.data.blueprintsModel.Model.GetItem(constructionBlueprint.Key).consumerFactoryIds[GetId()] = storage.RemainingCapacity(constructionBlueprint.Key);
        }
    }

    public List<Contract> GetSupplyContracts()
    {
        List<Contract> list = new List<Contract>();
        foreach (string structureId in supplierContractIds)
        {
            Contract structure = GameManager.instance.contracts[structureId];
            list.Add(structure);
        }

        return list;
    }

    public bool StationHasStructure(string structureId)
    {
        SupplyStructure reference = GameManager.instance.locations[GetReferenceId()] as SupplyStructure;
        if ( reference == null)
        {
            if (GetId() == structureId)
                return true;
            return ConnectedStructures.Exists(x => x.GetId() == structureId);
        }
        else
        {
            return reference.StationHasStructure(structureId);
        }
    }
    
    /// <summary>
    /// Gets the head station in a station complex
    /// </summary>
    public SupplyStructure Station { get {
            SupplyStructure reference = GameManager.instance.locations[GetReferenceId()] as SupplyStructure;
            if (reference == null)
            {
                return this;
            }
            else
            {
                return reference.Station;
            }
        } }

    //Station Docking classes
    protected bool StationUndockShip( Ship ship)
    {
        if (UndockShip(ship.GetId()))
        {
            return true;
        }
        foreach ( SupplyStructure structure in ConnectedStructures)
        {
            if (UndockShip(ship.GetId()))
            {
                return true;
            }
        }
        return false;
    }

    public bool UndockShip(Ship ship)
    {
        if (UndockShip(ship.GetId()))
        {
            return true;
        }
        //return Station.StationUndockShip(ship);
        throw new System.Exception("Not docked to this structure");
    }

    private bool UndockShip(string shipId)
    {
        foreach (Dock dock in docks)
        {
            if (dock.shipId == shipId)
            {
                dock.UndockShip(shipId);
                return true;
            }
        }
        return false;
    }

    public bool DockAvailable(string structureId)
    {
        //check if they want to dock with this structure
        if (structureId == GetId())
        {
            return DockAvailable();
        }
        //return Station.StationDockAvailalble(structureId);
        throw new System.Exception("structureId does not match this structure");
    }

    private bool DockAvailable()
    {
        foreach (Dock dock in docks)
        {
            if (dock.IsAvailable)
            {
                return true;
            }
        }
        return false;
    }

    private bool StationDockAvailalble(string structureId)
    {
        //check if they want to dock with this structure
        if (structureId == GetId())
        {
            return DockAvailable();
        }
        foreach (SupplyStructure structure in ConnectedStructures)
        {
            if (structure.GetId() == structureId)
            {
                return DockAvailable();
            }
        }
        return false;
    }

    public Vector3d ClaimDock(Ship ship)
    {
        if (DockAvailable(GetId()))
        {
            return ClaimStructureDock(ship);
        }
        //return Station.StationClaimDock(ship);
        throw new System.Exception("No Dock available");
    }

    private Vector3d ClaimStructureDock(Ship ship)
    {
        foreach (Dock dock in docks)
        {
            if (dock.IsAvailable)
            {
                dock.ClaimDock(ship);
                return dock.referencePosition;
            }
        }
        throw new System.Exception("No Dock available");
    }

    public Vector3d StationClaimDock(Ship ship)
    {
        foreach (Dock dock in docks)
        {
            if (dock.IsAvailable)
            {
                dock.ClaimDock(ship);
                return dock.referencePosition;
            }
        }
        foreach (SupplyStructure structure in ConnectedStructures)
        {
            if (structure.DockAvailable())
            {
                return structure.ClaimDock(ship);
            }
        }
        throw new System.Exception("No Dock available");
    }

    public bool DockShip(Ship ship)
    {
        if (DockShipToStructure(ship)) return true;
        //return StationDockShip(ship);
        throw new System.Exception("No Dock available");
    }

    private bool DockShipToStructure(Ship ship)
    {
        foreach (Dock dock in docks)
        {
            if (dock.shipId == ship.GetId())
            {
                dock.DockShip(ship);
                return true;
            }
        }
        return false;
    }

    private bool StationDockShip(Ship ship)
    {
        if (DockShipToStructure(ship)) return true;
        foreach (SupplyStructure structure in ConnectedStructures)
        {
            if (structure.DockShipToStructure(ship))
            {
                return true;
            }
        }
        throw new System.Exception("No Dock available");
    }

    public ItemBlueprint FindFuel()
    {
        var fuels = GameManager.instance.data.blueprintsModel.Model.blueprints.FindAll(x => x.GetResearch().name == "Fuel Cell");
        if (fuels.Count == 0)
            return null;

        //Seperate fuels into ones that can fill capacity and ones that can't
        List<ItemBlueprint> fuelsComplete = new List<ItemBlueprint>();
        List<ItemBlueprint> fuelsPartial = new List<ItemBlueprint>();
        double remainingCap = storage.RemainingCapacity(fuels[0].Id);
        foreach (ItemBlueprint fuel in fuels)
        {
            if (fuel.Supply >= remainingCap)
            {
                fuelsComplete.Add(fuel);
            }
            else if (fuel.Supply > 0)
            {
                fuelsPartial.Add(fuel);
            }
        }

        //Pick fuel
        if (fuelsComplete.Count > 0)
        {
            return fuelsComplete[Random.Range(0, fuelsComplete.Count - 1)];
        }
        else if (fuelsPartial.Count > 0)
        {
            return fuelsPartial[Random.Range(0, fuelsPartial.Count - 1)];
        }
        return null;


    }
}