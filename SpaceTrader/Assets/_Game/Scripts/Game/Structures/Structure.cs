﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeControl;
using System.Xml.Serialization;

[System.Serializable]
public class Structure : PositionEntity
{
    private static int minUpdatedDuration = Dated.Hour;
    private static int maxUpdateDuration = Dated.Day;
    [SerializeField]
    private double updateTime;
    [SerializeField]
    private int _count;
    [SerializeField]
    private string _managerId;
    [SerializeField]
    private double _maxArmor;
    [SerializeField]
    private double _currentArmor;
    [SerializeField]
    private Dated _dateCreated;
    [SerializeField]
    private Dated _lastUpdated;
    [SerializeField]
    private bool _deleteStructure;
    [SerializeField]
    protected List<UpdateDel> updateList = new List<UpdateDel>();

    public ConstructionStatus constructionStatus = ConstructionStatus.Constructed;
    public float constructionProgress = 0;
    public string structureBlueprintId;
    public List<string> constructionContractIds;
    public ModelRef<IdentityModel> owner;
    public MoneyBudget money;
    public Storage storage;
    public double deltaTime = 1;
    public List<string> incomingShips = new List<string>();

    public string Info { get; set; }
    public int Count { get => _count; set => _count = value; }
    public string ManagerId { get => _managerId; set => _managerId = value; }
    public double MaxArmor { get => _maxArmor; set => _maxArmor = value; }
    public double CurrentArmor { get => _currentArmor; set => _currentArmor = value; }
    public Dated DateCreated { get => _dateCreated; set => _dateCreated = value; }//new Dated(GameManager.instance.data.date.time);
    public Dated LastUpdated { get => _lastUpdated; set => _lastUpdated = value; }//new Dated(GameManager.instance.data.date.time);
    public bool DeleteStructure { get => _deleteStructure; set => _deleteStructure = value; }

    public Structure() { }

    public Structure(IdentityModel identityModel, string structureBlueprint, string _referenceId, Vector3d _localPosition, bool construct = false) :
        base(_referenceId, _localPosition)
    {
        if (_referenceId != "") //Avoids adding structures added to no reference, or spawned in empty galactic space.
        {
            GameManager.instance.locations[_referenceId].PositionEntityIds.Add(GetId());
        }
        updateTime = Random.Range(Dated.Hour, Dated.Day);
        owner = new ModelRef<IdentityModel>(identityModel);
        identityModel.AddKnownSolarBodyId(GetReferenceId());
        DateCreated = new Dated(GameManager.instance.data.date.time);
        LastUpdated = new Dated(GameManager.instance.data.date.time);

        this.structureBlueprintId = structureBlueprint;

        var research = GetStructureResearch();
        MaxArmor = research.baseArmor;
        CurrentArmor = MaxArmor;

        if (construct)
        {
            constructionStatus = ConstructionStatus.WaitingForConstructionMaterials;
            constructionContractIds = new List<string>();
            constructionProgress = 0;
        }
        else constructionProgress = 1;

        //OVERWRITES REFERENCE ID AND POSITION IF STRUCTURE PART OF A STATION
        if (research.tags.Contains(ResearchTags.Station))
        {
            //Find station
            var stations = GetReferenceBody().Structures.FindAll(x => x as Station != null);
            int stationCount = 1;
            Station pickedStation = null;
            if (stations != null)
            {
                stationCount += stations.Count;
                foreach (Station station in stations)
                {
                    if (station.childStructureIds.Count < station.attachmentCount)
                    {
                        //Add to station
                        station.childStructureIds.Add(GetId());
                        SetReferenceId(station.GetId());
                        SetReferencePosition(Vector3d.zero);
                        pickedStation = station;
                        break;
                    }
                }
            }

            if (pickedStation == null)
            {
                //Find station blueprints
                var possibleBaseStationBlueprints = GameManager.instance.data.blueprintsModel.Model.blueprints.FindAll(x => x.GetResearch().name == "Station Base");

                Station station;

                if (possibleBaseStationBlueprints.Count != 0)
                {
                    //Pick basestation station Blueprint
                    station = new Station(identityModel.name + " Station " + stationCount, identityModel, possibleBaseStationBlueprints[Random.Range(0, possibleBaseStationBlueprints.Count)].Id, GetReferenceId(), 4);
                }
                else
                {
                    //Create company to manage blueprints related to the research
                    Creature manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, identityModel.referenceId, GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);

                    new CompanyModel(identityModel.Owner.name + " Station Base Branch", "Station Base", GameManager.instance.locations[identityModel.referenceId] as SolarBody, identityModel.Government, manager, identityModel.Owner);

                    //Create blueprint
                    var blueprint = identityModel.Owner.GetOwnedBlueprints().Find(x => x.research == "Station Base");

                    if (blueprint == null) throw new System.Exception("Could not find created structure blueprint for: " + "Station Base");

                    station = new Station(identityModel.name + " Station " + stationCount, identityModel, blueprint.Id, GetReferenceId(), 4);

                }

                //Add structure to station
                station.childStructureIds.Add(GetId());
                SetReferenceId(station.GetId());
                SetReferencePosition(Vector3d.zero);
            }

            // Add to UpdateList
            updateList.Add(() => Construction());

        }
    }

    public virtual void Update()
    {
        deltaTime = GameManager.instance.data.date.time - LastUpdated.time;
        LastUpdated.AddTime(deltaTime);

        if (DeleteStructure)
            return;

        // Operations to run when updateTimeHit
        while (updateTime <= LastUpdated.time)
        {
            updateTime += Random.Range(minUpdatedDuration, maxUpdateDuration);

            foreach (UpdateDel update in updateList)
            {
                update();
            }
        }

    }

    /// <summary>
    /// Allows for structures to be constructed on site, rather than the full thing being constructed at a factory
    /// TODO: currently depreciated, need to implement
    /// </summary>
    public void Construction()
    {
        if (constructionStatus != ConstructionStatus.Constructed)
        {
            constructionStatus = ConstructionStatus.Constructed;
            while (true)
            {
                break; //TODO: Figure out how construction of structures will work

                //if (constructionStatus == ConstructionStatus.UnderConstruction)
                //{
                //    var productionDone = (float)(deltaTime / GetStructureBlueprint().workAmount);
                //    constructionProgress += productionDone;
                //    if (constructionProgress > 1)
                //    {
                //        constructionProgress = 1;
                //        constructionStatus = ConstructionStatus.Constructed;
                //        break;
                //    }
                //    else
                //    {
                //        break;
                //    }
                //}
                //else if (constructionStatus == ConstructionStatus.WaitingForConstructionMaterials)
                //{
                //    //TODO: Add the ability to find and set contracts to build structure. NOTE: This may have already been accomplished in ProductionStructure and its construction mechanism
                //    //Check to see if has all 
                //}
                //else
                //{
                //    break;
                //}
            }
        }
    }

    public ItemBlueprint GetStructureModel()
    {
        return GameManager.instance.data.blueprintsModel.Model.GetItem(structureBlueprintId);
    }

    public Research GetStructureResearch()
    {
        return GetStructureModel().GetResearch();
    }

    public virtual Contract SearchContracts(string itemId, double amount, ContractType contractType = ContractType.Supply)
    {
        //Find a contract
        string contractId = "";
        double itemAmount = amount;
        contractId = owner.Model.Government.GetCheapestContractId(itemId, GetId(), itemAmount);

        if (contractId != "" && contractId != null)
        {
            Contract contract = GameManager.instance.contracts[contractId];

            //Set wanted conditions;
            contract.client = new ModelRef<IdentityModel>(owner.Model);
            contract.destinationId = GetId();
            contract.contractType = contractType;
            contract.contractState = ContractState.Sent;

            if (itemAmount < contract.itemAmount)
            {
                contract.alternateItemAmount = itemAmount;
            }
            else
            {
                contract.alternateItemAmount = contract.itemAmount;
            }

            return contract;
        }
        return null;
    }

    protected delegate void UpdateDel();

}

public enum ConstructionStatus
{
    WaitingForConstructionMaterials,
    UnderConstruction,
    Constructed
}
