﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistributionCenter : SupplyStructure {

    private double resetDuration = Dated.Day;
    private double lastReset = GameManager.instance.data.date.time;

    public DistributionCenter() { }

    public DistributionCenter(IdentityModel owner, SolarBody body, int _count = 1):
        base(owner, GameManager.instance.data.blueprintsModel.Model.blueprints.Find(x => x.research == "Distribution Center").Id, body.GetId(), (Vector3d)Random.onUnitSphere * ((SolarBody)GameManager.instance.locations[body.GetId()]).GetBodyRadius() * .5)
    {

        //Add required items
        materialBlueprints = new Dictionary<string, int>();
        foreach (ItemBlueprint item in GameManager.instance.data.blueprintsModel.Model.blueprints)
        {
            materialBlueprints.Add(item.Id,1);
        }

        workers = 100;
        //workAmount = 1 * 24 * 30; //Optimal Rate of 1 a month TODO: Either fix workAmount in Blueprint --or--> Create better system for distributing items to populace

        SetName("Distribution Center " + GetId());

        //Set IStructure Properties
        //maxArmor = 100;
        //currentArmor = maxArmor;

        //Create storage
        Dictionary<string, double> storeCapacity = new Dictionary<string, double>();

        foreach (KeyValuePair<string, int> construction in materialBlueprints)
        {
            var blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(construction.Key);

            storeCapacity[blueprint.research] = 1;
        }

        storage = new Storage(StorageType.Research, storeCapacity);
    }

    public override void Update()
    {
        base.Update();
        // Operations to run when updateTimeHit

        if (lastReset + resetDuration < GameManager.instance.data.date.time)
        {
            //Info = "Count: " + count + "\nItemCount: " + itemsStorage.Count;
            //Create storage
            Dictionary<string, double> storeCapacity = new Dictionary<string, double>();

            foreach (KeyValuePair<string, int> construction in materialBlueprints)
            {
                var blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(construction.Key);

                storeCapacity[blueprint.research] = 1;
            }

            storage = new Storage(StorageType.Research, storeCapacity);

            lastReset = GameManager.instance.data.date.time;
        }

    }
}
