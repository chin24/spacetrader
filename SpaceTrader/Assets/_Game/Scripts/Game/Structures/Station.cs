﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeControl;
using System.Xml.Serialization;

public class Station: SupplyStructure {

    

    public int attachmentCount;

    public double timePassed = 0;

    public float runningCost = 10f;

    public Station() { }

    public Station(string name, IdentityModel owner, string structureBlueprint, string referenceId, int attachmentCount, bool doConstruct = false):
        base( owner, structureBlueprint, referenceId, (Vector3d)Random.onUnitSphere * (((SolarBody)GameManager.instance.locations[referenceId]).GetBodyRadius() * .5 + 2500))
    {
        childStructureIds = new List<string>();
        this.attachmentCount = attachmentCount;

        this.SetName(name);

        ////Create storage
        Dictionary<string, double> storeCapacity = new Dictionary<string, double>();

        foreach (KeyValuePair<string, int> construction in materialBlueprints)
        {
            var blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(construction.Key);

            storeCapacity.Add(blueprint.research, 10000);
        }


        storage = new Storage(StorageType.Research, storeCapacity);

        //Fill product to half full

        if (!doConstruct)
        {
            foreach (KeyValuePair<string, int> construction in materialBlueprints)
            {
                var blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(construction.Key);

                blueprint.BuyItemInsta(5000, "Supplier Contracts", ref owner.money, "Income", ref blueprint.owner.Model.money);
                storage.AddItem(blueprint.Id, GetId(), GetId(), 5000);
            }


        }
    }

    public override void Update()
    {
        base.Update();
    }

    /// <summary>
    /// Item is Being Baught from station
    /// </summary>
    /// <param name="itemName"></param>
    /// <param name="itemType"></param>
    /// <param name="itemAmount"></param>
    /// <param name="buyer"></param>
    /// <returns></returns>
    internal Item Buy(Item buyItem, IdentityModel buyer)
    {
        return null;
        //    foreach (ProductionItem item in factory.outputItems)
        //    {
        //        if (item.name == buyItem.name)
        //        {
        //            if (buyItem.amount > item.amount)
        //            {
        //                buyItem.amount = item.amount;
        //            }
        //            ProductionItem soldItem = (ProductionItem) buyItem;
        //            soldItem.price = item.price;
        //            soldItem.totalPrice = soldItem.price * soldItem.amount;

        //            money += soldItem.totalPrice;
        //            item.amount -= buyItem.amount;
        //            buyer.money -= soldItem.totalPrice;
        //            buyItem.amount = 0;
        //            buyItem.pendingAmount = soldItem.amount;
        //            return buyItem;
        //        }

        //    }

        //    return buyItem * 0;
    }
    ///// <summary>
    ///// Item is being sold to station
    ///// </summary>
    ///// <param name="sellItem"></param>
    ///// <returns>Return much item that was actually sold to station</returns>
    internal Item Sell(Item sellItem, IdentityModel seller)
    {
        return null;
        //    foreach (ProductionItem item in factory.inputItems)
        //    {
        //        if (item.name == sellItem.name)
        //        {
        //            item.pendingAmount += sellItem.amount;
        //            float price = item.price * sellItem.amount;
        //            money -= price;
        //            return sellItem;
        //        }
        //    }

        //    return sellItem * 0;
        //}
        //internal void SellIncomplete(Items sellItem)
        //{
        //    foreach (ProductionItem item in factory.inputItems)
        //    {
        //        if (item.name == sellItem.name)
        //        {
        //            item.pendingAmount -= sellItem.amount;
        //            money += item.price * sellItem.amount;
        //        }
        //    }
    }
    }
