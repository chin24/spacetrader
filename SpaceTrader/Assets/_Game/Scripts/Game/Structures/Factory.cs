﻿using UnityEngine;
using System.Collections.Generic;
using CodeControl;

public class Factory : ProductionStructure {

    public Factory() { }

    public Factory(IdentityModel owner, string structureBlueprint, string productionItemId, string _referenceId, int _count = 1, bool construct= false):
        base(owner, structureBlueprint, productionItemId, _referenceId, (Vector3d)Random.onUnitSphere * ((SolarBody)GameManager.instance.locations[_referenceId]).GetBodyRadius() * .5, _count, construct)
    {
        //TODO: Needs to go in Factory Blueprint
        //int machineryAmount = (int)blueprint.contstructionParts.Find(x => x.itemType == ItemType.FactoryMachinery).amount;
        //workAmount /= machineryAmount;
    }

    /// <summary>
    /// Creates items and uses items based on the elapsed time
    /// </summary>
    /// <param name="elapsedTime">time elapsed (in seconds)</param>
    public override void Update()
    {
        base.Update();

        //var price = parentBody.GetMarketPrice(productionItemId) / productionTime;
        //var cost = workers * workerPayRate;

        //requiredItems.ForEach(x => cost += x.amount * x.price / GameManager.instance.data.itemsData.Model.GetItem(x.id).productionTime);
        Info = "Count: " + Count + "\nOn: " + on.ToString()
            + "\nIs Producing: " + isProducing.ToString();
            //+ "\nPrice/Cost " + (price / cost).ToString("0.0000");

        var progress = (float)(deltaTime / ProductionTime);

        if (storage.RemainingCapacity(productionItemId) > 0)
        {
            if (isProducing)
            {
                productionProgress += progress;
                while (productionProgress > 1)
                {


                    //productionProgress -= Mathd.Floor(productionProgress);

                    if (SearchRequiredItems())
                    {
                        isProducing = true;
                        UseRequiredItems();
                        productionProgress--;
                    }
                    else
                    {
                        productionProgress = 0;
                        isProducing = false;
                        //requiredItems.ForEach(x => x.price = ReferenceBody.GetMarketPrice(x.id));
                    }
                }
            }
            else if (SearchRequiredItems())
            {
                isProducing = true;
            }
        }
        else
            isProducing = false;
        
    }   
}
