﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipInteract : MonoBehaviour {

    public SolarBody solar;

    public void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
            ToolTip.instance.SetTooltip(solar.GetName(), String.Format("Satellites: {0}", solar.Satelites.Count));
    }
    public void OnMouseExit()
    {
        ToolTip.instance.CancelTooltip();
    }

    public void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            UIManager.instance.SelectSolarSystem(solar);
        }
            
    }
}
