﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class GovernmentInfoPanel
{
    public static void BuildGovernment( this InfoPanelController contr)
    {
        GovernmentModel gov = (GovernmentModel)contr.model.target.Model;
        contr.title.text = gov.name;

        //===============================
        var position = contr.CreatePanel("Overview",gov.spriteColor);

        contr.Button(position, () =>
        {
                var creature = GameManager.instance.data.GetCreature(gov.leaders[0]);
                return "Leader: " + creature.GetName();
        }, () =>
        {
            var creature = GameManager.instance.data.GetCreature(gov.leaders[0]);
            GameManager.instance.OpenInfoPanel(creature);
        });

        contr.Button(position, () =>
        {
            return "Capital: " + GameManager.instance.data.getSolarBody(gov.referenceId).GetName();
        }, () =>
        {
            GameManager.instance.OpenInfoPanel(gov.referenceId);
        });

        contr.Button(position, () =>
        {
            return "Money: " + Units.ReadItem(gov.MoneyTotal) + "c";
        });


        contr.Button(position, () =>
        {
            return "Star Count:" + gov.starsId.Count;
        });

        contr.BuildList(gov.Id, gov.licensedCompanies.ToList().FindAll(x => x == x.Owner));

        //=====================
        position = contr.CreatePanel("All Contracts");

        contr.ButtonList<Contract>(gov.Id, gov.GetPostedContracts(), position, (contract) =>
        {
            string output = "Item: " + contract.itemName +
                     " : " + Units.ReadItem(contract.itemAmount + contract.itemAmountEnroute) +
                     "\nUnit Price: " + Units.ReadItem(contract.unitPrice) +
                     "c | Distance Cost: " + Units.ReadItem(contract.PricePerKm * contract.distance * 2) + " c";
            return output;
        }, (contract) => () => GameManager.instance.OpenInfoPanel(contract.id, TargetType.Contract), (contract, but) =>
        {

            Image image = but.GetComponent<Image>();
            if (contract.contractState == ContractState.Active) image.color = Color.green;
            else image.color = Color.white;
        });

        //===================
        position = contr.CreatePanel("Raw");

        contr.ButtonList<string, double>(gov.Id, gov.rawResourcesAmount, position, (raw) =>
         {
             string output = "Resource: " + raw.Key +
                      " : " + Units.ReadItem(1000000 / raw.Value) +
                      "c\nAmount: " + Units.ReadItem(raw.Value);
             return output;
         }, (raw) => () => GameManager.instance.OpenInfoPanel(raw.Key, TargetType.RawResource));
    }
}
