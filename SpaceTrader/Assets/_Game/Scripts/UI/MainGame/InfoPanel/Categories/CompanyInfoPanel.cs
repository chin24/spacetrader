﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class CompanyInfoPanel
{
    public static void BuildCompany( this InfoPanelController contr)
    {
        CompanyModel comp = contr.model.target.Model as CompanyModel;
        contr.title.text = comp.name;

        var position = contr.CreatePanel("Overview", comp.spriteColor);

        contr.Button(position, () =>
        {
            var creature = GameManager.instance.data.GetCreature(comp.ceoId);
            return "CEO: " + creature.GetName();
        }, () =>
        {
            var creature = GameManager.instance.data.GetCreature(comp.ceoId);
            GameManager.instance.OpenInfoPanel(creature);
        });

        contr.Button(position, () =>
        {
            return "Headquarters: " + GameManager.instance.data.getSolarBody(comp.referenceId).GetName();
        }, () =>
        {
            GameManager.instance.OpenInfoPanel(comp.referenceId);
        });

        contr.Button(position, () =>
        {
            return "Research: " + comp.researchFocus;
        }, () =>
        {
            GameManager.instance.OpenInfoPanel(comp.researchFocus, TargetType.Research);
        });

        contr.Button(position, () =>
        {
            return "Money: " + Units.ReadItem(comp.MoneyTotal) + "c";
        });
    }
}
