﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class IdentityInfoPanel
{
    public static void BuildIdentityModel( this InfoPanelController contr)
    {
        IdentityModel identityModel = contr.model.target.Model;

        //======================
        if (identityModel.entities != null)
            contr.BuildList(identityModel.Id, "Branches", identityModel.entities.ToList());

        //======================
        var position = contr.CreatePanel("Money");



        //var graph = Object.Instantiate(emptyGraphPrefab,position);
        //graph.yAxis.MaxAutoGrow = true;
        //graph.yAxis.MaxAutoShrink = true;
        //graph.yAxis.MinAutoGrow = true;
        //graph.yAxis.MaxAutoShrink = true;
        //graph.xAxis.MaxAutoGrow = true;
        //graph.xAxis.MaxAutoShrink = true;

        //TextUpdates += () =>
        //{
        //    while (graph.lineSeries.Count > 0)
        //        graph.deleteSeries();

        //    foreach (KeyValuePair<int, double[]> update in identityModel.money.updateInterval)
        //    {
        //        foreach (KeyValuePair<string, Dictionary<double, double>> cat in identityModel.money.moneyTracking[update.Key])
        //        {
        //            if (cat.Value.Count == 0)
        //            {
        //                break;
        //            }
        //            string name = cat.Key;
        //            WMG_List<Vector2> list = new WMG_List<Vector2>();
        //            foreach (KeyValuePair<double, double> money in cat.Value)
        //            {
        //                list.Add(new Vector2((float)money.Key, (float)money.Value));
        //            }

        //            if (graph.lineSeries.Exists(x => x.name == name))
        //            {
        //                graph.lineSeries.Find(x => x.name == name).GetComponent<WMG_Series>().pointValues = list;
        //            }
        //            else
        //            {
        //                var series = graph.addSeries();
        //                series.gameObject.name = name;
        //                UnityEngine.Random.InitState(name.GetHashCode());
        //                series.lineColor = UnityEngine.Random.ColorHSV(.5f,1f,.5f,1,.5f,1);
        //                series.seriesName = name;
        //                series.pointValues = list;
        //            }
        //        }
        //    }
        //    graph.Refresh();
        //};
        //graph.Init();



        contr.Button(position, () =>
        {
            string output = "";
            foreach (KeyValuePair<int, double[]> update in identityModel.money.updateInterval)
            {
                foreach (KeyValuePair<string, Dictionary<double, double>> cat in identityModel.money.moneyTracking[update.Key])
                {
                    if (cat.Value.Count == 0)
                    {
                        break;
                    }

                    if (cat.Value != null)
                    {
                        output += "\n" + cat.Key;

                        foreach (KeyValuePair<double,double> data in cat.Value)
                        {
                            output += " | " + Units.ReadItem(data.Value);
                        }
                    }
                    else
                    {
                        output += "\n" + cat.Key;
                    }
                }
            }
            return output;
        });

        //=========================
        position = contr.CreatePanel("Budget");


        contr.ButtonList<string, double[]>(identityModel.Id, identityModel.money.budget, position, (budget) =>
        {
            string output = "Budget " + budget.Key +
                     " (" + (budget.Value[0] * 100).ToString("0.0") + ") " +  Units.ReadItem(budget.Value[1]) + " of " + Units.ReadItem(budget.Value[2]);
            return output;
        }, (budget) => () => {
            if (GameManager.instance.research.allResearch.ContainsKey(budget.Key))
            GameManager.instance.OpenInfoPanel(budget.Key, TargetType.Research);
        });

        if (identityModel.research != null)
        {
            //===============================================
            position = contr.CreatePanel("Research");


            contr.ButtonList<string, double>(identityModel.Id, identityModel.research, position, (research) =>
            {
                string output = "Research" + research.Key +
                         " : " + Units.ReadItem(research.Value);
                return output;
            }, (research) => () => GameManager.instance.OpenInfoPanel(research.Key, TargetType.Research));
        }

        if (identityModel.ownedBlueprintIds != null || identityModel.ownedBlueprintIds.Count != 0)
        {
            //================================
            position = contr.CreatePanel("Blueprints");


            contr.ButtonList<ItemBlueprint>(identityModel.Id, identityModel.GetOwnedBlueprints(), position, (blueprint) =>
            {
                string output = "Name: " + blueprint.coloredName +
                         " : " + Units.ReadItem(blueprint.unitsCreated);
                return output;
            }, (blueprint) => () => GameManager.instance.OpenInfoPanel(blueprint.Id, TargetType.Blueprint));
        }

        //=====================================
        position = contr.CreatePanel("Own Contracts");

        contr.ButtonList<Contract>(identityModel.Id, identityModel.GetConstructionContracts(), position, (contract) =>
        {
            string output = "Constructing: " + contract.itemName +
                     " : " + Units.ReadItem(contract.cost) +
                     "c\nUnit Price: " + Units.ReadItem(contract.unitPrice) +
                     "c | Distnace Cost: " + Units.ReadItem(contract.PricePerKm * contract.distance * 2) + " c";
            return output;
        }, (contract) => () => GameManager.instance.OpenInfoPanel(contract.id, TargetType.Contract), (contract, but) => {

            Image image = but.GetComponent<Image>();
            if (contract.contractState == ContractState.Active) image.color = Color.green;
            else image.color = Color.white;
        });

        contr.ButtonList<Contract>(identityModel.Id, identityModel.GetContracts(), position, (contract) =>
        {
            string output = "Item: " + contract.itemName +
                     " : " + Units.ReadItem(contract.itemAmount + contract.itemAmountEnroute) +
                     "\nUnit Price: " + Units.ReadItem(contract.unitPrice) +
                     "c | Distance Cost: " + Units.ReadItem(contract.PricePerKm * contract.distance * 2) + " c";
            return output;
        }, (contract) => () => GameManager.instance.OpenInfoPanel(contract.id, TargetType.Contract), (contract, but) => {

            Image image = but.GetComponent<Image>();
            if (contract.contractState == ContractState.Active) image.color = Color.green;
            else image.color = Color.white;
        });

        

        //======================================
        position = contr.CreatePanel("Own Ships");

        contr.ButtonList<Ship>(identityModel.Id, identityModel.GetOwnedShips(), position, (ship) =>
        {
            string output = "Name : " + ship.GetName() +
                "\nLocation: " + GameManager.instance.locations[ship.GetReferenceId()].GetName() +
                "\nCurrent Action: " + ship.shipAction.ToString();
            if (ship.ShipTargetId != null)
            {
                if (GameManager.instance.locations[ship.ShipTargetId] as Structure != null)
                {
                    Structure structure = GameManager.instance.locations[ship.ShipTargetId] as Structure;
                    double distance = Vector3d.Distance(structure.SystemPosition, ship.SystemPosition);
                    output += "\nTarget: " + structure.GetName() +
                    "\nDistance: " + Units.ReadDistance(distance) +
                    " : ETA: " + Dated.ReadTime(distance / ship.SubLightSpeed);
                }
                else
                {
                    SolarBody body = GameManager.instance.locations[ship.ShipTargetId] as SolarBody;
                    double distance = Vector3d.Distance(body.SystemPosition, ship.SystemPosition);
                    output += "\nTarget: " + body.GetId() +
                    "\nDistance: " + Units.ReadDistance(distance) +
                    " : ETA: " + Dated.ReadTime(distance / ship.SubLightSpeed);

                }

            }
            return output;
        }, (ship) => () => GameManager.instance.OpenInfoPanel(ship), (ship, but) => {

            Image image = but.GetComponent<Image>();
            if (ship.shipAction == ShipAction.Moving) image.color = Color.green;
            else if (ship.shipAction == ShipAction.LookingForDock) image.color = Color.yellow;
            else if (ship.shipAction == ShipAction.Docking) image.color = Color.cyan;
            else if (ship.shipAction == ShipAction.Docked) image.color = Color.blue;
            else if (ship.shipAction == ShipAction.Working) image.color = Color.magenta;
            else image.color = Color.white;
        });

        contr.BuildList(identityModel.Id,identityModel.GetStructures());
    }
}
