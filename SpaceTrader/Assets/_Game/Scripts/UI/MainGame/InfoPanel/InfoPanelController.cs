﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeControl;
using System;
using UnityEngine.UI;

public class InfoPanelController : Controller<InfoPanelModel> {

    public WMG_Axis_Graph emptyGraphPrefab;
    public Button uiButtonInstance;
    public Text title;
    public GameObject titleContent;
    public ScrollView scrollViewPrefab;
    public GameObject infoContentPanel;
    public PlanetGridPanel planetTiles;
    public UpdateDel TextUpdates;
    public GameObject contentPosition;
    /// <summary>
    /// Used to create tabs in the info panel.
    /// </summary>
    internal List<GameObject> menuContent = new List<GameObject>();
    
    public bool fix = true;

    protected override void OnInitialize()
    {
        TextUpdates = () => { };
        if (model.targetType == TargetType.Governments)
        {
            BuildGovernments();
        }
        else if (model.targetType == TargetType.Blueprint)
        {
            BuildBlueprint();
        }
        else if (model.targetType == TargetType.Contract)
        {
            BuildContract();
        }
        else if (model.targetType == TargetType.Research)
        {
            BuildResearch();
        }
        else if (model.targetType == TargetType.Identity)
        {
            if (model.target.Model.GetType() == typeof(GovernmentModel))
            {

                this.BuildGovernment();
                
            }
            else
            {
                this.BuildCompany();
                
            }

            this.BuildIdentityModel();

            SelectPanel(0);
        }
        else if (model.targetType == TargetType.SolarBody)
        {
            SolarBody body = GameManager.instance.data.getSolarBody(model.id);
            title.text = body.GetName();
            Text text;

            if (body.GetSolarSubType() != SolarSubTypes.GasGiant && body.GetSolarType() != SolarTypes.Star)
            {
                var tiles = Instantiate(planetTiles, contentPosition.transform);
                tiles.SelectPlanet(body);
            }

            Button button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Overview";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            var index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            var position = menuContent[index].transform;

            if (body.GetSolarType() != SolarTypes.Star)
            {
                button = Instantiate(uiButtonInstance, position);
                var buttonText = button.GetComponentInChildren<Text>();
                buttonText.text = "Parent: " + body.GetReferenceBody().GetName();
                button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(body.GetReferenceBody().GetId(), TargetType.SolarBody));
            }

            if (body.GetSolarSubType() != SolarSubTypes.GasGiant && body.GetSolarType() != SolarTypes.Star && body.Inhabited)
            {
                Button(position, () =>
                {
                    return "Population: " + Units.ReadItem(body.GetSolarPopulation().GetTotalPopulation()) + "\n"
                    + "Young: " + Units.ReadItem(body.GetSolarPopulation().GetYoung()) + "\n"
                    + "Adult: " + Units.ReadItem(body.GetSolarPopulation().GetAdult()) + "\n"
                    + "Old: " + Units.ReadItem(body.GetSolarPopulation().GetOld());
                });
            }


            Button(position, () =>
            {
                return "Structure Count: " + body.Structures.Count;
            });

            if (body.GetSolarSubType() != SolarSubTypes.GasGiant && body.GetSolarType() != SolarTypes.Star)
            {

                Button(position, () =>
                {
                    return "Resource Types Count: " + body.GetRawResources().Count;
                });

                Button(position, () =>
                {
                    return "Resource Types Count: " + body.GetRawResources().Count;
                });

                

            }

            button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Orbit";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            position = menuContent[index].transform;

            button = Instantiate(uiButtonInstance, position);
            text = button.GetComponentInChildren<Text>();
            text.text = body.GetInfo(GameManager.instance.data.date.time);
            button.interactable = false;

            if (body.Satelites.Count > 0)
            {
                button = Instantiate(uiButtonInstance, titleContent.transform);
                button.GetComponentInChildren<Text>().text = "Satellites";
                //name.GetComponent<Image>().color = gov.spriteColor;
                menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
                index = menuContent.Count - 1;
                button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
                position = menuContent[index].transform;

                ButtonList<SolarBody>(body.GetId(), body.Satelites, position, (x) =>
                {
                    string output = x.GetName();
                    if (x.Satelites.Count > 0)
                        output += " (" + Units.ReadItem(x.Satelites.Count) + ")";
                    if (x.companies.Count > 0)
                        output += " - " + x.companies.Count;
                    return output;
                }, (x) => () => GameManager.instance.OpenInfoPanel(x.GetId(), TargetType.SolarBody), (x, but) => {
                    Image image = but.GetComponent<Image>();
                    image.color = x.GetSolarColor();
                });
            }

            if (body.GetSolarSubType() != SolarSubTypes.GasGiant && body.GetSolarType() != SolarTypes.Star)
            {
                button = Instantiate(uiButtonInstance, titleContent.transform);
                button.GetComponentInChildren<Text>().text = "Resources";
                //name.GetComponent<Image>().color = gov.spriteColor;
                menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
                index = menuContent.Count - 1;
                button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
                position = menuContent[index].transform;

                ButtonList<RawResource>(body.GetId(), body.GetRawResources(), position, (x) =>
                {
                    string output = "Resource: " + x.GetName() + " - " + Units.ReadItem(x.GetAmount());
                    if (x.owner != null)
                        output += "\nOwner: " + x.owner.Model.name;
                    if (x.user != null)
                        output += "\nUser: " + x.user.Model.name;
                    if (x.GetTimeUntilDepleted() > 0) output += "\nTime to Depletion: " + Dated.ReadTime(x.GetTimeUntilDepleted());
                    return output;
                }, (x) => () => GameManager.instance.OpenInfoPanel(x), (x, but) => {

                    Image image = but.GetComponent<Image>();
                    if (x.GetTimeUntilDepleted() > 0) image.color = Color.green;
                    else image.color = Color.white;
                });

                

                BuildList(body.GetId(), "Companies", body.companies.ToList().FindAll(x => x == x.Owner));

                BuildList(body.GetId(), body.Structures);

            }

            SelectPanel(0);
        }
        else if (model.targetType == TargetType.Structure)
        {
            BuildStructurePanel();

            SelectPanel(0);
        }
        else if (model.targetType == TargetType.Item)
        {
            var item = GameManager.instance.data.blueprintsModel.Model.GetItem(model.id);
            title.text = item.Name;

            Button button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Overview";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            var index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            var position = menuContent[index].transform;

            //button = Instantiate(uiButtonInstance, position);
            //button.GetComponentInChildren<Text>().text = "Type: " + item.itemType.ToString();
            //button.interactable = false;

            button = Instantiate(uiButtonInstance, position);
            button.GetComponentInChildren<Text>().text = "Description: " + item.GetResearch().description;
            button.interactable = false;

            button = Instantiate(uiButtonInstance, position);
            button.GetComponentInChildren<Text>().text = "Work Amount: " + item.workAmount;
            button.interactable = false;

            //if (item.workers > 0)
            //{
            //    button = Instantiate(uiButtonInstance, position);
            //    button.GetComponentInChildren<Text>().text = "Workers: " + Units.ReadItem(item.workers);
            //    button.interactable = false;
            //}



            if (item.GetResearch().category != ResearchCategory.Raw)
            {
                button = Instantiate(uiButtonInstance, titleContent.transform);
                button.GetComponentInChildren<Text>().text = "Materials";
                //name.GetComponent<Image>().color = gov.spriteColor;
                menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
                index = menuContent.Count - 1;
                button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
                position = menuContent[index].transform;

                foreach(KeyValuePair<string,int> blueprint in item.ConstructionBlueprints)
                {
                    button = Instantiate(uiButtonInstance, position);
                    var buttonText = button.GetComponentInChildren<Text>();
                    buttonText.text = "Name: " + GameManager.instance.data.blueprintsModel.Model.GetItem(blueprint.Key).coloredName + " - " + Units.ReadItem(blueprint.Value);
                    button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(blueprint.Key));
                }
            }


            SelectPanel(0);
        }


    }

    private void BuildStructurePanel()
    {
        model.structure = GameManager.instance.locations[model.id] as Structure;
        var positionEntity = GameManager.instance.locations[model.structure.GetReferenceId()];
        title.text = model.structure.GetName();
        Text text;
        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = "Overview";
        //name.GetComponent<Image>().color = gov.spriteColor;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        var index = menuContent.Count - 1;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
        var position = menuContent[index].transform;

        button = Instantiate(uiButtonInstance, position);
        var owner = model.structure.owner.Model;
        button.GetComponentInChildren<Text>().text = "Owner: " + owner.name;
        button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(owner));

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Location: " + positionEntity.GetName();
        button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(positionEntity.GetId()));

        Button(position, () =>
        {
            return model.structure.Info;
        });

        //Button(position, () =>
        //{
        //    return "Storage Filled: " + (((model.structure).currentStorageAmount / (model.structure).totalStorageAmount) * 100 * model.structure.Count).ToString("0.00") + " %  "
        //        + Units.ReadItem(((DistributionCenter)model.structure).currentStorageAmount) + " - " +
        //         Units.ReadItem(((DistributionCenter)model.structure).totalStorageAmount * model.structure.Count);
        //});

        if (model.structure.GetStructureResearch().tags.Contains(ResearchTags.ProducesRawProducts) && model.structure.GetType() == typeof(Factory))
        {
            button = Instantiate(uiButtonInstance, position);
            text = button.GetComponentInChildren<Text>();
            text.text = "Mining: " + ((Driller)model.structure).productionItemName;
            button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(((Driller)model.structure).productionItemId, TargetType.Blueprint));
        }
        if (model.structure as SupplyStructure != null)
        {
            var supplyStructure = model.structure as SupplyStructure;

            if(supplyStructure.ConnectedStructures.Count > 0)
            {
                button = Instantiate(uiButtonInstance, position);
                text = button.GetComponentInChildren<Text>();
                text.text = "Connected Structures: ";
                button.interactable = false;

                ButtonList<SupplyStructure>(supplyStructure.GetId(), supplyStructure.ConnectedStructures, position, (structure) =>
               {
                   string output = structure.GetName();
                   return output;
               }, (structure) => () => GameManager.instance.OpenInfoPanel(structure.GetId(), TargetType.Structure), (contract, but) =>
               {

                   //Image image = but.GetComponent<Image>();
                   //if (contract.contractState == ContractState.Active) image.color = Color.green;
                   //else image.color = Color.white;
               });
            }

            if (model.structure.GetType() == typeof(Factory))
            {
                button = Instantiate(uiButtonInstance, position);
                button.GetComponentInChildren<Text>().text = "Producing Item: " + ((Factory)model.structure).productionItemName;
                button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(((Factory)model.structure).productionItemId, TargetType.Blueprint));

                Button(position, () =>
                {
                    return "Modified Production Time: " + Dated.ReadTime(((Factory)model.structure).ProductionTime);
                });

                Button(position, () =>
                {
                    return "Production Percent: " + ((((Factory)model.structure).productionProgress * 100).ToString("0.00") + " %");
                });
            }
        }

        if (model.structure.GetType() == typeof(Ship))
        {
            Ship ship = model.structure as Ship;

            Button(position, () =>
            {
                return "Current Action: " + ship.shipAction.ToString();
            });



            Button(position, () =>
            {
                if (ship.contractId != null)
                {
                    Contract contract = GameManager.instance.contracts[ship.contractId];
                    return (string)("Contract Name: " + contract.id +
                    "\nItem: " + contract.itemName +
                    "\nAmount: " + Units.ReadItem((double)contract.itemAmount + contract.itemAmountEnroute) +
                    "\nPrice: " + Units.ReadItem(contract.cost) +
                    "\nUnit Price: " + Units.ReadItem(contract.unitPrice) +
                    "c\nDistance Cost: " + Units.ReadItem(contract.PricePerKm * contract.distance * 2));
                }
                return "Contract: None";
            }, () =>
            {
                if (ship.contractId != null)
                {
                    GameManager.instance.OpenInfoPanel(ship.contractId, TargetType.Contract);
                }
            });

            Button(position, () =>
            {
                if (ship.ShipTargetId != null)
                {
                    string output = "";
                    if (GameManager.instance.locations[ship.ShipTargetId] as Structure != null)
                    {
                        Structure structure = GameManager.instance.locations[ship.ShipTargetId] as Structure;
                        double distance = Vector3d.Distance(structure.SystemPosition, ship.SystemPosition);
                        output += "Target: " + structure.GetName() +
                        "\nDistance: " + Units.ReadDistance(distance) +
                        "\nETA: " + Dated.ReadTime(distance / ship.SubLightSpeed);
                    }
                    else
                    {
                        SolarBody body = GameManager.instance.locations[ship.ShipTargetId] as SolarBody;
                        double distance = Vector3d.Distance(body.SystemPosition, ship.SystemPosition);
                        output += "Target: " + body.GetName() +
                        "\nDistance: " + Units.ReadDistance(distance) +
                        "\nETA: " + Dated.ReadTime(distance / ship.SubLightSpeed);
                    }

                    output += "\nMovement: " + ship.GetShipRoute()[ship.GetShipRouteIndex()].nodeType.ToString() +
                    " - " + ship.GetShipRoute()[ship.GetShipRouteIndex()].name;

                    return output;
                }
                return "Target: None";
            }, () => {
                if (ship.ShipTargetId != null)
                {
                    Structure structure = GameManager.instance.locations[ship.ShipTargetId] as Structure;
                    if (structure != null)
                        GameManager.instance.OpenInfoPanel(structure);
                    else
                    {
                        GameManager.instance.OpenInfoPanel(ship.ShipTargetId, TargetType.SolarBody);
                    }
                }
            });

            Button(position, () =>
            {
                if (ship.fuel != null)
                {
                    return "Fuel: " + (ship.fuel.AmountSize / ship.fuelCapacity * 100).ToString("0.00") + " %" +
                    "\nDistance: " + Units.ReadDistance(ship.fuelRange * ship.fuel.amount) +
                "\nETA: " + Dated.ReadTime((ship.fuelRange * ship.fuel.amount) / ship.SubLightSpeed);
                }
                return "Fuel: Empty";
            }, () => {
                if (ship.fuel != null)
                {
                    GameManager.instance.OpenInfoPanel(ship.fuel);
                }
            });
        }

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Go to -->";
        button.onClick.AddListener(() => GameManager.instance.GoToTarget(positionEntity.GetId()));

        ProductionStructure productionStructure = model.structure as ProductionStructure;
        if (productionStructure != null)
        {
            SolarBody body = positionEntity as SolarBody;
            button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Connections";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            position = menuContent[index].transform;

            if (productionStructure.GetType() != typeof(DistributionCenter))
            {
                Button(position, () =>
                {
                    return "Rate: " + Units.ReadRate(productionStructure.ProductionRateActual) + " of " + Units.ReadRate(productionStructure.ProductionRateOptimal);
                });

                Button(position, () =>
                {
                    return "Contracts Out: " + productionStructure.clientContracts.Count;
                });

                ButtonList<Contract>(productionStructure.GetId(), productionStructure.clientContracts, position, (contract) =>
                {
                    string output = "Item: " + contract.itemName +
                            " | Amount: " + Units.ReadItem(contract.itemAmount + contract.itemAmountEnroute) +
                            "u | Cost: " + Units.ReadItem(contract.cost) + "c";
                    if (contract.destinationId != null)
                    {
                        output += "\nDestination: " + GameManager.instance.locations[contract.destinationId].GetName();
                    }
                    return output;
                }, (contract) => () => GameManager.instance.OpenInfoPanel(contract.id, TargetType.Contract), (contract, but) => {

                    Image image = but.GetComponent<Image>();
                    if (contract.contractState == ContractState.Active) image.color = Color.green;
                    else image.color = Color.white;
                });
            }
            

            

            Button(position, () =>
            {
                return "Contracts In: " + productionStructure.supplierContractIds.Count;
            });

            ButtonList<Contract>(productionStructure.GetId(), productionStructure.GetSupplyContracts(), position, (contract) =>
            {
                string output = "Item: " + contract.itemName +
                        "\nAmount: " + Units.ReadItem(contract.itemAmount + contract.itemAmountEnroute) +
                        "\nCost: " + Units.ReadItem(contract.cost) +
                        "c\nOrigin: " + contract.Origin.GetName();
                return output;
            }, (contract) => () => GameManager.instance.OpenInfoPanel(contract.id, TargetType.Contract), (contract, but) => {

                Image image = but.GetComponent<Image>();
                if (contract.contractState == ContractState.Active) image.color = Color.green;
                else image.color = Color.white;
            });

            Button(position, () =>
            {
                return "Needed Items: " + productionStructure.neededItemAmount.Count;
            });

            ButtonList<string,double>(productionStructure.GetId(), productionStructure.neededItemAmount, position, (x) =>
            {
                ItemBlueprint item = GameManager.instance.data.blueprintsModel.Model.GetItem(x.Key);
                string output = item.Name + " | " + Units.ReadItem(x.Value);
                return output;
            }, (x) => () => GameManager.instance.OpenInfoPanel(x.Key, TargetType.Item));

        }

        button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = "Items";
        //name.GetComponent<Image>().color = gov.spriteColor;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        index = menuContent.Count - 1;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
        position = menuContent[index].transform;

        foreach( KeyValuePair<string,List<Item>> storage in model.structure.storage.GetStorage())
        {
            Button(position, () =>
            {
                return  storage.Key + ": " + model.structure.storage.itemCapacity[storage.Key][0] + "/" + model.structure.storage.itemCapacity[storage.Key][1];
            });

            ButtonList<Item>(model.structure.GetId(), storage.Value, position, (x) =>
            {
                string output = "Name: " + x.Name + " - " + Units.ReadItem(x.amount);
                if (x.destinationId != null) output += " | " + GameManager.instance.locations[x.destinationId].GetName();
                return output;
            }, (item) => () => GameManager.instance.OpenInfoPanel(item.id, TargetType.Item));
        }

        
    }


    internal void BuildList(string name, List<Structure> list)
    {
        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = "Structures";
        //name.GetComponent<Image>().color = gov.spriteColor;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        int index = menuContent.Count - 1;
        var position = menuContent[index].transform;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);

        ButtonList<Structure>(name, list, position, (structure) =>
        {
            string output = "Name : " + +structure.Count + " " + structure.GetName();

            if (structure as ProductionStructure != null)
            {
                ProductionStructure productionStructure = (ProductionStructure)structure;
                output += " (" + productionStructure.clientContracts.Count + ")";
            }

            output += " - " + GameManager.instance.locations[structure.GetReferenceId()].GetName();
            return output;
        }, (item) => () => GameManager.instance.OpenInfoPanel(item), (structure, but) => {

            if (structure as ProductionStructure != null)
            {
                ProductionStructure productionStructure = (ProductionStructure)structure;
                Image image = but.GetComponent<Image>();
                if (productionStructure.isProducing && productionStructure.clientContracts.Count > 0)
                {
                    image.color = Color.green;
                }
                else if (productionStructure.isProducing) image.color = Color.yellow;
                else if (productionStructure.clientContracts.Count > 0) image.color = Color.blue;
                else image.color = Color.red;
            }
        });
    }

    internal Transform CreatePanel ( string name, Color color)
    {
        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = name;
        button.GetComponent<Image>().color = color;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        var index = menuContent.Count - 1;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
        return menuContent[index].transform;
    }

    internal Transform CreatePanel(string name)
    {
        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = name;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        var index = menuContent.Count - 1;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
        return menuContent[index].transform;
    }

    public void BuildList(string id, string tabName, List<IdentityModel> companies)
    {
        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = tabName;
        //name.GetComponent<Image>().color = gov.spriteColor;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        int index = menuContent.Count - 1;
        Transform position = menuContent[index].transform;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);

        

        ButtonList<IdentityModel>(id, companies, position, (company) =>
        {
            string output = "Name: " + company.name;

            if (company as CompanyModel != null)
            {
                output += " | " + ((CompanyModel)company).researchFocus;
            }
            output += " | " + Units.ReadItem(company.MoneyTotal) +
                    "c\nShips: " + Units.ReadItem(company.ownedShipIds.Count) +
                    "\nLocation: " + GameManager.instance.locations[company.referenceId].GetName();
            if (company.Owner == company && company.entities != null)
                output += "\nBranches: " + company.entities.Count;
            return output;
        }, (company) => () => GameManager.instance.OpenInfoPanel(company), (company, but) => {

            Image image = but.GetComponent<Image>();
            if (company.MoneyTotal > 0) image.color = Color.green;
            else image.color = Color.red;
        });
    }

    internal void BuildList(string name, List<CompanyModel> companies)
    {
        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = "Companies";
        //name.GetComponent<Image>().color = gov.spriteColor;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        int index = menuContent.Count - 1;
        Transform position = menuContent[index].transform;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);

        ButtonList<CompanyModel>(name, companies, position, (company) =>
        {
            string output = "Name: " + company.name + " | " + (company).researchFocus + " | " + Units.ReadItem(company.MoneyTotal) +
                    "c\nShips: " + Units.ReadItem(company.ownedShipIds.Count) +
                    "\nLocation: " + GameManager.instance.locations[company.referenceId].GetName();
            if (company.Owner == company && company.entities != null)
                output += "\nBranches: " + company.entities.Count;
            return output;
        }, (company) => () => GameManager.instance.OpenInfoPanel(company), (company, but) => {

            Image image = but.GetComponent<Image>();
            if (company.MoneyTotal > 0) image.color = Color.green;
            else image.color = Color.red;
        });
    }

    private void BuildContract()
    {
        Contract contract = GameManager.instance.contracts[model.id];
        title.text = contract.itemName + " Contract: " + contract.id;

        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = "Overview";
        //name.GetComponent<Image>().color = gov.spriteColor;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        var index = menuContent.Count - 1;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
        var position = menuContent[index].transform;


        Button(position, () =>
        {
            return "State: " + contract.contractState.ToString();
        });

        Button(position, () =>
        {
            return "Duration: " + Dated.ReadTime(contract.duration);
        });

        if (contract.contractEndDate != null)
        {
            Button(position, () =>
            {
                return "Contract End Date: " + contract.contractEndDate.GetFormatedDate();
            });
        }

        //Button(position, () =>
        //{
        //    return "Renewable: " + contract.reknewable.ToString();
        //});

        IdentityModel identity = contract.owner.Model;
        button = Instantiate(uiButtonInstance, position);
        //if (company.contractState == ContractState.Active) button.GetComponent<Image>().color = Color.green;
        Text text = button.GetComponentInChildren<Text>();
        text.text = "Owner: " + identity.name;
        button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(identity));

        if (contract.client != null)
        {
            identity = contract.client.Model;
            button = Instantiate(uiButtonInstance, position);
            //if (company.contractState == ContractState.Active) button.GetComponent<Image>().color = Color.green;
            text = button.GetComponentInChildren<Text>();
            text.text = "Client: " + identity.name;
            button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(identity));
        }
        

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Item: " + contract.itemName;
        button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(contract.itemId, TargetType.Item));

        Button(position, () =>
        {
            return "Cost: " + Units.ReadItem(contract.cost) + "c";
        });

        Button(position, (StringDel)(() =>
        {
            return (string)("Total Amount: " + Units.ReadItem(contract.itemAmount + contract.itemAmountEnroute) + "u (" + Units.ReadItem(contract.itemAmountEnroute) + ")");
        }));

        Button(position, () =>
        {
            return "Unit Price: " + Units.ReadItem(contract.unitPrice) + "c";
        });

        Button(position, () =>
        {
            return "Price Per Gm: " + Units.ReadItem(contract.PricePerKm * Units.G) + "c";
        });

        Button(position, () =>
        {
            return "Distance: " + Units.ReadDistance(contract.distance);
        });

        Button(position, () =>
        {
            return "Distance Cost: " + Units.ReadItem(contract.PricePerKm * contract.distance * 2);
        });

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Origin: " + GameManager.instance.locations[contract.originId].GetName();
        button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(contract.originId));

        if (contract.destinationId != null)
        {
            button = Instantiate(uiButtonInstance, position);
            button.GetComponentInChildren<Text>().text = "Destination: " + GameManager.instance.locations[contract.destinationId].GetName();
            button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(contract.destinationId));
        }

        if (contract.shipIds.Count > 0)
        {
            

            button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Assigned Ships";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            index = menuContent.Count - 1;
            position = menuContent[index].transform;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            var scroll = Instantiate(scrollViewPrefab, position);
            position = scroll.Content.transform;

            ButtonList<Ship>(contract.id, contract.GetShips(), position, (ship) =>
            {
                string output = "Name : " + ship.GetName() +
                                "\nLocation: " + GameManager.instance.locations[ship.GetReferenceId()].GetName();
                if (ship.ShipTargetId != null)
                {
                    Structure structure = GameManager.instance.locations[ship.ShipTargetId] as Structure;
                    double distance = Vector3d.Distance(structure.SystemPosition, ship.SystemPosition);
                    output += "\nTarget: " + structure.GetId() +
                        "\nDistance: " + Units.ReadDistance(distance) +
                        "\nETA: " + Dated.ReadTime(distance / ship.SubLightSpeed);
                }
                return output;
            }, (ship) => () => GameManager.instance.OpenInfoPanel(ship), (ship, but) => {

                Image image = but.GetComponent<Image>();
                if (ship.shipAction == ShipAction.Moving) image.color = Color.green;
                else image.color = Color.grey;
            });
        }

        SelectPanel(0);
    }

    private void BuildGovernments()
    {
        title.text = "Governments                                                              " + GameManager.instance.data.governments.Count;

        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = "Overview";
        //name.GetComponent<Image>().color = gov.spriteColor;
        var scroll = Instantiate(scrollViewPrefab, contentPosition.transform);
        menuContent.Add(scroll.Content); 
        var index = menuContent.Count - 1;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
        var position = menuContent[index].transform;

        foreach (GovernmentModel government in GameManager.instance.data.governments)
        {
            Button(position, () => 
            { return "Name: " + government.name + " | " + Units.ReadItem(government.MoneyTotal) +
                "c\nBranches: " + government.entities.Count + 
                "\nCompanies: " + Units.ReadItem(government.licensedCompanies.ToList().FindAll(x => x == x.Owner).Count);
            }, () => GameManager.instance.OpenInfoPanel(government));
        }
    }

    public void BuildBlueprint()
    {
        ItemBlueprint blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(model.id);
        title.text = blueprint.coloredName;

        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = "Overview";
        //name.GetComponent<Image>().color = gov.spriteColor;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        int index = menuContent.Count - 1;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
        var position = menuContent[index].transform;

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Creator: " + blueprint.createdBy.Model.name;
        button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(blueprint.createdBy.Model));

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Owner: " + blueprint.owner.Model.name;
        button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(blueprint.owner.Model));

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Research: " + blueprint.research;
        button.onClick.AddListener(() => GameManager.instance.OpenInfoPanel(blueprint.research, TargetType.Research));

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Work Amount: " + Units.ReadItem(blueprint.workAmount);
        button.interactable = false;

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Mass: " + Units.ReadItem(blueprint.mass);
        button.interactable = false;

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Estimated Value: " + Units.ReadItem(blueprint.EstimatedValue);
        button.interactable = false;

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Size: " + Units.ReadItem(blueprint.size) + "m";
        button.interactable = false;

        button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = "Inventory";
        //name.GetComponent<Image>().color = gov.spriteColor;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        index = menuContent.Count - 1;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
        position = menuContent[index].transform;

        Button(position, () =>
        {
            return "Created: " + Units.ReadItem(blueprint.unitsCreated);
        });

        Button(position, () =>
        {
            return "Sold: " + Units.ReadItem(blueprint.unitsSold);
        });

        Button(position, () =>
        {
            return "Supply: " + Units.ReadItem(blueprint.Supply);
        });

        Button(position, () =>
        {
            return "Demand: " + Units.ReadItem(blueprint.Demand);
        });

        if (blueprint.productionFactoryIds.Count > 0 || blueprint.consumerFactoryIds.Count > 0)
        {
            button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Production/Consumption";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            position = menuContent[index].transform;

            
            if (blueprint.productionFactoryIds.Count > 0)
            {
                Button(position, () =>
                {
                    return "Production: " + Units.ReadItem(blueprint.productionFactoryIds.Count);
                });

                ButtonList<string, double>(blueprint.Id, blueprint.productionFactoryIds, position, (print) =>
                {
                    ProductionStructure x = GameManager.instance.locations[print.Key] as ProductionStructure;
                    string output = "Name: " + x.GetName() +
                                    "| : " + Units.ReadItem(print.Value);
                    return output;
                }, (print) => () => GameManager.instance.OpenInfoPanel(print.Key, TargetType.Structure));
            }

            if (blueprint.consumerFactoryIds.Count > 0)
            {
                Button(position, () =>
                {
                    return "Consumption: " + Units.ReadItem(blueprint.consumerFactoryIds.Count);
                });

                ButtonList<string, double>(blueprint.Id, blueprint.consumerFactoryIds, position, (print) =>
                {
                    SupplyStructure x = GameManager.instance.locations[print.Key] as SupplyStructure;
                    string output = "Name: " + x.GetName() +
                                    "| : " + Units.ReadItem(print.Value);
                    return output;
                }, (print) => () => GameManager.instance.OpenInfoPanel(print.Key, TargetType.Structure));
            }
        }

        if (blueprint.ConstructionBlueprints.Count > 0)
        {
            button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Construction Blueprints";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            position = menuContent[index].transform;

            ButtonList<string,int>(blueprint.Id, blueprint.ConstructionBlueprints, position, (print) =>
            {
                ItemBlueprint x = GameManager.instance.data.blueprintsModel.Model.GetItem(print.Key);
                string output = "Name: " + x.Name +
                                "| : " + print.Value;
                return output;
            }, (print) => () => GameManager.instance.OpenInfoPanel(print.Key, TargetType.Blueprint));
        }

        if ( blueprint.supportedBlueprints != null && blueprint.supportedBlueprints.Count > 0)
        {
            button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Supported Blueprints";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            position = menuContent[index].transform;

            ButtonList<string>(blueprint.Id, blueprint.supportedBlueprints, position, (x) =>
            {
                ItemBlueprint print = GameManager.instance.data.blueprintsModel.Model.GetItem(x);
                string output = "Name: " + print.Name;
                return output;
            }, (x) => () => GameManager.instance.OpenInfoPanel(x, TargetType.Blueprint));
        }

        if (blueprint.blueprintsThatSupportIt.Count > 0 || blueprint.blueprintsThatUseIt.Count > 0)
        {
            button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Connections";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            position = menuContent[index].transform;

            Button(position, () =>
            {
                return "Blueprints that use it (" + Units.ReadItem(blueprint.blueprintsThatUseIt.Count) + "):";
            });
            ButtonList<string>(blueprint.Id, blueprint.blueprintsThatUseIt, position, (x) =>
            {
                ItemBlueprint print = GameManager.instance.data.blueprintsModel.Model.GetItem(x);
                string output = "Name: " + print.Name;
                return output;
            }, (x) => () => GameManager.instance.OpenInfoPanel(x, TargetType.Blueprint));

            Button(position, () =>
            {
                return "Blueprints that support it (" + Units.ReadItem(blueprint.blueprintsThatSupportIt.Count) + "):";
            });

            ButtonList<string>(blueprint.Id, blueprint.blueprintsThatSupportIt, position, (x) =>
            {
                ItemBlueprint print = GameManager.instance.data.blueprintsModel.Model.GetItem(x);
                string output = "Name: " + print.Name;
                return output;
            }, (x) => () => GameManager.instance.OpenInfoPanel(x, TargetType.Blueprint));
        }
        SelectPanel(0);
    }

    public void BuildResearch()
    {
        Research research = GameManager.instance.research.allResearch[model.id];
        title.text = research.name;

        Button button = Instantiate(uiButtonInstance, titleContent.transform);
        button.GetComponentInChildren<Text>().text = "Overview";
        //name.GetComponent<Image>().color = gov.spriteColor;
        menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
        int index = menuContent.Count - 1;
        button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
        var position = menuContent[index].transform;

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Name: " + research.name;
        button.interactable = false;

        button = Instantiate(uiButtonInstance, position);
        button.GetComponentInChildren<Text>().text = "Description: " + research.description;
        button.interactable = false;

        if (research.requiredResearch != null && research.requiredResearch.Count > 0)
        {
            button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Required Research";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            position = menuContent[index].transform;

            ButtonList<string,ResearchPart>(research.name, research.requiredResearch, position, (x) =>
            {
                ItemBlueprint blueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(x.Key);
                string output = "Name: " + x.Key;
                return output;
            }, (x) => () => GameManager.instance.OpenInfoPanel(x.Key, TargetType.Research));
        }

        if (research.supportedResearch != null && research.supportedResearch.Count > 0)
        {
            button = Instantiate(uiButtonInstance, titleContent.transform);
            button.GetComponentInChildren<Text>().text = "Supported Research";
            //name.GetComponent<Image>().color = gov.spriteColor;
            menuContent.Add(Instantiate(infoContentPanel, contentPosition.transform));
            index = menuContent.Count - 1;
            button.GetComponent<ButtonInfo>().SetPanelToggle(index, this);
            position = menuContent[index].transform;

            ButtonList<string>(research.name, research.supportedResearch, position, (x) =>
            {
                string output = "Name : " + x;
                return output;
            }, (x) => () => GameManager.instance.OpenInfoPanel(x, TargetType.Research));
        }
        SelectPanel(0);
    }

    public void ButtonList<TValue, TKey>(string name, Dictionary<TValue, TKey> list, Transform position, StringDel<KeyValuePair<TValue, TKey>> listStringDel, ListUnityAction<KeyValuePair<TValue, TKey>> listCall = null, ButtonUpdate<KeyValuePair<TValue, TKey>> updateDel = null)
    {
        var scroll = Instantiate(scrollViewPrefab, position);
        position = scroll.Content.transform;

        scroll.SetUpdateDel(() =>
       {
           foreach (KeyValuePair<TValue, TKey> item in list)
           {
               if (!scroll.buttons.ContainsKey(name + item.GetHashCode().ToString()))
               {
                   string id = name + item.GetHashCode().ToString();
                   var button = Button(name + item.GetHashCode().ToString(), position, () => listStringDel(item), listCall(item));
                   scroll.buttons.Add(id, button);
                   scroll.updateTicksTracker.Add(id, scroll.updateTick);
               }
               else
               {
                   scroll.updateTicksTracker[name + item.GetHashCode().ToString()] = scroll.updateTick;
                   if (updateDel != null)
                   {
                       Button button = scroll.buttons[name + item.GetHashCode()];
                       Image image = button.GetComponent<Image>();
                       updateDel(item, button);
                   }

               }
           }
       });
    }

    public void ButtonList<T>( string name, List<T> list, Transform position, StringDel<T> listStringDel, ListUnityAction<T> listCall = null, ButtonUpdate<T> updateDel = null)
    {
        var scroll = Instantiate(scrollViewPrefab, position);
        position = scroll.Content.transform;

        scroll.SetUpdateDel( () =>
        {
            foreach (T item in list)
            {
                if (!scroll.buttons.ContainsKey(name + item.GetHashCode().ToString()))
                {
                    string id = name + item.GetHashCode().ToString();
                    var button = Button(id, position, () => listStringDel(item), listCall(item));
                    scroll.buttons.Add(id, button);
                    scroll.updateTicksTracker.Add(id, scroll.updateTick);
                }
                else
                {
                    scroll.updateTicksTracker[name + item.GetHashCode().ToString()] = scroll.updateTick;
                    if (updateDel != null)
                    {
                        Button button = scroll.buttons[name + item.GetHashCode()];
                        Image image = button.GetComponent<Image>();
                        updateDel(item, button);
                    }
                        
                }
            }
        });
    }

    public delegate void ButtonUpdate<T>(T item, Button button);

    public delegate string StringDel<T>(T item);

    public delegate UnityEngine.Events.UnityAction ListUnityAction<T>(T item);

    public Button Button(Transform position, StringDel stringDel, UnityEngine.Events.UnityAction call = null)
    {
        Button button = Instantiate(uiButtonInstance, position);
        Text text = button.GetComponentInChildren<Text>();

        button.GetComponent<ButtonInfo>().SetUpdateDel(() =>
        {
            text.text = stringDel();
        });

        if (call != null)
        {
            button.onClick.AddListener(call);
        }
        else
        {
            button.interactable = false;
        }

        return button;
    }

    public Button Button( string id, Transform position, StringDel stringDel, UnityEngine.Events.UnityAction call = null)
    {
        Button button = Button(position, stringDel, call);
        button.name = id;
        return button;
    }


    public void Close()
    {
        model.Delete();
    }
    // Update is called once per frame
    void Update () {

        var scaler = GetComponent<CanvasScaler>();
        if (scaler.enabled)
        {
            scaler.enabled = false;
        }
        if (fix)
        {
            scaler.enabled = true;
            fix = false;
        }
        
        TextUpdates();
    }

    

    public delegate void UpdateDel();
    public delegate string StringDel();

    public void SelectPanel(int index)
    {
        for (int i = 0; i < menuContent.Count; i++)
        {
            if (i == index)
            {
                menuContent[i].SetActive(true);
            }
            else
            {
                menuContent[i].SetActive(false);
            }
        }
        fix = true;
    }
}
