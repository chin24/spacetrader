﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalView : MonoBehaviour {

    public PlanetCreator planetObj;
    public StationGeneration station_base_prefab;
    public TooltipShip shipObj;
    public MapCamera moonViewCamera;
    public MapCamera normalCamera;
    public MapCamera solarCamera;
    public MapCamera planetCamera;
    public MapCamera galaxyCamera;

    internal static NormalView instance;

    internal GameManager game;
    internal ViewManager galaxy;
    

    internal PlanetCreator mainPlanet;
    internal GameObject station;
    //internal List<GameObject> moons = new List<GameObject>();
    internal bool control = false;

    private SolarBody solar;
    private Structure structure;

    private ParticleSystem particles;
    private ParticleSystem.Particle[] points;
    private int pointsMax;

    //Track ships
    internal List<TooltipShip> ships = new List<TooltipShip>();

    //private float solarSpriteScale = .02f;

    // Use this for initialization
    void Start () {
        if (instance == null)
        {
            instance = this;
            game = GameManager.instance;
            galaxy = ViewManager.instance;
            particles = GetComponent<ParticleSystem>();
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (control)
        {
            planetCamera.SetCameraView((Vector3)((structure.GetReferencePosition() * Position.ToMm) + ((Vector3d)normalCamera.mainCamera.transform.position) * Position.ToMm / Position.ToMeters), normalCamera.mainCamera.transform.rotation, normalCamera.mainCamera.transform.localScale, normalCamera.mainCamera.fieldOfView);
            moonViewCamera.SetCameraView((Vector3)((structure.GetReferencePosition() * Position.ToHm) + ((Vector3d)normalCamera.mainCamera.transform.position) * Position.ToHm / Position.ToMeters), normalCamera.mainCamera.transform.rotation, normalCamera.mainCamera.transform.localScale, normalCamera.mainCamera.fieldOfView);

            //for (int i = 1; i < pointsMax; i++)
            //{
            //    points[i].position = solar.satelites[i - 1].GamePosition(game.data.date.time);
            //    moons[i - 1].transform.position = solar.satelites[i - 1].lastKnownPosition;
            //}
            //particles.SetParticles(points, points.Length);

            var createdShips = new List<TooltipShip>(ships);

            if ((structure as SupplyStructure) != null && UIManager.instance.currentView == ViewType.Structure)
            {
                SupplyStructure supplyStructure = structure as SupplyStructure;
                //Create Ships
                foreach (Ship ship in supplyStructure.GetAllCloseShips())
                {

                    if (ships.Exists(x => x.ship == ship))
                    {
                        TooltipShip shipTooltip = ships.Find(x => x.ship == ship);
                        shipTooltip.transform.position = (Vector3)(ship.GetReferencePositionToPositionEntity(structure) * Position.ToMeters);
                        createdShips.Remove(shipTooltip);

                    }
                    else
                    {
                        //Create ship
                        TooltipShip shipTooltip = Instantiate(shipObj, this.transform);
                        shipTooltip.ship = ship;
                        shipTooltip.GetComponentInChildren<Transform>().eulerAngles = new Vector3(0, -180, 0);
                        shipTooltip.GetComponentInChildren<Transform>().localScale = Vector3.one * .5f;
                        ships.Add(shipTooltip);
                        shipTooltip.transform.position = (Vector3)(ship.GetReferencePositionToPositionEntity(structure) * Position.ToMeters);

                    }

                }
            }
            

            for (int i = 0; i < createdShips.Count; i++)
            {
                ships.Remove(createdShips[i]);
                Destroy(createdShips[i].gameObject);
            }
        }
    }

    public void CreateNormalView(Structure _structure)
    {
        structure = _structure;
        solar = structure.GetReferenceBody();

        control = true;
        normalCamera.SetCameraControlTrue();
        planetCamera.SetCameraView((Vector3)((structure.GetReferencePosition() * Position.ToMm)  + ((Vector3d)normalCamera.mainCamera.transform.position) * Position.ToMm / Position.ToMeters), normalCamera.mainCamera.transform.rotation, normalCamera.mainCamera.transform.localScale, normalCamera.mainCamera.fieldOfView);
        solarCamera.SetCameraView((Vector3)((structure.GetReferencePosition() * Position.ToHm) + ((Vector3d)normalCamera.mainCamera.transform.position) * Position.ToHm / Position.ToMeters), normalCamera.mainCamera.transform.rotation, normalCamera.mainCamera.transform.localScale, normalCamera.mainCamera.fieldOfView);
        //game.nameOfSystem.text = solar.name;
        transform.localScale = Vector3.one;
        mainPlanet = Instantiate(planetObj, transform);
        mainPlanet.name = solar.GetName();
        mainPlanet.transform.localPosition = Vector3.zero;// (Vector3)(-structure.referencePosition * Position.ToHm);
        mainPlanet.SetPlanetSize((float)((solar.GetBodyRadius()) * Position.ToHm)) ;//* Mathd.Pow(game.data.mainCameraOrtho[1], .9f));
        //mainPlanet.planetGetComponent<Renderer>().material.color = solar.color;
        mainPlanet.gameObject.layer = 11;
        mainPlanet.planet.layer = 11;

        //Create Station
        station = Instantiate(station_base_prefab.gameObject, transform);
        station.name = structure.GetName();
        station.transform.localPosition = Vector3.zero;
        station.gameObject.layer = 12;
        station.GetComponent<StationGeneration>().structure = structure as SupplyStructure;
        station.transform.Rotate(-90, 0, 0);
        //pointsMax = solar.satelites.Count + 1;
        //points = new ParticleSystem.Particle[pointsMax];

        //points[0].position = Vector3.zero;
        //points[0].startColor = solar.color;
        //points[0].startSize = 1;

        var info = mainPlanet.planet.GetComponent<PlanetInfo>();
        info.solar = solar;

        
    }


    public void DestroySystem(ViewType newView)
    {
        //if (model.nameText != "" && model.nameText != null)
        //{
        //    nameButton.enabled = false;
        //}
        //StopAllCoroutines();
        //game.nameOfSystem.text = game.data.galaxyName;

        //transform.localScale = Vector3.one * (float)Mathd.Pow((model.solar.bodyRadius), .01f);
        //model.localScale = (float)Mathd.Pow((model.solar.bodyRadius), .01f);
        if (mainPlanet == null) return;
        Destroy(mainPlanet.gameObject);
        Destroy(station);
        //for (int i = 0; i < moons.Count; i++)
        //{
        //    Destroy(moons[i]);

        //}
        //particles.SetParticles(points, 0);
        //moons = new List<GameObject>();
        normalCamera.cameraControl = false;
        control = false;
        //particles.SetParticles(points, 0);
    }

    public void SelectStructure(Structure structure)
    {
        //points[0].startSize = (float)(solar.bodyRadius / 500);
        //for (int i = 1; i < pointsMax; i++)
        //{

        //    LineRenderer line = moons[i - 1].GetComponent<LineRenderer>();
        //    line.widthMultiplier = 0;
        //    points[i].startSize = (float)(Mathd.Pow(solar.satelites[i - 1].bodyRadius, .5));
        //}
        //points[structure.ReferenceBody.satelites.FindIndex( x => x.name == structure.name) + 1].startSize = 0;
        //particles.SetParticles(points, points.Length);
    }

    public void SelectNormalView()
    {

        //points[0].startSize = 1;
        //for (int i = 1; i < pointsMax; i++)
        //{
        //    points[i].startSize = 10;
        //    LineRenderer line = moons[i - 1].GetComponent<LineRenderer>();
        //    line.widthMultiplier = mainPlanet.transform.localScale.x * .1f;
        //}
        //particles.SetParticles(points, points.Length);
        normalCamera.SetCameraControlTrue();
    }
}
