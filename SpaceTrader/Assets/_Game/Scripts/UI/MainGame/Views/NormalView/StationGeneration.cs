﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationGeneration : MonoBehaviour {

    public bool setup = true;
    public SupplyStructure structure;
    public GameObject station_shipyard_prefab;
    public GameObject station_factory_prefab;
    public GameObject stationResearchPrefab;
    public GameObject[] connectionPoints;
    public GameObject[] docks;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (setup && structure != null)
        {
            setup = false;
            //Setup and generate needed stations

            //Set Dock positions and generate landed ships

            if (structure.docks != null)
            {
                if (structure.docks.Count == docks.Length)
                {
                    for(int i = 0; i < docks.Length; i++)
                    {
                        structure.docks[i].referencePosition =  (Vector3d)(transform.position - docks[i].transform.position) / Position.ToMeters;
                        if (!structure.docks[i].IsAvailable)
                        {
                            //Set shipDock Location
                            Ship ship = structure.docks[i].GetShip();
                            ship.dockPosition = structure.docks[i].referencePosition;
                            if (ship.Docked) //Reposition ship
                            {
                                ship.SetReferencePosition(ship.dockPosition);
                            }
                        }
                    }
                }
            }

            //Set connected structures
            if (structure.childStructureIds != null)
            {
                for (int i = 0; i < structure.childStructureIds.Count; i++)
                {
                    string structureId = structure.childStructureIds[i];
                    SupplyStructure childStructure = GameManager.instance.locations[structureId] as SupplyStructure;

                    //Create Station
                    GameObject station = null;
                    if(childStructure.GetType() == typeof(Research))
                    {
                        station = Instantiate(stationResearchPrefab, connectionPoints[i].transform);
                    }
                    else if (childStructure.GetType() == typeof(Factory))
                    {
                        if (childStructure.GetStructureResearch().name == "Station Factory")
                            station = Instantiate(station_factory_prefab, connectionPoints[i].transform);
                        else if(childStructure.GetStructureResearch().name == "Station Shipyard")
                            station = Instantiate(station_shipyard_prefab, connectionPoints[i].transform);
                    }
                    if (station == null)
                        throw new System.Exception("station prefab match not found");
                    station.name = structure.GetName();
                    station.transform.localPosition = Vector3.zero;
                    station.transform.localRotation = Quaternion.identity;
                    station.gameObject.layer = 12;
                    station.GetComponent<StationGeneration>().structure = childStructure;
                    //Replace connection tag
                    connectionPoints[i].tag = "connection_point_taken";

                    //update station localPosition
                    childStructure.SetReferencePosition((Vector3d) (transform.position - station.transform.position) / Position.ToMeters);
                }
            }
            
        

        }
		
	}
}
