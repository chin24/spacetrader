﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolarView : IconManager {

    public PlanetInfo sunObj;
    public PlanetCreator planetObj;
    public MapCamera solarCamera;
    public MapCamera galaxyCamera;

    internal static SolarView instance;

    internal GameManager game;
    internal ViewManager galaxy;

    

    internal GameObject sun;
    internal List<PlanetCreator> planets = new List<PlanetCreator>();
    internal List<TooltipShip> ships = new List<TooltipShip>();
    internal bool control = false;

    private SolarBody solar;

    //private float solarSpriteScale = .02f;

    private ParticleSystem particles;
    private ParticleSystem.Particle[] points;
    private int pointsMax;

    //Manage Icons
    public Canvas iconsCanvas;
    public Image hasStation;

    public SolarBody Solar { get => solar; set => solar = value; }

    // Use this for initialization
    void Start () {
        if (instance == null)
        {
            instance = this;
            game = GameManager.instance;
            galaxy = ViewManager.instance;
            particles = GetComponent<ParticleSystem>();
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
	
	// Update is called once per frame
	void LateUpdate () {
		if (control)
        {
            galaxyCamera.SetCameraView((Vector3)(solar.GetReferencePosition() + (((Vector3d) solarCamera.mainCamera.transform.position) * Position.ToLightyears / Position.ToGm)), solarCamera.mainCamera.transform.rotation, solarCamera.mainCamera.transform.localScale, solarCamera.mainCamera.fieldOfView);

            for (int i = 1; i < pointsMax; i++)
            {
                points[i].position = solar.Satelites[i-1].GetGamePosition(game.data.date.time);
                planets[i-1].transform.position = points[i].position;
            }
            particles.SetParticles(points, points.Length);

            //Manage Ships
            if (UIManager.instance.currentView == ViewType.SolarSystem)
            {
                foreach (string structureId in solar.PositionEntityIds)
                {
                    var location = GameManager.instance.locations[structureId];

                    if (location.GetType() == typeof(Ship))
                    {
                        Ship ship = location as Ship;


                        //Find ship GameObject


                        if (ships.Exists(x => x.ship == ship))
                        {
                            TooltipShip shipTooltip = ships.Find(x => x.ship == ship);
                            shipTooltip.transform.position = (Vector3)(ship.GetReferencePosition() * Position.ToGm);
                        }
                        else
                        {
                            //Create ship
                            TooltipShip shipTooltip = ShipObjectCreationManager.instance.CreateShip(ship, transform);
                            shipTooltip.transform.localScale = Vector3.one * 25;
                            shipTooltip.ship = ship;
                            ships.Add(shipTooltip);
                            shipTooltip.transform.position = (Vector3)(ship.GetReferencePosition() * Position.ToGm);

                        }

                        for (int i = ships.Count - 1; i >= 0; i--)
                        {
                            TooltipShip shipTooltip = ships[i];
                            // Delete if moved to another SOI
                            if (shipTooltip.ship.GetReferenceId() != solar.GetId())
                            {
                                ships.Remove(shipTooltip);
                                Destroy(shipTooltip.gameObject);
                                break;
                            }
                        }
                    }
                }
            }
            
        }
    }

    public void CreateSystem(SolarBody _solar)
    {
        DestroySystem();
        solar = _solar;
        control = true;
        solarCamera.SetCameraControlTrue();
        galaxyCamera.SetCameraView((Vector3)(solar.GetReferencePosition() + (((Vector3d)solarCamera.mainCamera.transform.position) * Position.ToLightyears / Position.ToGm)), solarCamera.mainCamera.transform.rotation, solarCamera.mainCamera.transform.localScale, solarCamera.mainCamera.fieldOfView);
        //game.nameOfSystem.text = solar.name;

        transform.localScale = Vector3.one;
        sun = Instantiate(sunObj.gameObject, transform);
        sun.name = solar.GetName();
        sun.transform.localPosition = Vector3.zero;
        sun.transform.localScale = Vector3.one * 50 ;//(float)(Mathd.Pow((solar.bodyRadius), solarSpriteScale)) ;//* Mathd.Pow(game.data.mainCameraOrtho[1], .9f));
        sun.GetComponent<Renderer>().material.color = solar.GetSolarColor();
        sun.GetComponent<Renderer>().enabled = false;

        pointsMax = solar.Satelites.Count + 1;
        points = new ParticleSystem.Particle[pointsMax];

        points[0].position = Vector3.zero;
        points[0].startColor = solar.GetSolarColor();
        points[0].startSize = 2000;

        var info = sun.GetComponent<PlanetInfo>();
        info.solar = solar;

        //Destroy Icons

        planets = new List<PlanetCreator>();
        for (int i = 0; i < solar.Satelites.Count; i++)
        {
            SolarBody body = solar.Satelites[i];
            Vector3 position = body.GetGamePosition(game.data.date.time);
            planets.Add(Instantiate(planetObj, transform));
            planets[i].name = body.GetName();
            planets[i].transform.position = position;
            planets[i].planet.GetComponent<Renderer>().material.color = new Color(body.GetSolarColor().r, body.GetSolarColor().g,body.GetSolarColor().b, .1f);
            //planets[i].planet.transform.localScale = Vector3.one * (float) (body.SOI() * Position.ToGm);
            //planets[i].planet.layer = 9;
            planets[i].planet.GetComponent<Renderer>().enabled = false;
            //planets[i].GetComponent<SpriteRenderer>().sortingOrder = 4;
            planets[i].SetPlanetSize( sun.transform.localScale.x * .5f);
              
            info = planets[i].planet.GetComponent<PlanetInfo>();
            info.solar = body;

            //Create station indicator
            if (body.HasStations)
            {
                CreateIcon(i, planets[i].gameObject);
            }

            points[i + 1].position = position;
            points[i+1].startColor = solar.Satelites[i].GetSolarColor();
            points[i+1].startSize = 1000;

            LineRenderer line = planets[i].planet.GetComponent<LineRenderer>();

            //Creates the line rendering for the orbit of planets

            Vector3[] positions = new Vector3[361];
            double time = game.data.date.time;
            body.SetOrbit(time, solar.GetMass());


            for (var b = 0; b < 360; b++)
            {
                positions[b] = body.GetApproximatePositions()[b];
            }
            line.positionCount = 360;
            line.SetPositions(positions);


            if (body.GetSolarSubType() == SolarSubTypes.GasGiant)
            {
                Color col = Color.blue;
                col.a = .1f;
                line.startColor = col;
                line.endColor = col;
            }
            else if (body.GetSolarType() == SolarTypes.DwarfPlanet)
            {
                Color col = Color.yellow;
                col.a = .1f;
                line.startColor = col;
                line.endColor = col;
            }
            else if (body.GetSolarType() == SolarTypes.Comet)
            {
                Color col = Color.white;
                col.a = .1f;
                line.startColor = col;
                line.endColor = col;
            }
            else
            {
                Color col = Color.green;
                col.a = .1f;
                line.startColor = col;
                line.endColor = col;
            }

            line.widthMultiplier = sun.transform.localScale.x * .5f;
        }
        particles.SetParticles(points, points.Length);
        //StartCoroutine("UpdateSolarObjects");
    }

    private void CreateIcon(int i, GameObject obj)
    {
        //Create station indicator
        if (solar.Satelites[i].HasStations)
        {
            var icon = Instantiate(hasStation, iconsCanvas.transform);
            icon.GetComponent<IconObjectTrack>().SetTarget(obj, solarCamera.mainCamera);
            icons.Add(icon.gameObject);
        }
    }

    public void DestroySystem()
    {
        //if (model.nameText != "" && model.nameText != null)
        //{
        //    nameButton.enabled = false;
        //}
        //StopAllCoroutines();
        //game.nameOfSystem.text = game.data.galaxyName;

        //transform.localScale = Vector3.one * (float)Mathd.Pow((model.solar.bodyRadius), .01f);
        //model.localScale = (float)Mathd.Pow((model.solar.bodyRadius), .01f);
        if (sun != null)
            Destroy(sun);
        else return;

        for (int i = 0; i < planets.Count; i++)
        {
            Destroy(planets[i].gameObject);

        }
        DestroyShips();
        planets = new List<PlanetCreator>();
        control = false;
        solarCamera.cameraControl = false;
        particles.SetParticles(points, 0);
        
    }

    public void SelectPlanet(SolarBody planet)
    {
        //points[0].startSize = (float)(solar.bodyRadius / 500);
        for (int i = 1; i < pointsMax; i++)
        {
            
            LineRenderer line = planets[i - 1].planet.GetComponent<LineRenderer>();
            line.widthMultiplier = 0;
            points[i].startSize = (float)(Mathd.Pow(solar.Satelites[i - 1].GetBodyRadius(), .5));
        }
        points[planet.GetReferenceBody().Satelites.FindIndex(x => x.GetName() == planet.GetName()) + 1].startSize = 0;
        particles.SetParticles(points, points.Length);
        DestroyShips();
    }

    

    public void DestroyShips()
    {
        //Destroy ships
        for (int i = ships.Count - 1; i >= 0; i--)
        {
            Destroy(ships[i].gameObject);

        }

        ships = new List<TooltipShip>();
    }
}
