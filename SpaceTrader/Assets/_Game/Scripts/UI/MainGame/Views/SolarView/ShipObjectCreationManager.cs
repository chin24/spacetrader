﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipObjectCreationManager : MonoBehaviour
{
    public TooltipShip solarViewDrillerShip;
    public TooltipShip solarViewCargoShip;
    internal static ShipObjectCreationManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public TooltipShip CreateShip( Ship ship, Transform transform)
    {
        if (ship.GetStructureResearch().tags.Contains(ResearchTags.ProducesRawProducts))
        {
            return Instantiate(solarViewDrillerShip, transform);
        } else
        {
            return Instantiate(solarViewCargoShip, transform);
        }
    }
}
