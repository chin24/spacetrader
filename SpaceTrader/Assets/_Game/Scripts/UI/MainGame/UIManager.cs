﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeControl;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class UIManager: MonoBehaviour
    {
    public static UIManager instance;
    public ViewType currentView { get; private set; }
    public delegate void ClickAction(ViewType newView);
    public delegate void ClickSolar(SolarBody solar);
    public delegate void ClickGalaxy();
    public delegate void ClickStructure(Structure structure);
    public static event ClickGalaxy Galaxy;
    public static event ClickAction ViewChange;
    public static event ClickSolar SolarSystem;
    public static event ClickSolar Planet;
    public static event ClickStructure Structure;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            currentView = ViewType.Galaxy;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    //public void OnGUI()
    //{
    //    GUI.Box(new Rect(20, 20, 50, 50), Units.ReadDistance(GameManager.instance.data.cameraGalaxyOrtho * GameDataModel.galaxyDistanceMultiplication));
    //}

    public void SelectGalaxy()
    {
        SelectViewChange(ViewType.Galaxy);  
        SolarView.instance.DestroySystem();
        GalaxyView.instance.SelectGalaxyView();
        if (ViewChange != null)
        {
            ViewChange(ViewType.Galaxy);
        }
        if (Galaxy != null)
        {
            Galaxy();
        }
        currentView = ViewType.Galaxy;
    }
    public void SelectSolarSystem(SolarBody solar)
    {
        SelectViewChange(ViewType.SolarSystem);
        GalaxyView.instance.SelectSolarSystem(solar);
        SolarView.instance.CreateSystem(solar);
        if (ViewChange != null)
        {
            ViewChange(ViewType.SolarSystem);
        }
        if (SolarSystem != null)
        {
            SolarSystem(solar);
        }
        currentView = ViewType.SolarSystem;
    }

    public void SelectSolarView()
    {
        if (SolarView.instance.Solar != null)
        {
            SelectSolarSystem(SolarView.instance.Solar);
        }
    }

    public void SelectPlanet(SolarBody solar)
    {
        SelectViewChange(ViewType.Planet);  
        SolarView.instance.SelectPlanet(solar);
        PlanetView.instance.CreatePlanetSystem(solar);
        if (ViewChange != null)
        {
            ViewChange(ViewType.Planet);
        }
        if (Planet != null)
        {
            Planet(solar);
        }
        currentView = ViewType.Planet;
    }

    public void SelectStructure(Structure structure)
    {
        SelectViewChange(ViewType.Structure);
        PlanetView.instance.SelectStructure(structure);
        NormalView.instance.CreateNormalView(structure);
        if (ViewChange != null)
        {
            ViewChange(ViewType.Structure);
        }
        if (Structure != null)
        {
            Structure(structure);
        }
        currentView = ViewType.Structure;
    }

    private void SelectViewChange(ViewType newView)
    {
        GalaxyView.instance.DisableClicks(newView);
        GalaxyView.instance.ClearIcons(newView);
        SolarView.instance.ClearIcons(newView);
        PlanetView.instance.DestroySystem(newView);
        NormalView.instance.DestroySystem(newView);
    }
}



public enum ViewType
{
    Galaxy,
    SolarSystem,
    Planet,
    Structure
}
