﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipShip : MonoBehaviour {

    public Ship ship;
    LineRenderer line;

    public void Awake()
    {
        line = GetComponent<LineRenderer>();
    }

    public void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (ship.ShipTargetId != null)
            {
                ToolTip.instance.SetTooltip(ship.GetName(), String.Format("Destination: {0}", GameManager.instance.locations[ship.ShipTargetId].GetName()));
            }
            else
            {
                ToolTip.instance.SetTooltip(ship.GetName(), String.Format("Info: {0}", ship.Info));
            }
        }
            
    }
    public void OnMouseExit()
    {
        ToolTip.instance.CancelTooltip();
    }

    public void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
            GameManager.instance.OpenInfoPanel(ship);
    }
    public void Update()
    {
        if (ship != null)
        {
            
            if ((GameManager.instance.locations[ship.GetReferenceId()] as SolarBody)!= null && (GameManager.instance.locations[ship.GetReferenceId()] as SolarBody).GetSolarType() != SolarTypes.Star)
            {
                line.widthMultiplier = .02f;
                //transform.localScale = Vector3.one * .08f;
            }
            else
            {
                line.widthMultiplier = 5f;
                //transform.localScale = Vector3.one * 1f;
            }
            if (ship.ShipTargetId != null)
            {
                var target = GameManager.instance.locations[ship.ShipTargetId];

                if ((GameManager.instance.locations[ship.GetReferenceId()] as SolarBody) != null && (GameManager.instance.locations[ship.GetReferenceId()] as SolarBody).GetSolarType() != SolarTypes.Star)
                {
                    if (target.GetReferenceId() != ship.GetReferenceId())
                    {
                        Vector3 distance = (Vector3) (target.SystemPosition - ship.SystemPosition).normalized * 100 + transform.position;
                        transform.LookAt(distance, Vector3.back);
                        line.SetPosition(0, transform.position);
                        line.SetPosition(1, distance);
                        line.endColor = Color.yellow;
                    }
                    else
                    {
                        Vector3 targetPosition = (Vector3)(target.GetReferencePosition() * Position.ToMm);
                        transform.LookAt(targetPosition, Vector3.back);
                        line.endColor = Color.blue;
                    }
                }
                else
                {
                    
                    Vector3 targetPosition = (Vector3)(target.SystemPosition * Position.ToGm);
                    transform.LookAt(targetPosition, Vector3.back);
                    line.SetPosition(0, transform.position);
                    line.SetPosition(1, targetPosition);
                    line.endColor = Color.blue;
                }
            }
        }
    }
}
