﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetGridPanel : MonoBehaviour {

    public GameObject waterButton;
    public GameObject iceButton;
    public GameObject volcanicButton;
    public GameObject grassButton;
    public GameObject rockyButton;
    public GameObject desertButton;

	// Use this for initialization
	void Start () {
	}

    public void SelectPlanet(SolarBody body)
    {
        if (body.GetPlanetTiles() == null)
            return;

        foreach (PlanetTile tile in body.GetPlanetTiles())
        {
            GameObject prefab;

            if (tile.GetPlanetTileType() == PlanetTileType.Desert)
            {
                prefab = desertButton;
            }
            else if (tile.GetPlanetTileType() == PlanetTileType.Grasslands)
            {
                prefab = grassButton;
            }
            else if (tile.GetPlanetTileType() == PlanetTileType.Ice)
            {
                prefab = iceButton;
            }
            else if (tile.GetPlanetTileType() == PlanetTileType.Ocean)
            {
                prefab = waterButton;
            }
            else if (tile.GetPlanetTileType() == PlanetTileType.Rocky)
            {
                prefab = rockyButton;
            }
            else if (tile.GetPlanetTileType() == PlanetTileType.Volcanic)
            {
                prefab = volcanicButton;
            }
            else
            {
                prefab = volcanicButton;
                throw new System.Exception("Unknown planet tile type");
            }

            for(int i = 0; i < tile.GetCount(); i++)
            {
                GameObject button = Instantiate(prefab, transform);
            }
            
        }
    }
}
