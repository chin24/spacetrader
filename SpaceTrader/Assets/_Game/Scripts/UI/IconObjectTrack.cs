﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconObjectTrack : MonoBehaviour {

    private float multMod = 1;
    public float scaleMod = .1f;
    internal GameObject target;
    internal Camera cameraTarget;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null)
        {
            var pos = cameraTarget.WorldToScreenPoint(target.transform.position);
            transform.position = new Vector3(pos.x, pos.y, 10);
            //Add scaling
            float distance = (cameraTarget.transform.position - target.transform.position).sqrMagnitude;
            transform.localScale = Vector3.one * multMod / Mathf.Pow(distance, scaleMod);        
        }
	}

    public void SetTarget(GameObject _target, Camera _camera)
    {
        target = _target;
        cameraTarget = _camera;
    }
}
