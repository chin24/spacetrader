﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInfo : MonoBehaviour {

    [HideInInspector]
    public int index;
    [HideInInspector]
    public InfoPanelController controller;
    internal InfoPanelController.UpdateDel updateDel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (updateDel != null)
        {
            updateDel();
        }
	}

    //void OnEnable()
    //{
    //    if (controller != null)
    //    {
    //        controller.TextUpdates += updateDel;
    //    }
    //}

    //void OnDisable()
    //{
    //    if (controller != null)
    //    {
    //        controller.TextUpdates -= updateDel;
    //    }
    //}

    public void SetUpdateDel(InfoPanelController.UpdateDel updateDel)
    {
        this.updateDel = updateDel;
        //controller.TextUpdates += updateDel;
    }

    public void SetPanelToggle(int index, InfoPanelController controller)
    {
        this.index = index;
        this.controller = controller;

        GetComponent<Button>().onClick.AddListener(() => controller.SelectPanel(index));
    }

    public void OnDestroy()
    {
        GetComponent<Button>().onClick.RemoveListener(() => controller.SelectPanel(index));
    }
}
