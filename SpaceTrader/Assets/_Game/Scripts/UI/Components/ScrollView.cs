﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollView : MonoBehaviour {

    public GameObject Content;
    internal InfoPanelController.UpdateDel updateDel;
    public Dictionary<string, Button> buttons = new Dictionary<string, Button>();

    /// <summary>
    /// Used to update items that need updating.
    /// </summary>
    public Dictionary<string, int> updateTicksTracker = new Dictionary<string, int>();
    public int updateTick = 0;

    private void Update()
    {
        if (updateDel != null)
        {
            updateTick++;
            updateDel();
            RemoveObsButtons();
        }
    }

    public void SetUpdateDel(InfoPanelController.UpdateDel updateDel)
    {
        
        this.updateDel = updateDel;
        //controller.TextUpdates += updateDel;
    }

    private void RemoveObsButtons()
    {
        foreach (KeyValuePair<string, int> tick in updateTicksTracker)
        {
            if (tick.Value != updateTick)
            {
                var button = buttons[tick.Key];

                //Delete button
                buttons.Remove(tick.Key);
                updateTicksTracker.Remove(tick.Key);
                Destroy(button.gameObject);
                break;
            }
        }
    }
}
