﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWorkers {

    int workers { get; set; }
    double workerPayRate { get; set; }
}
