﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelResearch : Research {

    public FuelResearch() { }

    public FuelResearch(string _name, string _description, int _totalResearchPoints, int _baseWorkAmount)
        : base(_name, _description, _totalResearchPoints, _baseWorkAmount, ResearchCategory.Intermediate)
    {

    }

    public override ItemBlueprint GenerateBlueprint(IdentityModel identityModel)
    {
        StringIntDictionary requiredBlueprints = new StringIntDictionary();
        int workAmount = baseWorkAmount;
        float armor = baseArmor;
        float size = this.size;
        float mass = baseMass;

        if (requiredResearch != null)
        {
            foreach (KeyValuePair<string, ResearchPart> research in requiredResearch)
            {
                //Find all available free blueprints
                var possibleBlueprints = identityModel.Owner.ownedBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == research.Key);
                ItemBlueprint blueprint = null;
                if (possibleBlueprints.Count == 0)
                {
                    possibleBlueprints = identityModel.Government.allBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == research.Key);
                }
                else
                {
                    //TODO: extend government and personal blueprints when no longer picking random blueprints
                }
                if (possibleBlueprints.Count != 0)
                {
                    //Randomly pick blueprint
                    //TODO: Make smarter blueprint finding algorithm or make another function
                    var pickedBlueprint = possibleBlueprints[Random.Range(0, possibleBlueprints.Count)];
                    blueprint = GameManager.instance.data.blueprintsModel.Model.blueprints.Find(x => x.Id == pickedBlueprint);

                }
                else
                {
                    //Create company to manage blueprints related to the research
                    Creature manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, identityModel.referenceId, GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);

                    new CompanyModel(identityModel.Owner.name + " " + research.Key + " Branch", research.Key, GameManager.instance.locations[identityModel.referenceId] as SolarBody, identityModel.Government, manager, identityModel.Owner);

                    //Create blueprint
                    blueprint = identityModel.Owner.GetOwnedBlueprints().Find(x => x.research == research.Key);
                }
                if (blueprint == null) throw new System.Exception("Could not find created structure blueprint for: " + research.Key);


                int amount = Random.Range(research.Value.minAmount, research.Value.maxAmount + 1);
                float massContribution = Random.Range(research.Value.minMassContributionPercent, research.Value.maxMassContributionPercent) * blueprint.mass;

                workAmount += research.Value.workCost;
                armor += blueprint.baseArmor * research.Value.armorContributionPercent * amount;
                mass += massContribution * amount;
                //Randomly choose count and Add
                requiredBlueprints.Add(blueprint.Id, amount);
            }
        }

        if (supportedResearch != null)
        {
            foreach (string research in supportedResearch)
            {
                //Find all available free blueprints
                var possibleBlueprints = identityModel.Owner.ownedBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == research);

                if (possibleBlueprints.Count == 0)
                {
                    possibleBlueprints = identityModel.Government.allBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == research);
                }
                else
                {
                    //TODO: extend government and personal blueprints when no longer picking random blueprints
                }
                if (possibleBlueprints.Count == 0)
                {
                    //Create company to manage blueprints related to the research
                    Creature manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, identityModel.referenceId, GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);

                    new CompanyModel(identityModel.Owner.name + " " + research + " Branch", research, GameManager.instance.locations[identityModel.referenceId] as SolarBody, identityModel.Government, manager, identityModel.Owner);

                    //Create blueprint
                    var blueprint = identityModel.Owner.GetOwnedBlueprints().Find(x => x.research == research);

                    if (blueprint == null) throw new System.Exception("Could not find created structure blueprint for: " + research);
                }
            }
        }
        workAmount = (int)(workAmount * Random.Range(.5f, 1.5f));
        if (workAmount == 0)
            workAmount = 1;
        return new FuelBlueprint(name, name, identityModel, workAmount, mass, Random.Range(.75f,1.25f), size, armor, requiredBlueprints); //identityModel.Owner.name + " " + 
    }
}
