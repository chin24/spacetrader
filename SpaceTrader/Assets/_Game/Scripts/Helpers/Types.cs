﻿using UnityEngine;
using System.Collections;

public enum IdentityType {
    Government,
    Company
}

public enum BlueprintType
{
    Ship,
    Factory,
    Driller,
    Storage,
    LivingQuarters
}

public enum ViewMode
{
    Galaxy,
    Planet,
    Moon,
    Satellite,
    ThirdPerson
}

public enum SolarTypes
{
    Star,
    Planet,
    DwarfPlanet,
    Moon,
    Comet,
    Asteroid,
    Structure
}

public enum SolarSubTypes
{
    SuperGiant,
    WhiteDwarf,
    MainSequence,
    Rocky,
    GasGiant,
    Desert,
    Ocean,
    EarthLike,
    Volcanic,
    Ice
}

public enum VisualDisplay
{
    Normal,
    Temperature
}

public enum PlanetTileType
{
    Rocky,
    Desert,
    Ocean,
    Grasslands,
    Volcanic,
    Ice
}


