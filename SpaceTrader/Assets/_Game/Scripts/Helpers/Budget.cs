using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeControl;

public class MoneyBudget: Money
{
    public double budgetAmount;
    public int budgetInterval = Dated.Month * 3;
    public double budgetProgress;
    /// <summary>
    /// Holds the budget name and then a double array including budget percent, budget left, budgeted amount
    /// </summary>
    public Dictionary<string, double[]> budget = new Dictionary<string, double[]>();

    public MoneyBudget() { }

    public static MoneyBudget StandardBuget { get { return new MoneyBudget(0, new Dictionary<int, double>() { { Dated.Year, 50 }, { Dated.Month, 12 } }); } }

    public MoneyBudget(double startTime, Dictionary<int, double> _updateInterval) :
        base(startTime, _updateInterval)
    { }

    public override void UpdateMoney()
    {
        base.UpdateMoney();
        if (budgetProgress + budgetInterval < GameManager.instance.data.date.time)
        {
            budgetProgress = GameManager.instance.data.date.time;
            AdjustBalance();
            BudgetMoney(moneyTotal);
        }
        

    }

    public void BudgetMoney(double amount)
    {
        budgetAmount = amount;
        foreach (KeyValuePair<string,double[]> part in budget)
        {
            part.Value[1] = part.Value[0] * amount;
            part.Value[2] = part.Value[0] * amount;
        }
    }

    public override void StartingBalance(double amount)
    {
        base.StartingBalance(amount);
        BudgetMoney(amount);
    }

    public override void SubtractMoney(string cat, double amount)
    {
        base.SubtractMoney(cat, amount);
        RemoveBudgetAmount(cat, amount);
    }

    private void RemoveBudgetAmount(string name, double amount)
    {
        if (!budget.ContainsKey(name))
        {
            budget.Add(name, new double[3] { .1, 0, 0 });
            BalanceBudget();
        }
            budget[name][1] -= amount;
    }

    private void AdjustBalance()
    {
        //Get total percent
        foreach (KeyValuePair<string, double[]> part in budget)
        {
            if (part.Value[1] <= 0)
                part.Value[0] += .1;
            else if (part.Value[1] / part.Value[2] >= .8)
                part.Value[0] -= .1;
        }

        BalanceBudget();
    }

    private void BalanceBudget()
    {
        double totalPercent = 0;

        //Get total percent
        foreach (KeyValuePair<string, double[]> part in budget)
        {
            totalPercent += part.Value[0];
        }

        //Set new percents
        foreach (KeyValuePair<string, double[]> part in budget)
        {
            part.Value[0] = part.Value[0] / totalPercent;
            double newMoneyAmount = budgetAmount * part.Value[0];
            double changeInMoneyAmount = newMoneyAmount - part.Value[2];
            part.Value[1] += changeInMoneyAmount;
            part.Value[2] = newMoneyAmount;
        }
    }

    public double CheckAmount(string name)
    {
        return budget[name][1];
    }
}