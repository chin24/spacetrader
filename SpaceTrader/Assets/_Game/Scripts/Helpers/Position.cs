﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Holds Position functions to be used by other classes
/// </summary>
public class Position {
    

    //----------------Needed variables to be set by the user----------------//

    public static readonly double ToMeters = 1000; //Normal View
    public static readonly double ToHm = .1; //Moon View/Close to Planet View
    public static readonly double ToMm = .001; //Planet System View
    public static readonly double ToGm = .000001; //Solar System View
    public static readonly double ToLightyears = 1.05697e-13; //Galaxy View

    //------------------------------------------------------------------//
    /// <summary>
    /// Build the systemConversion field
    /// </summary>
    public Position() {   }
    
    //-------------------------Position Funtions----------------------//
    /// <summary>
    /// Used by the orbit Function to make sure planetary orbits are in correct game scale
    /// </summary>
    public static double KmToLocalScale(int index)
    {
        if (index == 1)
            return ToGm;
        else if (index == 2)
            return ToMm;
        else
            throw new Exception("Index not configured: " + index);
    }
    

    //------------------------Helper Functions-----------------------------------//
}
