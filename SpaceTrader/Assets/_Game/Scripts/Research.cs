﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Research {

	public string name { get; set; }
    public string id { get; set; }
    public int totalResearchPoints { get; set; }
    public string description { get; set; }

    public int baseWorkAmount = 0;
    public float baseMass = 0;
    public float size = 0;
    public float baseArmor = 0;
    public ResearchCategory category;
    public List<ResearchTags> tags;

    /// <summary>
    /// Required research name and parts.
    /// </summary>
    public Dictionary<string, ResearchPart> requiredResearch;
    public Dictionary<string, ResearchPart> optionalResearch;
    /// <summary>
    /// Research that is needed in order for part to function correctly
    /// </summary>
    public List<string> supportedResearch;

    public Research() { }
    
    public Research(string _name, string _description, int _totalResearchPoints, int _baseWorkAmount, ResearchCategory category)
    {
        name = _name;
        baseWorkAmount = _baseWorkAmount;
        this.category = category;
        description = _description;
        totalResearchPoints = _totalResearchPoints;
    }

    public virtual ItemBlueprint GenerateBlueprint(IdentityModel identityModel)
    {
        StringIntDictionary requiredBlueprints = new StringIntDictionary();
        int workAmount = baseWorkAmount;
        float armor = baseArmor;
        float size = this.size;
        float mass = baseMass;
        if (requiredResearch != null)
        {
            foreach (KeyValuePair<string, ResearchPart> research in requiredResearch)
            {
                //Find all available free blueprints
                var possibleBlueprints = identityModel.Owner.ownedBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == research.Key);
                ItemBlueprint blueprint = null;
                if (possibleBlueprints.Count == 0)
                {
                    possibleBlueprints = identityModel.Government.allBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == research.Key);
                }
                else
                {
                    //TODO: extend government and personal blueprints when no longer picking random blueprints
                }
                if (possibleBlueprints.Count != 0)
                {
                    //Randomly pick blueprint
                    //TODO: Make smarter blueprint finding algorithm or make another function
                    var pickedBlueprint = possibleBlueprints[Random.Range(0, possibleBlueprints.Count)];
                    blueprint = GameManager.instance.data.blueprintsModel.Model.blueprints.Find(x => x.Id == pickedBlueprint);

                }
                else
                {
                    //Create company to manage blueprints related to the research
                    Creature manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, identityModel.referenceId, GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);

                    new CompanyModel(identityModel.Owner.name + " " + research.Key + " Branch", research.Key, GameManager.instance.locations[identityModel.referenceId] as SolarBody, identityModel.Government, manager, identityModel.Owner);

                    //Create blueprint
                    blueprint = identityModel.Owner.GetOwnedBlueprints().Find(x => x.research == research.Key);
                }
                if (blueprint == null) throw new System.Exception("Could not find created structure blueprint for: " + research.Key);


                int amount = Random.Range(research.Value.minAmount, research.Value.maxAmount + 1);
                float massContribution = Random.Range(research.Value.minMassContributionPercent, research.Value.maxMassContributionPercent) * blueprint.mass;

                workAmount += research.Value.workCost;
                armor += blueprint.baseArmor * research.Value.armorContributionPercent * amount;
                mass += massContribution * amount;
                //Randomly choose count and Add
                requiredBlueprints.Add(blueprint.Id, amount);
            }
        }

        if (supportedResearch != null)
        {
            foreach (string research in supportedResearch)
            {
                //Find all available free blueprints
                var possibleBlueprints = identityModel.Owner.ownedBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == research);

                if (possibleBlueprints.Count == 0)
                {
                    possibleBlueprints = identityModel.Government.allBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == research);
                }
                else
                {
                    //TODO: extend government and personal blueprints when no longer picking random blueprints
                }
                if (possibleBlueprints.Count == 0)
                {
                    //Create company to manage blueprints related to the research
                    Creature manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, identityModel.referenceId, GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);

                    new CompanyModel(identityModel.Owner.name + " " + research + " Branch", research, GameManager.instance.locations[identityModel.referenceId] as SolarBody, identityModel.Government, manager, identityModel.Owner);

                    //Create blueprint
                    var blueprint = identityModel.Owner.GetOwnedBlueprints().Find(x => x.research == research);

                    if (blueprint == null) throw new System.Exception("Could not find created structure blueprint for: " + research);
                }
            }
        }

        return new ItemBlueprint(identityModel.Owner.name + " " + name, name,identityModel, workAmount, mass, size, armor, requiredBlueprints);
    }
}

/// <summary>
/// Holds information for how a part can be added to create another part
/// </summary>
public class ResearchPart
{
    public int workCost;

    public int minAmount;
    public int maxAmount;

    public float armorContributionPercent;

    public float minSizeContributionPercent;
    public float maxSizeContributionPercent;
    public float minMassContributionPercent;
    public float maxMassContributionPercent;

    public ResearchPart(){}
	
}

public class ResearchDictionary
{
    public Dictionary<string, Research> allResearch = new Dictionary<string, Research>();
    public List<string> researchNames = new List<string>();
    public ResearchDictionary() { }

    public void Add( Research research)
    {
        allResearch.Add(research.name, research);
        researchNames.Add(research.name);
    }
}

public enum ResearchCategory
{
    Raw,
    Intermediate,
    Structure,
    Ship
}

public enum ResearchTags
{
    ProducesRawProducts,
    ProducesIntermediateProducts,
    ProducesStructures,
    NoAtmosphere,
    LowAtmosphere,
    Atmosphere,
    Station,
    BaseStation,
    Research,
    Distribution,
    ProducesShips,
    HoldsCargo,
    CombatShip
}