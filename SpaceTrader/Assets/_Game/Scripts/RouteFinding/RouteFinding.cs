﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;

public class RouteFinding : MonoBehaviour
{

    public bool displayGizmos = false;
    RouteRequestManager requestManager;

    private GameManager game;
    private ViewManager galaxy;

    private List<RouteNode> path;
    // Use this for initialization
    void Awake()
    {
        game = GetComponent<GameManager>();
        galaxy = GetComponent<ViewManager>();
        requestManager = GetComponent<RouteRequestManager>();

    }

    internal void StartRouteFinding(Ship model)
    {
        FindRoute(model);
    }

    void OnDrawGizmos()
    {
        //Gizmos.color = Color.blue;
        //Gizmos.DrawCube(transform.position, new Vector3(10, 10));
    }
    private void FindRoute(Ship model)
    {
        List<RouteNode> openSet = new List<RouteNode>();
        HashSet<RouteNode> closedSet = new HashSet<RouteNode>();

        RouteNode startNode = null;
        RouteNode targetNode = null;
        
        if (model.GetReferenceObj() is Structure)
        {
            startNode = new RouteNode(RouteNodeType.Undock, 0, model.GetReferencePosition(), model.GetReferenceId(), 0) { name = model.GetReferenceObj().GetName() };
        }
        else
        {
            startNode = new RouteNode(RouteNodeType.LeaveSOI, 0, model.GetReferencePosition(), model.GetReferenceId(), 0) { name = model.GetReferenceObj().GetName() };
        }
        if (model.GetShipTarget() is Structure)
        {
            targetNode = new RouteNode(RouteNodeType.Dock, 0, Vector3d.zero, model.ShipTargetId, 0) { name = (model.GetShipTarget() as Structure).GetName() };
            //UnityEngine.Debug.Log("Ship Name: " + model.Name+ " - id: " + model.Id + " StartNode: " + startNode.name + " (" + model.ReferenceObj.ReferenceObj.Name + ")" + " TargetNode: " + targetNode.name + " (" + model.GetShipTarget().ReferenceObj.Name + ")");
        }
        else
        {
            targetNode = new RouteNode(RouteNodeType.Arrive, 0, Vector3d.zero, model.ShipTargetId, 0) { name = (model.GetShipTarget() as SolarBody).GetName() };
        }
        openSet.Add(startNode);
        while (openSet.Count > 0)
        {
            RouteNode currentNode = openSet[0];

            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < currentNode.fCost || openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost)
                {
                    currentNode = openSet[i];
                }

            }
            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            if (currentNode.Equals(targetNode))
            {
                var path = RetracePath(startNode, currentNode);
                UnityEngine.Debug.Log("Found target");
                RouteRequestManager.instance.FinishedProcessingRoute(model, path, true);
                return;
            }
            PositionEntity positionEntity = GameManager.instance.locations[currentNode.currentNodeId];
            foreach (RouteNode neighborNode in positionEntity.GetRouteNodes(currentNode.nodeType))
            {
                if (closedSet.Contains(neighborNode))
                {
                    continue;
                }

                //filter neighbors accordingly

                double newMovementCostToNeighbor = currentNode.gCost + GetDistance(currentNode, neighborNode);
                if (newMovementCostToNeighbor < neighborNode.gCost || !openSet.Contains(neighborNode))
                {
                    neighborNode.gCost = newMovementCostToNeighbor;
                    neighborNode.hCost = GetDistance(neighborNode, targetNode);
                    neighborNode.parent = currentNode;

                    if (!openSet.Contains(neighborNode))
                    {
                        openSet.Add(neighborNode);
                    }
                }
            }
        }

        RouteRequestManager.instance.FinishedProcessingRoute(model,new RouteNode[] { }, false);

        
    }

    RouteNode[] RetracePath(RouteNode startNode, RouteNode endNode)
    {
        path = new List<RouteNode>();
        RouteNode[] waypoints;
        RouteNode currentNode = endNode;


        while (!currentNode.Equals(startNode))
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        path.Add(currentNode);
        //waypoints = SimplifyPath(path);
        waypoints = path.ToArray();
        Array.Reverse(waypoints);

        return waypoints;
    }

    private double GetDistance(RouteNode node1, RouteNode node2)
    {
        return Vector3d.Distance(GameManager.instance.locations[node1.currentNodeId].SystemToGalaxyPosition(), GameManager.instance.locations[node2.currentNodeId].SystemToGalaxyPosition());
    }

}
