﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class RouteRequestManager : MonoBehaviour {

    Queue<RouteRequest> routeRequestQueue = new Queue<RouteRequest>();
    RouteRequest currentRouteRequest;

    internal static RouteRequestManager instance;
    RouteFinding routeFinding;

    bool isProcessingPath;

    void Awake()
    {
        instance = this;
        routeFinding = GetComponent<RouteFinding>();
    }

    /// <summary>
    /// Adds a request to the Route Request Manager
    /// </summary>
    /// <param name="model">model the request is for</param>
    /// <param name="callback">callback function</param>
    /// <returns>bool for whether or not request was successful</returns>
    public static bool RequestRoute(Ship model, Action<Ship, RouteNode[], bool> callback)
    {
        RouteRequest newRequest = new RouteRequest(model, callback);

        if (instance)
        {
            instance.routeRequestQueue.Enqueue(newRequest);
            instance.TryProcessNext();
            return true;
        }
        else return false;

    }

    void TryProcessNext()
    {
        if (!isProcessingPath && routeRequestQueue.Count > 0)
        {
            currentRouteRequest = routeRequestQueue.Dequeue();
            isProcessingPath = true;
            routeFinding.StartRouteFinding(currentRouteRequest.model);
        }
    }

    public void FinishedProcessingRoute(Ship model, RouteNode[] targets, bool success)
    {
        currentRouteRequest.callback(model, targets, success);
        isProcessingPath = false;
        TryProcessNext();
    }

    struct RouteRequest
    {
        public Ship model;
        public Action<Ship, RouteNode[], bool> callback;

        public RouteRequest(Ship _model, Action<Ship, RouteNode[],bool> _callback)
        {
            model = _model;
            callback = _callback;
        }
    }
}
