﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouteNode : IEquatable<RouteNode>
{

    public RouteNodeType nodeType;
    public int speedLimit;
    public Vector3d referencePosition;
    public string currentNodeId;
    public long distanceTarget;
    public string name;
    public RouteNode parent;
    public double hCost;
    public double gCost;

    public RouteNode() { }

    public RouteNode(RouteNodeType nodeType, int speedLimit, Vector3d referencePosition, string currentNodeId, long distanceTarget)
    {
        this.nodeType = nodeType;
        this.speedLimit = speedLimit;
        this.referencePosition = referencePosition;
        this.currentNodeId = currentNodeId;
        this.distanceTarget = distanceTarget;
    }

    public double fCost { get { return gCost = hCost; } }

    public override bool Equals(object obj)
    {
        return Equals(obj as RouteNode);
    }

    public bool Equals(RouteNode other)
    {
        return other != null &&
               nodeType == other.nodeType &&
               currentNodeId == other.currentNodeId;
    }

    public override int GetHashCode()
    {
        var hashCode = 1383598072;
        hashCode = hashCode * -1521134295 + nodeType.GetHashCode();
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(currentNodeId);
        return hashCode;
    }
}

public enum RouteNodeType
{
    LeaveSOI,
    EnterSOI,
    Undock,
    Dock,
    Arrive
}
