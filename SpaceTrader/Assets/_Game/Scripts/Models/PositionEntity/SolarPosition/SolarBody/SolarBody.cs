﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public class SolarBody: SolarPosition
{

    [SerializeField]
    private List<Structure> structures = new List<Structure>();
    [SerializeField]
    private List<SolarBody> satelites = new List<SolarBody>();
    [SerializeField]
    private int totalPopulation;
    [SerializeField]
    private SolarTypes solarType;
    [SerializeField]
    private SolarSubTypes solarSubType;
    [SerializeField]
    private double mass;
    [SerializeField]
    private double bodyRadius;
    [SerializeField]
    private double surfaceTemp;
    [SerializeField]
    private double surfacePressure;
    [SerializeField]
    private double luminosity;
    [SerializeField]
    private double bondAlebo;
    [SerializeField]
    private double greenhouse;
    [SerializeField]
    private double surfaceGravity;
    [SerializeField]
    private List<PlanetTile> planetTiles;
    [SerializeField]
    private List<RawResource> rawResources;
    [SerializeField]
    private Population solarPopulation;
    [SerializeField]
    private Color solarColor;
    public ModelRef<IdentityModel> owner;
    public ModelRefs<IdentityModel> companies = new ModelRefs<IdentityModel>();

    

    public List<Structure> Structures
    {
        get
        {
            return structures;
        }

        set
        {
            structures = value;
        }
    }
    //----------------GameDisplay Properties---------------//
    public bool HasStations
    {
        get
        {
            if (PositionEntityIds.Count > 0) return true;
            for (int i = 0; i < Satelites.Count; i++)
            {
                if (Satelites[i].HasStations) return true;
            }
            return false;
        }
    }
    //----------------Commerce-----------------------------//
    public bool Inhabited { get { return GetSolarPopulation().GetTotalPopulation() > 0; } }

    public List<SolarBody> Satelites
    {
        get
        {
            return satelites;
        }

        set
        {
            satelites = value;
        }
    }

    public SolarBody() { }

    /// <summary>
    /// Used for creating stars
    /// </summary>
    /// <param name="_name"></param>
    /// <param name="_solarIndex"></param>
    /// <param name="_solarSubType"></param>
    /// <param name="mass"></param>
    /// <param name="radius"></param>
    /// <param name="orbit"></param>
    /// <param name="_color"></param>
    /// <param name="_surfaceTemp"></param>
    public SolarBody(string _name, Vector3d galaxyPosition, SolarSubTypes _solarSubType, double mass, double radius, Color _color, double _surfaceTemp):
        base("Galaxy", galaxyPosition)
    {
        SetRawResources(new List<RawResource>());
        SetPlanetTiles(new List<PlanetTile>());
        this.SetMass(mass);
        this.SetBodyRadius(radius);
        SetName(_name);
        this.SetSolarType(GetSolarType());
        SetSolarColor(_color);
        SetSurfaceTemp(_surfaceTemp);
        SetSurfacePressure(1); // In atm
        SetLuminosity(3.846e33 / Mathd.Pow(mass / 2e30, 3)); // in ergs per sec
        SetSurfaceGravity(GameDataModel.G * mass / Mathd.Pow(Units.convertToMeters * radius, 2) / 9.81);
        SetSolarType(SolarTypes.Star);
        SetSolarSubType(_solarSubType);
        SetBondAlebo(.5);
        SetSurfacePressure(1);
        SetGreenhouse(.0137328 * Mathd.Pow(GetSurfacePressure(), 2) + .0986267 * GetSurfacePressure());
    }

    public SolarBody(string _name, string referenceId, SolarTypes _solarType, SolarSubTypes _solarSubType, double mass, double radius, double SMA, double ECC, double MNA, double EPH, double LPE,double INC, double LAP, Color _color, SolarBody star):
        base(referenceId, new Vector3d(0,SMA, 0), SMA, ECC, MNA, EPH, LPE, INC, LAP)
    {
        this.SetMass(mass);
        this.SetBodyRadius(radius);
        SetName(_name);
        SetTotalPopulation(0);
        this.SetSolarType(_solarType);
        this.SetSolarSubType(_solarSubType);
        SetSolarColor(_color);
        SetSurfaceGravity(GameDataModel.G * mass / Mathd.Pow(Units.convertToMeters * radius, 2) / 9.81);
        SetSurfacePressure(0); // In atm
        SetBondAlebo(Random.value);
        

        if (GetSolarType() == SolarTypes.Moon || GetSolarType() == SolarTypes.DwarfPlanet)
        {
            SetSurfacePressure(Random.Range(.01f, 1));
            SetGreenhouse(.0137328 * Mathd.Pow(GetSurfacePressure(), 2) + .0986267 * GetSurfacePressure());
            if (GetGreenhouse() < 0)
            {
                SetGreenhouse(0);
            }
            if (GetSolarType() == SolarTypes.Moon)
            {
                SetSurfaceTemp(Mathd.Pow(((1 - GetBondAlebo()) * star.GetLuminosity()) / ((16 * Mathd.PI * 5.6705e-8) * Mathd.Pow(SystemPosition.magnitude * Units.convertToMeters, 2)), .25) * Mathd.Pow((1 + .438 * GetGreenhouse() * .9), .25));
            }
            else //Assumes solarbody is a planet orbiting the center star
            {
                SetSurfaceTemp(Mathd.Pow(((1 - GetBondAlebo()) * star.GetLuminosity()) / ((16 * Mathd.PI * 5.6705e-8) * Mathd.Pow(SMA * Units.convertToMeters, 2)), .25) * Mathd.Pow((1 + .438 * GetGreenhouse() * .9), .25));
            }

            var rand = Random.value;

            if (GetSurfaceTemp() - 273.15 < -50)
            {
                if (rand < .15)
                {
                    SetSolarSubType(SolarSubTypes.Ice);
                }
                else if (rand < .2)
                {
                    SetSolarSubType(SolarSubTypes.Volcanic);
                }
            }
            else if (GetSurfaceTemp() - 273.15 > 120)
            {
                if (GetSurfacePressure() > .1)
                {
                    if (rand < .75)
                    {
                        SetSolarSubType(SolarSubTypes.Volcanic);
                    }
                }
                else
                {
                    if (rand < .25)
                    {
                        SetSolarSubType(SolarSubTypes.Desert);
                    }
                }

            }
            else
            {
                if (GetSurfacePressure() > .5)
                {
                    if (rand < .55)
                    {
                        SetSolarSubType(SolarSubTypes.Ocean);
                    }
                    else if (rand < .85)
                    {
                        SetSolarSubType(SolarSubTypes.EarthLike);
                    }
                    else if (rand < .95)
                    {
                        SetSolarSubType(SolarSubTypes.Volcanic);
                    }
                }
                else if (GetSurfacePressure() > .25)
                {
                    if (rand < .75)
                    {
                        SetSolarSubType(SolarSubTypes.EarthLike);
                    }
                    else if (rand < .85)
                    {
                        SetSolarSubType(SolarSubTypes.Ocean);
                    }
                }
            }
        }
        else if (GetSolarSubType() == SolarSubTypes.GasGiant)
        {
            SetSurfacePressure(Random.Range(10, 100));
            SetGreenhouse(.0137328 * Mathd.Pow(GetSurfacePressure(), 2) + .0986267 * GetSurfacePressure());
            if (GetGreenhouse() < 0)
            {
                SetGreenhouse(0);
            }
            SetSurfaceTemp(Mathd.Pow(((1 - GetBondAlebo()) * star.GetLuminosity()) / ((16 * Mathd.PI * 5.6705e-8) * Mathd.Pow(SMA * Units.convertToMeters, 2)), .25) * Mathd.Pow((1 + .438 * GetGreenhouse() * .9), .25));
        }
        else if (GetSolarType() == SolarTypes.Planet && GetSolarSubType() == SolarSubTypes.Rocky)
        {
            var rand = Random.value;
            if (rand < .75)
            {
                SetSurfacePressure(Random.Range(.01f, 2));
            }
            else
            {
                SetSurfacePressure(Random.Range(.01f, 100));
            }

            SetGreenhouse(.0137328 * Mathd.Pow(GetSurfacePressure(), 2) + .0986267 * GetSurfacePressure());
            if (GetGreenhouse() < 0)
            {
                SetGreenhouse(0);
            }
            SetSurfaceTemp(Mathd.Pow(((1 - GetBondAlebo()) * star.GetLuminosity()) / ((16 * Mathd.PI * 5.6705e-8) * Mathd.Pow(SMA * Units.convertToMeters, 2)), .25) * Mathd.Pow((1 + .438 * GetGreenhouse() * .9), .25));

            rand = Random.value;
            if (GetSurfaceTemp() - 273.15 < -50)
            {
                if (rand < .25)
                {
                    SetSolarSubType(SolarSubTypes.Ice);
                }
            }
            else if (GetSurfaceTemp() - 273.15 > 120)
            {
                if (GetSurfacePressure() > 20)
                {
                    if (rand < .75)
                    {
                        SetSolarSubType(SolarSubTypes.Volcanic);
                    }
                }
                else
                {
                    if (rand < .25)
                    {
                        SetSolarSubType(SolarSubTypes.Desert);
                    }
                }
                
            }
            else
            {
                if (GetSurfacePressure() > 40)
                {
                    if (rand < .55)
                    {
                        SetSolarSubType(SolarSubTypes.Volcanic);
                    }
                    else if (rand < .85)
                    {
                        SetSolarSubType(SolarSubTypes.Ocean);
                    }
                }
                else if (GetSurfacePressure() > 20)
                {
                    if (rand < .55)
                    {
                        SetSolarSubType(SolarSubTypes.Ocean);
                    }
                    else if (rand < .85)
                    {
                        SetSolarSubType(SolarSubTypes.EarthLike);
                    }
                    else if (rand < .95)
                    {
                        SetSolarSubType(SolarSubTypes.Volcanic);
                    }
                }
                else if (GetSurfacePressure() > .25)
                {
                    if (rand < .75)
                    {
                        SetSolarSubType(SolarSubTypes.EarthLike);
                    }
                    else if (rand < .85)
                    {
                        SetSolarSubType(SolarSubTypes.Ocean);
                    }
                }
            }
        }
        
        //------------Assigning Colors----------------//
        if (GetSolarSubType() == SolarSubTypes.Rocky)
        {
            SetSolarColor(Color.gray);
        }
        else if (GetSolarSubType() == SolarSubTypes.EarthLike)
        {
            SetSolarColor(Color.green);
        }
        else if (GetSolarSubType() == SolarSubTypes.Ice)
        {
            SetSolarColor(Color.white);
        }
        else if (GetSolarSubType() == SolarSubTypes.Desert)
        {
            SetSolarColor(Color.yellow);
        }
        else if (GetSolarSubType() == SolarSubTypes.Ocean)
        {
            SetSolarColor(Color.blue);
        }
        else if (GetSolarSubType() == SolarSubTypes.Volcanic)
        {
            SetSolarColor(Color.red);
        }
        else if (GetSolarSubType() == SolarSubTypes.GasGiant)
        {
            SetSolarColor(new Color(1, .5f, 0));
        }

        SetRawResources(new List<RawResource>());
        SetPlanetTiles(new List<PlanetTile>());

        if (GetSolarType() != SolarTypes.Star && GetSolarSubType() != SolarSubTypes.GasGiant)
        {
            GeneratePlanetTiles();
        }

        SetScaleIndex();
    }

    public int GetTotalPopulation()
    {
        return totalPopulation;
    }
    private void SetTotalPopulation(int value)
    {
        totalPopulation = value;
    }
    public SolarTypes GetSolarType()
    {
        return solarType;
    }
    private void SetSolarType(SolarTypes value)
    {
        solarType = value;
    }
    public SolarSubTypes GetSolarSubType()
    {
        return solarSubType;
    }
    private void SetSolarSubType(SolarSubTypes value)
    {
        solarSubType = value;
    }

    /// <summary>
    /// Mass in kg
    /// </summary>
    public double GetMass()
    {
        return mass;
    }

    /// <summary>
    /// Mass in kg
    /// </summary>
    public void SetMass(double value)
    {
        mass = value;
    }



    /// <summary>
    /// In km
    /// </summary>
    public double GetBodyRadius()
    {
        return bodyRadius;
    }

    /// <summary>
    /// In km
    /// </summary>
    public void SetBodyRadius(double value)
    {
        bodyRadius = value;
    }




    //-----------------Star Properties---------------------//
    /// <summary>
    /// Temperature at surface of solar body, measured in K
    /// </summary>
    public double GetSurfaceTemp()
    {
        return surfaceTemp;
    }


    //-----------------Star Properties---------------------//
    /// <summary>
    /// Temperature at surface of solar body, measured in K
    /// </summary>
    private void SetSurfaceTemp(double value)
    {
        surfaceTemp = value;
    }



    /// <summary>
    /// Pressure at surface of solar body, measured in atm
    /// </summary>
    public double GetSurfacePressure()
    {
        return surfacePressure;
    }

    /// <summary>
    /// Pressure at surface of solar body, measured in atm
    /// </summary>
    private void SetSurfacePressure(double value)
    {
        surfacePressure = value;
    }



    public double GetLuminosity()
    {
        return luminosity;
    }

    private void SetLuminosity(double value)
    {
        luminosity = value;
    }




    //----------------Rocky Satellite Properties-----------//
    public double GetBondAlebo()
    {
        return bondAlebo;
    }


    //----------------Rocky Satellite Properties-----------//
    private void SetBondAlebo(double value)
    {
        bondAlebo = value;
    }



    public double GetGreenhouse()
    {
        return greenhouse;
    }

    private void SetGreenhouse(double value)
    {
        greenhouse = value;
    }



    public double GetSurfaceGravity()
    {
        return surfaceGravity;
    }

    private void SetSurfaceGravity(double value)
    {
        surfaceGravity = value;
    }



    public List<PlanetTile> GetPlanetTiles()
    {
        return planetTiles;
    }

    private void SetPlanetTiles(List<PlanetTile> value)
    {
        planetTiles = value;
    }

    public List<RawResource> GetRawResources()
    {
        return rawResources;
    }

    private void SetRawResources(List<RawResource> value)
    {
        rawResources = value;
    }

    public Population GetSolarPopulation()
    {
        return solarPopulation;
    }

    private void SetSolarPopulation(Population value)
    {
        solarPopulation = value;
    }

    //TODO: remove color as it has to deal with ui and not the actual object
    public Color GetSolarColor()
    {
        return solarColor;
    }

    //TODO: remove color as it has to deal with ui and not the actual object
    public void SetSolarColor(Color value)
    {
        solarColor = value;
    }

    private void GeneratePlanetTiles()
    {
        int numTiles = (int)(2 * Mathd.PI * GetBodyRadius() * Units.convertToMeters / 4e7 * 15) + 1;
        System.Random rand = new System.Random(GameManager.instance.data.id);
        for(int i = 0; i < numTiles; i++)
        {
            PlanetTile tile = new PlanetTile(GetSolarSubType());
            var index = GetPlanetTiles().FindIndex(x =>x.GetPlanetTileType() == tile.GetPlanetTileType());
            if (index >= 0)
            {
                GetPlanetTiles()[index].SetCount(GetPlanetTiles()[index].GetCount() + 1);
            }
            else
                GetPlanetTiles().Add(tile);

            foreach (RawResourceBlueprint raw in GameManager.instance.data.rawResources.Model.rawResources)
            {
                RawResourceInfo info = raw.GetInfo(tile.GetPlanetTileType());

                if (rand.NextDouble() < info.probability)
                {
                    RawResource resource = new RawResource(raw.name, raw.id, rand.Next(info.maxAmount), Random.Range(info.accessibility.x, info.accessibility.y));

                    index = GetRawResources().FindIndex(x => x.GetId() == resource.GetId());

                    if (index >= 0)
                    {
                        GetRawResources()[index].AddAmount(resource.GetAmount());
                    }
                    else
                    {
                        GetRawResources().Add(resource);
                    }
                }
                
            }
           
            

        }
        
    }

    /// <summary>
    /// Gets the main star of this solar object, returns itself if it is the main star
    /// </summary>
    /// <returns>parent star</returns>
    public SolarBody GetStar()
    {
        if (GetReferenceId() == "Galaxy") return this;
        return ((SolarBody)GameManager.instance.locations[GetReferenceId()]).GetStar();
    }

    /// <summary>
    /// Returns all children solar bodies under this solar body
    /// </summary>
    /// <returns>All children solar bodies</returns>
    public List<SolarBody> GetAllSolarBodies()
    {
        List<SolarBody> bodies = new List<SolarBody>(Satelites);
        foreach (SolarBody body in Satelites)
        {
            bodies.AddRange(body.GetAllSolarBodies());
        }
        return bodies;
    }

    public Structure GetStructure(string structureId)
    {
        int index = structures.FindIndex(x => x.GetId() == structureId);
        if (index == -1)
            index = structures.FindIndex(x => x.GetId() == structureId);
        else
            return structures[index];
        if (index != -1)
            return structures[index];
        else
            return null;
    }

    public void Population(int people)
    {
        SetSolarPopulation(new Population(this));
    }

    public override List<RouteNode> GetRouteNodes(RouteNodeType nodeType)
    {
        List<RouteNode> routeNodes = base.GetRouteNodes(nodeType);

        switch (nodeType)
        {
            case RouteNodeType.LeaveSOI:
                //Add enter SOI Nodes
                if (GetReferenceId() != "Galaxy")
                {
                    foreach (SolarBody body in GetReferenceBody().Satelites)
                    {
                        PositionEntity pos = GameManager.instance.locations[body.GetId()];
                        routeNodes.Add(new RouteNode(RouteNodeType.EnterSOI, pos.GetSpeedLimit(), pos.GetReferencePosition(), body.GetId(), body.GetSOI()) { name = pos.GetName() });
                    }
                }
                break;

            case RouteNodeType.EnterSOI:

                foreach (SolarBody body in Satelites)
                {
                    PositionEntity pos = GameManager.instance.locations[body.GetId()];

                    routeNodes.Add(new RouteNode(RouteNodeType.EnterSOI, pos.GetSpeedLimit(), pos.GetReferencePosition(), body.GetId(), body.GetSOI()) { name = pos.GetName() });
                }
                break;

        }
        return routeNodes;
    }

    public override long GetSOI()
    {
        if (GetSMA() == 0) // Assuming sun SOI.
        {
            return (long) (Units.M * Mathd.Pow(GetMass(), .25));
        }

        long trueSOI = (long) (GetSMA() * Mathd.Pow(GetMass() / GetReferenceBody().GetMass(), .4f));
        long estSOI = Units.M * (long) Mathd.Pow(GetMass(), .25);
        return (trueSOI < estSOI) ? trueSOI : estSOI;
    }

    public double Temp(double time)
    {
        if (GetSolarType() == SolarTypes.Star)
            return GetSurfaceTemp();

        Vector3d position = GetPosition(time);

        var solar = GetStar();

        var distance = SystemPosition.magnitude;
        //(Mathd.Pow(solar.bodyRadius, 2) / Mathd.Pow(position.magnitude, 2)) * solar.surfaceTemp * 2314;
        SetSurfaceTemp(Mathd.Pow(((1 - GetBondAlebo()) * solar.GetLuminosity()) / ((16 * Mathd.PI * 5.6705e-8) * Mathd.Pow(distance * Units.convertToMeters, 2)), .25) * Mathd.Pow((1 + .438 * GetGreenhouse() * .9), .25));

        return GetSurfaceTemp();
    }

    #region "Commerce"

    public double GetMarketPrice(string itemId)
    {
        return 1.00;
        //    return GameManager.instance.data.stars[solarIndex[0]].GetMarketPrice(itemId);
    }

    public void SetBuying(Item item)
    {

        //    int itemIndex = -1;
        //    if (item.structureId == -1)
        //    {
        //        itemIndex = buyList.items.FindIndex(x => x.id == item.id);
        //    }
        //    else
        //    {
        //        itemIndex = buyList.items.FindIndex(x => x.id == item.id && x.structureId == item.structureId);
        //    }
        //    if (itemIndex >= 0)
        //    {
        //        buyList.items[itemIndex].SetAmount(item.amount, item.price);
        //    }
        //    else
        //    {
        //        buyList.AddItem(item);
        //    }

        //    GameManager.instance.data.stars[solarIndex[0]].SetBuying(item);
    }
    public void RemoveBuying(string itemId, string structureId, double amount)
    {

        //    int itemIndex = -1;
        //    if (structureId == -1)
        //    {
        //        itemIndex = buyList.items.FindIndex(x => x.id == itemId);
        //    }
        //    else
        //    {
        //        itemIndex = buyList.items.FindIndex(x => x.id == itemId && x.structureId == structureId);
        //    }

        //    if (itemIndex >= 0)
        //    {
        //        buyList.items[itemIndex].RemoveAmount(amount);
        //        if (buyList.items[itemIndex].amount == 0)
        //            buyList.items.RemoveAt(itemIndex);
        //    }

        //    GameManager.instance.data.stars[solarIndex[0]].RemoveBuying(itemId,structureId, amount);
    }

    public void SetSelling(Item item)
    {
        //    int itemIndex = -1;
        //    if (item.structureId == -1)
        //    {
        //        itemIndex = sellList.items.FindIndex(x => x.id == item.id);
        //    }
        //    else
        //    {
        //        itemIndex = sellList.items.FindIndex(x => x.id == item.id && x.structureId == item.structureId);
        //    }
        //    if (itemIndex >= 0)
        //    {
        //        sellList.items[itemIndex].SetAmount(item.amount, item.price);
        //    }
        //    else
        //    {
        //        sellList.AddItem(item);
        //    }
        //    GameManager.instance.data.stars[solarIndex[0]].SetSelling(item);
        }



    public void RemoveSelling(string itemId, string structureId, double amount)
    {

        //    int itemIndex = -1;
        //    if (structureId == -1)
        //    {
        //        itemIndex = sellList.items.FindIndex(x => x.id == itemId);
        //    }
        //    else
        //    {
        //        itemIndex = sellList.items.FindIndex(x => x.id == itemId && x.structureId == structureId);
        //    }

        //    if (itemIndex >= 0)
        //    {
        //        sellList.items[itemIndex].RemoveAmount(amount);
        //        if (sellList.items[itemIndex].amount == 0)
        //            sellList.items.RemoveAt(itemIndex);
        //    }

        //    GameManager.instance.data.stars[solarIndex[0]].RemoveSelling(itemId, structureId, amount);
    }

        #endregion
    
    public string GetInfo(double time)
    {
        double parentMass;
        if (GetSolarType() == SolarTypes.Star)
        {
            parentMass = GetMass();
            return string.Format("Mass: {0}\nRadius: {1}\nSatelite Count: {2}\nSurfaceTemp: {3} C\nSurface Gravity: {4} g",
           GetMass().ToString("G4") + " kg",
           Units.ReadDistance(GetBodyRadius()),
           Satelites.Count,
           GetSurfaceTemp().ToString("0.00"), 
           (GetSurfaceGravity()).ToString("0.00"));
        }
        else
        {
            parentMass = ((SolarBody)GameManager.instance.locations[GetReferenceId()]).GetMass();
        }

        var sTemp = Temp(time) - 273.15;

        return string.Format("Type: {8}\nSubType: {9}\nMass: {0}\nRadius: {1}\nOrbital Period: {2}\nSatelite Count: {3}\nSemi-Major Axis: {4}\nSurfaceTemp: {5} C\nSurface Gravity: {6} g\nSurfacePressure: {7} atm\n",
            GetMass().ToString("G4") + " kg",
            Units.ReadDistance(GetBodyRadius()),
            Dated.ReadTime(OrbitalPeriod(parentMass)),
            Satelites.Count,
            Units.ReadDistance(GetSMA()),
            sTemp.ToString("0.0"),
            (GetSurfaceGravity()).ToString("0.00"),
            GetSurfacePressure().ToString("0.00"),
            GetSolarType().ToString(),
            GetSolarSubType().ToString());
    }
}
