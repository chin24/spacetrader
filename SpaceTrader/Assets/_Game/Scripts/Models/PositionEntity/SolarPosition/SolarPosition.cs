﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SolarPosition : PositionEntity
{
    //TODO: move scale information to seperate class
    [SerializeField]
    /// <summary>
    /// Used to determine the scale of the world view
    /// </summary>
    private int scaleIndex = 1;

    //TODO: Will need to be moved when a view manager is created
    private Vector3[] approximatePositions;

    [SerializeField]
    private double sMA;
    [SerializeField]
    private double eCC;
    [SerializeField]
    private double iNC;
    [SerializeField]
    private double lPE;
    [SerializeField]
    private double lAP;
    [SerializeField]
    private double mNA;
    [SerializeField]
    private double ePH;

    public Vector3 LastKnownPosition { get; private set; }

    public SolarPosition() { }

    public SolarPosition(string referenceId, Vector3d localPosition):base(referenceId, localPosition) { }

    /// <summary>
    /// Creates the orbit of an object given the correct parameters
    /// </summary>
    /// <param name="SMA">Semi-major axis.</param>
    /// <param name="ECC"></param>
    /// <param name="MNA">Mean anomaly at epoch. Between -2pi and 2pi.</param>
    /// <param name="EPH">Epoch, a reference time.</param>
    /// <param name="LPE">Longitude of periapsis. Angle in radians</param>
    public SolarPosition(double SMA, double ECC, double MNA, double EPH, double LPE, double INC, double LAP)
    {
        this.SetSMA(SMA);
        this.SetECC(ECC);
        this.SetMNA(MNA);
        this.SetEPH(EPH);
        this.SetLPE(LPE);
        this.SetINC(INC);
        this.SetLAP(LAP);
    }

    /// <summary>
    /// Creates the orbit of an object given the correct parameters
    /// </summary>
    /// <param name="SMA">Semi-major axis.</param>
    /// <param name="ECC"></param>
    /// <param name="MNA">Mean anomaly at epoch. Between -2pi and 2pi.</param>
    /// <param name="EPH">Epoch, a reference time.</param>
    /// <param name="LPE">Longitude of periapsis. Angle in radians</param>
    public SolarPosition(string referenceId, Vector3d localPosition, double SMA, double ECC, double MNA, double EPH, double LPE, double INC, double LAP) :
        base(referenceId, localPosition)
    {
        this.SetSMA(SMA);
        this.SetECC(ECC);
        this.SetMNA(MNA);
        this.SetEPH(EPH);
        this.SetLPE(LPE);
        this.SetINC(INC);
        this.SetLAP(LAP);
    }

    /// <summary>
    /// Epoch, a reference time.
    /// </summary>
    public double GetEPH()
    {
        return ePH;
    }

    /// <summary>
    /// Epoch, a reference time.
    /// </summary>
    public void SetEPH(double value)
    {
        ePH = value;
    }

    /// <summary>
    /// Mean anomaly at epoch. Between -2pi and 2pi.
    /// </summary>
    public double GetMNA()
    {
        return mNA;
    }

    /// <summary>
    /// Mean anomaly at epoch. Between -2pi and 2pi.
    /// </summary>
    public void SetMNA(double value)
    {
        mNA = value;
    }

    /// <summary>
    /// Longitude of ascending node. between 0 and 2pi
    /// </summary>
    public double GetLAP()
    {
        return lAP;
    }

    /// <summary>
    /// Longitude of ascending node. between 0 and 2pi
    /// </summary>
    public void SetLAP(double value)
    {
        lAP = value;
    }

    /// <summary>
    /// Longitude of periapsis. Angle in radians between 0 and 2pi
    /// </summary>
    public double GetLPE()
    {
        return lPE;
    }

    /// <summary>
    /// Longitude of periapsis. Angle in radians between 0 and 2pi
    /// </summary>
    public void SetLPE(double value)
    {
        lPE = value;
    }

    /// <summary>
    /// inclination of orbit between 0 and pi
    /// </summary>
    public double GetINC()
    {
        return iNC;
    }

    /// <summary>
    /// inclination of orbit between 0 and pi
    /// </summary>
    public void SetINC(double value)
    {
        iNC = value;
    }

    public double GetECC()
    {
        return eCC;
    }

    public void SetECC(double value)
    {
        eCC = value;
    }

    /// <summary>
    /// Semi-major axis. Currently stored in km
    /// </summary>
    public double GetSMA()
    {
        return sMA;
    }

    /// <summary>
    /// Semi-major axis. Currently stored in km
    /// </summary>
    public void SetSMA(double value)
    {
        sMA = value;
    }

    public Vector3[] GetApproximatePositions()
    {
        return approximatePositions;
    }

    private void SetApproximatePositions(Vector3[] value)
    {
        approximatePositions = value;
    }

    protected void SetScaleIndex()
    {
        if (this.GetReferenceBody().GetSolarType() == SolarTypes.Star)
        {
            scaleIndex = 1; //Gm
        }
        else scaleIndex = 2; //Mm
    }

    //-----------------Orbital Functions-------------------//
    #region "Orbital Functions"
    /// <summary>
    /// Local position of solar body scaled to the game position
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public Vector3 GetGamePosition(double time)
    {
        var pos2 = (GetPosition(time) * (Position.KmToLocalScale(scaleIndex)));
        LastKnownPosition = (Vector3)pos2;
        return (Vector3)pos2;
    }

    public Vector3 SetOrbit(double time, double orbitedObjectMass)
    {
        SetApproximatePositions(new Vector3[361]);

        double timeInc = (OrbitalPeriod(orbitedObjectMass) / 360);

        for (var b = 0; b < 361; b++)
        {
            GetApproximatePositions()[b] = GetGamePosition(time);
            time += timeInc;
        }

        return LastKnownPosition;
    }
    /// <summary>
    /// Returns local position of solar body in km
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    protected Vector3d GetPosition(double time)
    {
        if (GetSMA() == 0)
        {
            return Vector3d.zero;
        }

        double parentMass = ((SolarBody)GameManager.instance.locations[GetReferenceId()]).GetMass();
        var ena = ENA(time, parentMass);
        var pos = new Vector3d(GetSMA() * (Mathd.Cos(ena) - GetECC()), GetSMA() * Mathd.Sqrt(1 - (GetECC() * GetECC())) * Mathd.Sin(ena));
        var pol = new Polar2d(pos);
        pol.angle += GetLPE();
        var pos2 = pol.cartesian;

        //rotate by inclination
        var z = Mathd.Sin(GetINC()) * pos2.x;
        var x = Mathd.Cos(GetINC()) * pos2.x;
        var y = pos2.y;
        //rotate by longitude of ascending node
        var xTemp = x;
        x = Mathd.Cos(GetLAP()) * xTemp - Mathd.Sin(GetLAP()) * y;
        y = Mathd.Sin(GetLAP()) * xTemp + Mathd.Cos(GetLAP()) * y;

        Vector3d pos3 = new Vector3d(x, y, z);
        //Update Reference Position
        SetReferencePosition(pos3);

        return pos3;

    }

   
    /// <summary>
    /// Eccentric anomaly at a future time given the current eccentric anomaly
    /// </summary>
    /// <returns></returns>
    private double ENA(double time, double parentMass)
    {
        double M = GetMNA() + (MM(parentMass) * (time - GetEPH()));
        double E = M;
        for (int i = 0; i < 10; i++) // How many times it iterates to solve the equation.
        {
            E = M + GetECC() * Mathd.Sin(E);
        }
        return E;
    }
    /// <summary>
    /// Orbital period in seconds.
    /// </summary>
    /// <param name="parentMass"></param>
    /// <returns></returns>
    public double OrbitalPeriod(double parentMass) { return 2 * Mathf.PI / MM(parentMass); }

    public double MM(double parentMass) // Mean motion of orbit in radians per second.
    {
        return Mathd.Sqrt((GameDataModel.G * parentMass) / (Mathd.Pow((double)GetSMA() * Units.convertToMeters, 3)));
    }

    
    #endregion

    public string GetOrbitalInfo()
    {
        return string.Format("Semi-major Axis:{0}\nEcc: {1}\nMean Anomaly at Epoch: {2}\nEpoch: {3}\nLong of Periapsis: {4}",
        Units.ReadDistance(GetSMA()),
        GetECC(), // 1
        GetMNA(),
        GetEPH(),
        GetLPE() // 4
        );
    }
}
