﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PositionEntity {

    [SerializeField]
    private string referenceId;
    [SerializeField]
    private List<string> positionEntityIds = new List<string>();
    [SerializeField]
    private string name;
    [SerializeField]
    private string id;
    [SerializeField]
    private int speedLimit;
    [SerializeField]
    private Vector3d referencePosition;

    //---------------Public Fields------------------------------------------//

    /// <summary>
    /// Position in relation to the center of the solar system in km.
    /// </summary>
    public Vector3d SystemPosition
    {
        get
        {
            if (GetReferenceId() == "Galaxy")
            {
                return Vector3d.zero;
            }
            return GameManager.instance.locations[GetReferenceId()].SystemPosition + GetReferencePosition();
            //if (solarIndex.Count == 1) //Entity orbiting star
            //{

            //}
            //else if (solarIndex.Count == 2) //Entity orbiting Planet
            //{
            //    return 1 / SystemConversion[2];
            //}
            //else if (solarIndex.Count == 3) //Entity orbiting Moon, close to a planet
            //{
            //    return 1 / SystemConversion[3];
            //}
            //else if (solarIndex.Count == 4) //Entity near another entity/space station
            //{
            //    return 1 / SystemConversion[4];
            //}
            //else if (solarIndex.Count == 5)
            //    return 1 / SystemConversion[4];
            //else
            //    throw new Exception("Local Scale is off, " + solarIndex.Count.ToString());
        }
        set
        {
            SetReferencePosition(value - GameManager.instance.locations[GetReferenceId()].SystemPosition);
        }
    }

    /// <summary>
    /// Holds the children entities of this object
    /// </summary>
    public List<string> PositionEntityIds
    {
        get
        {
            return positionEntityIds;
        }

        set
        {
            positionEntityIds = value;
        }
    }

    public PositionEntity() { }
    public PositionEntity(string _referenceId) : this(_referenceId, Vector3d.zero)
    {
    }
    public PositionEntity(string _referenceId, Vector3d _localPosition)
    {
        id = GetType().ToString() + GameManager.instance.data.id++.ToString();
        GameManager.instance.locations[GetId()] = this;
        referenceId = _referenceId;
        SetReferencePosition(_localPosition);
    }

    //TODO: double check the measure for SOI
    /// <summary>
    /// In km
    /// </summary>
    public virtual long GetSOI()
    {
        return 10;
    }
    /// <summary>
    /// Position in relationship to the refence solar body in solarIndex in km; This is the main location of solar position. If there is no reference, then the units are in ly.
    /// </summary>
    public Vector3d GetReferencePosition()
    {
        return referencePosition;
    }

    /// <summary>
    /// Position in relationship to the refence solar body in solarIndex in km; This is the main location of solar position. If there is no reference, then the units are in ly.
    /// </summary>
    public void SetReferencePosition(Vector3d value)
    {
        referencePosition = value;
    }
    /// <summary>
    /// Used to determine the max speed an object can travel in this entities SOI. In km/s
    /// </summary>
    public int GetSpeedLimit()
    {
        return speedLimit;
    }



    /// <summary>
    /// Used to determine the max speed an object can travel in this entities SOI. In km/s
    /// </summary>
    public void SetSpeedLimit(int value)
    {
        speedLimit = value;
    }
    public string GetId()
    {
        return id;
    }

    public string GetName()
    {
        return name;
    }

    public void SetName(string value)
    {
        name = value;
    }

    public PositionEntity GetReferenceObj()
    {
        if (GetReferenceId() == "") throw new System.Exception("No reference");
        if (GetReferenceId() == "Galaxy")
        {
            throw new System.Exception("Referenced to Galaxy");
        }

        return (GameManager.instance.locations[GetReferenceId()]);
    }
    /// <summary>
    /// Returns the first parent solarbody of object
    /// </summary>
    public SolarBody GetReferenceBody()
    {
        if (GetReferenceObj() as SolarBody == null)
        {
            return GameManager.instance.locations[GetReferenceId()].GetReferenceBody();
        }
        return ((SolarBody)GameManager.instance.locations[GetReferenceId()]);
    }
    /// <summary>
    /// The position of the entity that is in galaxy coordinate system.
    /// </summary>
    public Vector3d GetStarPosition()
    {
        if (GetReferenceId() == "Galaxy")
        {
            return GetReferencePosition();
        }

        return GameManager.instance.locations[GetReferenceId()].GetStarPosition();
    }

    /// <summary>
    /// The parenting id location.
    /// </summary>
    public string GetReferenceId()
    {
        return referenceId;
    }

    /// <summary>
    /// The parenting id location.
    /// </summary>
    public void SetReferenceId(string newReferenceId)
    {
        SetReferenceId(GameManager.instance.locations[newReferenceId]);
    }

    public void SetReferenceId(PositionEntity newReferenceObj)
    {

        SetReferencePosition(SystemPosition - newReferenceObj.SystemPosition);

        GetReferenceObj().PositionEntityIds.Remove(GetId());

        referenceId = newReferenceObj.GetId();

        newReferenceObj.PositionEntityIds.Add(GetId());

    }

    public virtual List<RouteNode> GetRouteNodes(RouteNodeType nodeType)
    {
        List<RouteNode> routeNodes = new List<RouteNode>();
        switch (nodeType)
        {
            case RouteNodeType.Undock:
                routeNodes.Add(new RouteNode(RouteNodeType.LeaveSOI, GetSpeedLimit(), Vector3d.right * GetSOI(), GetId(), 0) { name = GetName() });
                routeNodes.Add(new RouteNode(RouteNodeType.Dock, GetSpeedLimit(), Vector3d.right * GetSOI(), GetId(), 0) { name = GetName() });
                //Add stations and ships 
                foreach (string posId in PositionEntityIds)
                {
                    PositionEntity pos = GameManager.instance.locations[posId];

                    routeNodes.Add(new RouteNode(RouteNodeType.EnterSOI, pos.GetSpeedLimit(), pos.GetReferencePosition(), posId, pos.GetSOI()) { name = pos.GetName() });
                }
                break;
            case RouteNodeType.LeaveSOI:
                
                //Add enter SOI Nodes
                if (GetReferenceId() != "Galaxy")
                {
                    // parent nodes
                    routeNodes.Add(new RouteNode(RouteNodeType.LeaveSOI, GetReferenceObj().GetSpeedLimit(), Vector3d.right * GetSOI(), GetReferenceId(), 0) { name = GetReferenceObj().GetName() });
                    if (GetReferenceObj() is Structure)
                    {
                        routeNodes.Add(new RouteNode(RouteNodeType.Dock, GetReferenceObj().GetSpeedLimit(), GetReferenceObj().GetReferencePosition(), GetReferenceId(), 0) { name = GetReferenceObj().GetName() });
                    } else
                    {
                        routeNodes.Add(new RouteNode(RouteNodeType.Arrive, GetReferenceObj().GetSpeedLimit(), GetReferenceObj().GetReferencePosition(), GetReferenceId(), 0) { name = GetReferenceObj().GetName() });
                    }
                    
                    // parents children
                    foreach (string posId in GetReferenceObj().PositionEntityIds)
                    {
                        PositionEntity pos = GameManager.instance.locations[posId];

                        routeNodes.Add(new RouteNode(RouteNodeType.EnterSOI, pos.GetSpeedLimit(), pos.GetReferencePosition(), posId, pos.GetSOI()) { name = pos.GetName() });
                    }
                }
                break;

            case RouteNodeType.EnterSOI:

                if (this is Structure)
                {
                    routeNodes.Add(new RouteNode(RouteNodeType.Dock, GetSpeedLimit(), GetReferencePosition(), GetId(), 0) { name = GetName() });
                }
                else
                {
                    routeNodes.Add(new RouteNode(RouteNodeType.Arrive, GetSpeedLimit(), GetReferencePosition(), GetId(), 0) { name = GetName() });
                }

                //Add stations and ships 
                foreach (string posId in PositionEntityIds)
                {
                    PositionEntity pos = GameManager.instance.locations[posId];

                    routeNodes.Add(new RouteNode(RouteNodeType.EnterSOI, pos.GetSpeedLimit(), pos.GetReferencePosition(), posId, pos.GetSOI()) { name = pos.GetName() });
                }
                break;

        }
        return routeNodes;
    }

    /// <summary>
    /// Returns the reference position of a child object in relation to the given parent object
    /// </summary>
    /// <param name="positionEntity">parent object</param>
    /// <returns></returns>
    public Vector3d GetReferencePositionToPositionEntity(PositionEntity positionEntity)
    {
        //TODO: Create a DistanceTo Function to replace this and having to add two solarPositions together (finds shortest reference path between both objects)
        PositionEntity referenceObj = GetReferenceObj();
        Vector3d totalReferencePosition = GetReferencePosition();
        var count = 0;
        while (referenceObj.GetId() != positionEntity.GetId())
        {
            count++;
            if (referenceObj.GetReferenceObj() == referenceObj || count > 1000)
                throw new System.Exception("Loop");
            totalReferencePosition += referenceObj.GetReferencePosition();
            referenceObj = referenceObj.GetReferenceObj();

        }

        return totalReferencePosition;
    }


    //----------------------------------------------//

    /// <summary>
    /// Used to determine the real position of object at given index level taking into account the lower index levels
    /// </summary>
    /// <param name="systemIndex"></param>
    /// <returns></returns>
    public Vector3d SystemToGalaxyPosition()
    {
        if (GetReferenceId() == "Galaxy")
        {
            return GetReferencePosition();
        }
        return GetStarPosition() + SystemPosition * Position.ToLightyears;
    }
}
