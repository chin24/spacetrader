﻿using UnityEngine;
using System.Collections;
using CodeControl;
using System.Collections.Generic;
using System;

public class BlueprintsModel: Model{

    public List<ItemBlueprint> blueprints = new List<ItemBlueprint>();
    

    public BlueprintsModel() { }

    public ItemBlueprint GetItem(string id)
    {
        return blueprints.Find(x => x.Id == id);
    }
    
}




