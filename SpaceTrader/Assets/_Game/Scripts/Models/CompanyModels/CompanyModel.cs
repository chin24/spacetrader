﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeControl;

public class CompanyModel : IdentityModel {

    public string ceoId;
    
    public ModelRefs<GovernmentModel> governmentAccess = new ModelRefs<GovernmentModel>();
    public string researchFocus;
    public double moneyNegativeBalanceDuration;
    

    public CompanyModel() { }
    public CompanyModel(string _name, string research, SolarBody home, GovernmentModel gov, Creature _ceo, IdentityModel owner = null)
        :base(owner)
    {
        name = _name;
        ceoId = _ceo.GetId();
        referenceId = home.GetId();
        researchFocus = research;
        AddKnownSolarBodyId(referenceId);
        home.companies.Add(this);
        gov.localCompanies.Add(this);
        governmentAccess.Add(gov);
        gov.licensedCompanies.Add(this);
        money.StartingBalance(1000000);
        
        GameManager.instance.data.companies.Add(this);

        string structureBlueprintToUse = null;

        //Create blueprint
        var blueprint = GameManager.instance.research.allResearch[research].GenerateBlueprint(this);

        //Find the correct structure to create blueprint
        List<string> possibleStructureResearch = new List<string>();

        foreach (KeyValuePair<string,Research> selectedResearch in GameManager.instance.research.allResearch)
        {
            if (selectedResearch.Value.category == ResearchCategory.Structure)
            {
                if ((selectedResearch.Value.tags.Contains(ResearchTags.ProducesRawProducts) && blueprint.GetResearch().category == ResearchCategory.Raw) ||
                    (selectedResearch.Value.tags.Contains(ResearchTags.ProducesIntermediateProducts) && blueprint.GetResearch().category == ResearchCategory.Intermediate) ||
                    (selectedResearch.Value.tags.Contains(ResearchTags.ProducesShips) && blueprint.GetResearch().category == ResearchCategory.Ship) ||
                    (selectedResearch.Value.tags.Contains(ResearchTags.ProducesStructures) && blueprint.GetResearch().category == ResearchCategory.Structure))
                {
                    if (home.GetSurfacePressure() < .01)
                    {
                        if (selectedResearch.Value.tags.Contains( ResearchTags.NoAtmosphere))
                        {
                            possibleStructureResearch.Add(selectedResearch.Key);
                        }
                    }
                    else if (home.GetSurfacePressure() < .25)
                    {
                        if (selectedResearch.Value.tags.Contains( ResearchTags.LowAtmosphere))
                        {
                            possibleStructureResearch.Add(selectedResearch.Key);
                        }
                    }
                    else
                    {
                        if (selectedResearch.Value.tags.Contains( ResearchTags.Atmosphere))
                        {
                            possibleStructureResearch.Add(selectedResearch.Key);
                        }
                    }
                    if (selectedResearch.Value.tags.Contains( ResearchTags.Station) && home.GetSurfaceGravity() > .01)
                    {
                        possibleStructureResearch.Add(selectedResearch.Key);
                    }
                }
            }
        }

        if (possibleStructureResearch.Count == 0)
            throw new System.Exception("No structure research that supports: " + blueprint.Name);
        //Randomly pick Research
        var pickedStructureResearch = possibleStructureResearch[Random.Range(0, possibleStructureResearch.Count)];

        //Find all available free structure blueprints
        var possibleBlueprints = this.ownedBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == pickedStructureResearch);

        if (possibleBlueprints.Count == 0)
        {
            possibleBlueprints = this.Government.allBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == pickedStructureResearch);
        }
        else
        {
            //TODO: extend government and personal blueprints when no longer picking random blueprints
        }
        if (possibleBlueprints.Count != 0)
        {
            //Randomly pick blueprint
            //TODO: Make smarter blueprint finding algorithm or make another function
            structureBlueprintToUse = possibleBlueprints[Random.Range(0, possibleBlueprints.Count)];

        }
        else
        {
            //Create company to manage blueprints related to the research
            Creature manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, home.GetId(), GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);

            new CompanyModel(Owner.name + " " + pickedStructureResearch + " Branch", pickedStructureResearch, home, gov, manager, this.Owner);

            //Create blueprint
            structureBlueprintToUse = this.Owner.ownedBlueprintIds.Find( x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == pickedStructureResearch);

            if (structureBlueprintToUse == null) throw new System.Exception("Could not find created structure blueprint for: " + pickedStructureResearch);
        }

        //Create the Factor/Driller/Shipyard/etc. needed for blueprint to be created
        var structureTags = GameManager.instance.research.allResearch[pickedStructureResearch].tags;

        if (structureTags.Contains(ResearchTags.ProducesRawProducts))
        {
            if (home.GetRawResources().Exists(x => x.GetName() == blueprint.research) &&
                home.GetRawResources().Find(x => x.GetName() == blueprint.research).owner.Model as GovernmentModel != null &&
                (home.GetRawResources().Find(x => x.GetName() == blueprint.research).user == null || home.GetRawResources().Find(x => x.GetName() == blueprint.research).user.Model == null))
            {
                //Buy Driller
                var structureBlueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(structureBlueprintToUse);
                structureBlueprint.BuyItemInsta(1, "Structures",ref money,"Income",ref structureBlueprint.owner.Model.money);
                //Build Driller 
                new Driller(this, structureBlueprintToUse, blueprint.Id, home.GetId());
                //Buy Resource
                var raw = home.GetRawResources().Find(x => x.GetName() == blueprint.research);
                double rawCost = raw.GetAmount() * 1000000 / Government.rawResourcesAmount[blueprint.research];
                if (this.MoneyTotal * .5 >= rawCost)
                {
                    //Buy all of it
                    raw.owner = new ModelRef<IdentityModel>(this);
                    raw.user = new ModelRef<IdentityModel>(this);
                    Government.money.AddMoney("Income",rawCost);
                    money.SubtractMoney(raw.GetName(), rawCost);
                }
                else
                {
                    //Lease it
                    raw.user = new ModelRef<IdentityModel>(this);
                    Government.money.AddMoney("Income",10000);
                    money.SubtractMoney(raw.GetName(), rawCost);
                }

            }
            else
            {
                //Find all available free driller blueprints
                possibleBlueprints = this.ownedBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == "Driller Ship");

                if (possibleBlueprints.Count == 0)
                {
                    possibleBlueprints = this.Government.allBlueprintIds.FindAll(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == "Driller Ship");
                }
                else
                {
                    //TODO: extend government and personal blueprints when no longer picking random blueprints
                }
                if (possibleBlueprints.Count != 0)
                {
                    //Randomly pick blueprint
                    //TODO: Make smarter blueprint finding algorithm or make another function
                    structureBlueprintToUse = possibleBlueprints[Random.Range(0, possibleBlueprints.Count)];

                }
                else
                {
                    //Create company to manage blueprints related to the research
                    Creature manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, home.GetId(), GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);

                    new CompanyModel(Owner.name + " Driller Ship" + " Branch", "Driller Ship", home, gov, manager, this.Owner);

                    //Create blueprint
                    structureBlueprintToUse = this.Owner.ownedBlueprintIds.Find(x => GameManager.instance.data.blueprintsModel.Model.blueprints.Find(b => b.Id == x).research == "Driller Ship");

                    if (structureBlueprintToUse == null) throw new System.Exception("Could not find created structure blueprint for: " + "Driller Ship");
                }
                //Create company to manage blueprints related to the research
                Creature captain = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, home.GetId(), GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);
                //Buy it
                var structureBlueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(structureBlueprintToUse);
                structureBlueprint.BuyItemInsta(1, "Ships",ref money,"Income",ref structureBlueprint.owner.Model.money);
                new Ship( structureBlueprintToUse, this, captain, referenceId);
            }
                
        }
        else if (structureTags.Contains( ResearchTags.ProducesIntermediateProducts))
        {
            var structureBlueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(structureBlueprintToUse);
            structureBlueprint.BuyItemInsta(1, "Structures",ref money,"Income",ref structureBlueprint.owner.Model.money);
            //Build Factory
            new Factory(this, structureBlueprintToUse,blueprint.Id,home.GetId());
        }
        else if (structureTags.Contains( ResearchTags.ProducesShips))
        {
            var structureBlueprint = GameManager.instance.data.blueprintsModel.Model.GetItem(structureBlueprintToUse);
            structureBlueprint.BuyItemInsta(1, "Structures",ref money,"Income",ref structureBlueprint.owner.Model.money);
            //Build Shipyard
            new Factory(this, structureBlueprintToUse, blueprint.Id, home.GetId()); //Distribution
        }
        else
        {
            throw new System.Exception("Could not find correct structure to build for: " + pickedStructureResearch);
        }
    }

    public override void Update()
    {
        base.Update();
        if (money.moneyTotal > 0)
        {
            moneyNegativeBalanceDuration = 0;
        }
        else
        {
            moneyNegativeBalanceDuration += deltaTime;
            //Sell company if negative balance lasts too long
            if( moneyNegativeBalanceDuration > Dated.Year && Owner == this)
            {
                moneyNegativeBalanceDuration = 0;
                Government.AddSubEntity(this);
                if(entities != null)
                {
                    foreach (IdentityModel subEntity in entities)
                    {
                        Government.AddSubEntity(subEntity);
                    }
                    entities = null;
                }
            }
        }
    }
}
