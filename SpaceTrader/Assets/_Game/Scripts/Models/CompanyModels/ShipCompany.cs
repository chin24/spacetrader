﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeControl;

public class ShipCompany : CompanyModel {

    public string resourceId;


    public ShipCompany() { }
    public ShipCompany(string _name, string research, SolarBody home, GovernmentModel gov, Creature _ceo, IdentityModel _owner = null)
        : base(_name,research, home, gov, _ceo, _owner)
    {

    }

    public override void Update()
    {
        base.Update();
        if (identityState == IdentityState.Initial)
        {
            if (structureIds.Count == 0)
            {
                //BuildDriller();
            }
        }
    }
}
