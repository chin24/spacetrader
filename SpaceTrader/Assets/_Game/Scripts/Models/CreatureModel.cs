﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeControl;

[System.Serializable]
public class Creature : PositionEntity
{
    [SerializeField]
    private double _age;
    [SerializeField]
    private double _born;
    public CreatureType creatureType;
    public bool isPlayer;
    public Money money = new Money();

    public double Age { get => _age; private set => _age = value; }
    public double Born { get => _born; private set => _born = value; }

    
    

    public Creature(string _name, double credits, string referenceId, Dated currentDate, Dated _age, CreatureType type) :
        base(referenceId)
    {
        SetName(_name);
        isPlayer = false;
        creatureType = type;
        //this.solarIndex = solarIndex; TODO: fix solarIndex
        Age = _age.time;
        Born = currentDate.time - Age;
        GameManager.instance.data.creatures.Add(this);
    }

    public Creature(string _name, double credits, Structure structure, Dated currentDate, Dated _age, CreatureType type) :
        base(structure.GetId())
    {
        SetName(_name);
        isPlayer = false;
        creatureType = type;
        //solarIndex = structure.solarIndex; TODO: Fix solar index
        Age = _age.time;
        Born = currentDate.time - Age;
        GameManager.instance.data.creatures.Add(this);
    }

    public Creature(string _name, double credits, Ship ship, Dated currentDate, Dated _age, CreatureType type) :
        base(ship.GetId())
    {
        SetName(_name);
        isPlayer = false;
        creatureType = type;
        //solarIndex = ship.solarIndex; TODO: fix solar index
        Age = _age.time;
        Born = currentDate.time - Age;

        GameManager.instance.data.creatures.Add(this);
    }

}

public enum CreatureType
{
    Human,
    Liid
}

