﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RawResourcesModel: Model
{
    public List<RawResourceBlueprint> rawResources { get; private set; }

    public RawResourcesModel() {

        rawResources = new List<RawResourceBlueprint>();

        int rare = 1000;
        int scarce = 10000;
        int common = 100000;
        int abundant = 1000000;
        rawResources.Add(new RawResourceBlueprint(defaultRawResources.Warium.ToString(), defaultRawResources.Warium.ToString(),
            "Gaseous material mainly used for weapons", 5, 0, 1000 * Dated.Day / common,
            new List<RawResourceInfo>()
        {
            new RawResourceInfo(PlanetTileType.Desert, .1f, Vector2.up, common),
            new RawResourceInfo(PlanetTileType.Grasslands, .5f, Vector2.up,common),
            new RawResourceInfo(PlanetTileType.Ice, .1f, Vector2.up,common),
            new RawResourceInfo(PlanetTileType.Ocean, .5f, Vector2.up,common),
            new RawResourceInfo(PlanetTileType.Rocky, .01f, Vector2.up,common),
            new RawResourceInfo(PlanetTileType.Volcanic, .75f, Vector2.up,common),
        }));

        rawResources.Add(new RawResourceBlueprint(defaultRawResources.Armoroid.ToString(), defaultRawResources.Armoroid.ToString(),
            "Main material used to build structures", 10, 1, 1000 * Dated.Day / abundant,
            new List<RawResourceInfo>()
        {
            new RawResourceInfo(PlanetTileType.Desert, .1f, Vector2.up, abundant),
            new RawResourceInfo(PlanetTileType.Grasslands, .25f, Vector2.up,abundant),
            new RawResourceInfo(PlanetTileType.Ice, .1f, Vector2.up,common),
            new RawResourceInfo(PlanetTileType.Ocean, .05f, Vector2.up,scarce),
            new RawResourceInfo(PlanetTileType.Rocky, .5f, Vector2.up,abundant),
            new RawResourceInfo(PlanetTileType.Volcanic, .25f, Vector2.up,abundant),
        }));

        rawResources.Add(new RawResourceBlueprint(defaultRawResources.Fuelodite.ToString(), defaultRawResources.Fuelodite.ToString(),
            "Used to create ship fuel", 2, 0, 1000 * Dated.Day / abundant,
            new List<RawResourceInfo>()
        {
            new RawResourceInfo(PlanetTileType.Desert, .1f, Vector2.up, scarce),
            new RawResourceInfo(PlanetTileType.Grasslands, .1f, Vector2.up,scarce),
            new RawResourceInfo(PlanetTileType.Ice, .1f, Vector2.up,scarce),
            new RawResourceInfo(PlanetTileType.Ocean, .75f, Vector2.up,abundant),
            new RawResourceInfo(PlanetTileType.Rocky, .1f, Vector2.up,scarce),
            new RawResourceInfo(PlanetTileType.Volcanic, .5f, Vector2.up,common),
        }));

        rawResources.Add(new RawResourceBlueprint(defaultRawResources.Glassitum.ToString(), defaultRawResources.Glassitum.ToString(),
            "Used to create glass like structure", 1, .1f, 1000 * Dated.Day / common,
            new List<RawResourceInfo>()
        {
            new RawResourceInfo(PlanetTileType.Desert, .95f, Vector2.up, abundant),
            new RawResourceInfo(PlanetTileType.Grasslands, .25f, Vector2.up,common),
            new RawResourceInfo(PlanetTileType.Ice, .25f, Vector2.up,abundant),
            new RawResourceInfo(PlanetTileType.Ocean, .05f, Vector2.up,scarce),
            new RawResourceInfo(PlanetTileType.Rocky, .15f, Vector2.up,common),
            new RawResourceInfo(PlanetTileType.Volcanic, .35f, Vector2.up,common),
        }));

        rawResources.Add(new RawResourceBlueprint(defaultRawResources.Goldium.ToString(), defaultRawResources.Goldium.ToString(),
            "Rare gold like material", 3, .5f, 1000 * Dated.Day / rare,
            new List<RawResourceInfo>()
        {
            new RawResourceInfo(PlanetTileType.Desert, .02f, Vector2.up, rare),
            new RawResourceInfo(PlanetTileType.Grasslands, .05f, Vector2.up,rare),
            new RawResourceInfo(PlanetTileType.Ice, .01f, Vector2.up,rare),
            new RawResourceInfo(PlanetTileType.Ocean, .01f, Vector2.up,rare),
            new RawResourceInfo(PlanetTileType.Rocky, .1f, Vector2.up,rare),
            new RawResourceInfo(PlanetTileType.Volcanic, .05f, Vector2.up,rare),
        }));

        rawResources.Add(new RawResourceBlueprint(defaultRawResources.Coppode.ToString(), defaultRawResources.Coppode.ToString(),
            "Used to make intricate machinery", .5f, .2f, 1000 * Dated.Day / scarce,
            new List<RawResourceInfo>()
        {
            new RawResourceInfo(PlanetTileType.Desert, .1f, Vector2.up, scarce),
            new RawResourceInfo(PlanetTileType.Grasslands, .25f, Vector2.up,scarce),
            new RawResourceInfo(PlanetTileType.Ice, .05f, Vector2.up,rare),
            new RawResourceInfo(PlanetTileType.Ocean, .05f, Vector2.up,scarce),
            new RawResourceInfo(PlanetTileType.Rocky, .5f, Vector2.up,common),
            new RawResourceInfo(PlanetTileType.Volcanic, .75f, Vector2.up,common),
        }));

        rawResources.Add(new RawResourceBlueprint(defaultRawResources.Limoite.ToString(), defaultRawResources.Limoite.ToString(),
            "Used to make things more heat resistant", 1.5f, .5f, 1000 * Dated.Day / scarce,
            new List<RawResourceInfo>()
        {
            new RawResourceInfo(PlanetTileType.Desert, .1f, Vector2.up, common),
            new RawResourceInfo(PlanetTileType.Grasslands, .25f, Vector2.up,scarce),
            new RawResourceInfo(PlanetTileType.Ice, .05f, Vector2.up,rare),
            new RawResourceInfo(PlanetTileType.Ocean, .05f, Vector2.up,rare),
            new RawResourceInfo(PlanetTileType.Rocky, .5f, Vector2.up,scarce),
            new RawResourceInfo(PlanetTileType.Volcanic, .75f, Vector2.up,abundant),
        }));


    }

    public RawResourcesModel(List<RawResourceBlueprint> rawResources)
    {
        this.rawResources = rawResources;
    }

    public RawResourceBlueprint GetResource(string id)
    {
        return rawResources.Find(x => x.id == id);
    }
}

[System.Serializable]
public class RawResource
{
    //TODO: check if this should be turned into a struct. I remember last time I did it data manipulation did not work properly
    private double previousTime;
    private double previousAmount;
    private double timeUntilDepleted;
    private double rate;
    public ModelRef<IdentityModel> owner;
    public ModelRef<IdentityModel> user;
    [SerializeField]
    private string name;
    [SerializeField]
    private string id;
    [SerializeField]
    private double amount;
    [SerializeField]
    private double accessibility;
       

    public RawResource(string _name, string _resourceId, int _amount, float accessibility)
    {
        SetName(_name);
        SetId(_resourceId);
        SetAmount(_amount);
        //Vector2 accessRange = GameManager.instance.data.rawResources.Model.GetResource(_resourceId).rawResourceInfos.Find(x => x.planetTileType == tileType).accessibility;
        this.SetAccessibility(accessibility);
    }

    public double GetTimeUntilDepleted()
    {
        return timeUntilDepleted;
    }

    private void SetTimeUntilDepleted(double value)
    {
        timeUntilDepleted = value;
    }

    public double GetAccessibility()
    {
        return accessibility;
    }

    private void SetAccessibility(double value)
    {
        accessibility = value;
    }

    public double GetAmount()
    {
        return amount;
    }

    private void SetAmount(double value)
    {
        amount = value;
    }

    public string GetId()
    {
        return id;
    }

    private void SetId(string value)
    {
        id = value;
    }
    public string GetName()
    {
        return name;
    }

    private void SetName(string value)
    {
        name = value;
    }


    public double RemoveAmount(double removeAmount)
    {        
        if (removeAmount < GetAmount())
        {
            var time = GameManager.instance.data.date.time;
            if (previousTime == 0)
            {
                previousTime = GameManager.instance.data.date.time;
                previousAmount = GetAmount();
            }
            else if (time - previousTime > Dated.Hour)
            {
                var changeAmount = previousAmount - GetAmount() + removeAmount;
                var changeTime = GameManager.instance.data.date.time - previousTime;
                previousTime = time;
                previousAmount = GetAmount();
                if (changeTime == 0)
                {
                    changeTime = 1;
                    changeAmount = 0;
                    Debug.Log("Change time == 0");
                }
                rate = changeAmount / changeTime;
                SetTimeUntilDepleted(( (GetAmount() - removeAmount) / rate));
            }
            SetAmount(GetAmount() - removeAmount);
        }
        else
        {
            removeAmount = GetAmount();
            SetAmount(0);
        }

        return removeAmount;
    }

    public void AddAmount(double amount)
    {
        this.SetAmount(this.GetAmount() + amount);
    }

    public string GetInf()
    {
        return string.Format("Resource Name: {0}\nResource Amount: {1} kg\nResource:Accesibility: {2}",
            GetName(),
            GetAmount(),
            GetAccessibility());
    }

    //Operators
    public static RawResource operator *(RawResource item, int num)
    {
        item.SetAmount(item.GetAmount() * num);
        return item;
    }
    public static RawResource operator /(RawResource item, int num)
    {
        item.SetAmount(item.GetAmount() / num);
        return item;
    }
    public static RawResource operator +(RawResource item, int num)
    {
        item.SetAmount(item.GetAmount() + num);
        return item;
    }
    public static RawResource operator -(RawResource item, int num)
    {
        item.SetAmount(item.GetAmount() - num);
        if (item.GetAmount() < 0)
        {
            throw new System.Exception("Amount bellow 0");
        }
        return item;
    }
}

public class RawResourceBlueprint
{
    public string name;
    public string description;
    public List<RawResourceInfo> rawResourceInfos { get; private set; }
    public string id { get; private set; }
    public double miningTime;
    public float mass;
    public float armor;
    public double estimateValue;

    public RawResourceBlueprint() { }
    public RawResourceBlueprint(string _name, string _id, string _description, float _mass, float _armor, double _miningTime, List<RawResourceInfo> resourcesInfo)
    {
        name = _name;
        description = _description;
        rawResourceInfos = resourcesInfo;
        mass = _mass;
        armor = _armor;
        id = _id;
        miningTime = _miningTime;
    }

    public RawResourceInfo GetInfo(PlanetTileType planetTile)
    {
        return rawResourceInfos.Find(x => x.planetTileType == planetTile);
    }
}

public struct RawResourceInfo
{
    public PlanetTileType planetTileType;
    public float probability;
    public Vector2 accessibility;
    public int maxAmount;

    public RawResourceInfo(PlanetTileType tile, float _probability, Vector2 _accessibility, int _maxAmount)
    {
        planetTileType = tile;
        probability = _probability;
        accessibility = _accessibility;
        maxAmount = _maxAmount;
    }
}

public enum defaultRawResources
{
    Goldium,
    Armoroid,
    Coppode,
    Fuelodite,
    Warium,
    Limoite,
    Glassitum
}