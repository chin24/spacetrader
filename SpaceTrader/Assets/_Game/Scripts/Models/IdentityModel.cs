﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeControl;

public class IdentityModel : Model {

    public string name;
    public IdentityState identityState = IdentityState.Initial;
    public Color spriteColor;

    public ModelRef<IdentityModel> owner;
    public ModelRefs<IdentityModel> entities;

    public string referenceId { get; set; }
    public List<string> ownedShipIds = new List<string>();
    public List<string> structureIds = new List<string>();
    public List<string> knownSolarBodyIds = new List<string>();

    public List<string> ownedBlueprintIds = new List<string>();

    

    public IdentityModel Owner { get
        {
            if (owner == null)
            {
                return this;
            }
            else
            {
                return owner.Model;
            }
        } }

    public Dated dateCreated { get; set; }//new Dated(GameManager.instance.data.date.time);
    public double lastUpdated;
    public double deltaTime;
    //Research and Knowledge Tracking
    /// <summary>
    /// Research title and amount of knowledge held within the company.
    /// </summary>
    public Dictionary<string, double> research;


    public GovernmentModel Government
    {
        get
        {
            if (GetType() == typeof(GovernmentModel))
            {
                return this as GovernmentModel;
            }
            else if (GetType() == typeof(CompanyModel))
            {
                return ((CompanyModel)this).governmentAccess[0];
            }
            else
            {
                return owner.Model.Government;
            }
        }
    }

    public List<string> contracts = new List<string>();

    /// <summary>
    /// Used to update money at a given interval;
    /// </summary>
    public double timeUpdate;

    //----------------Money-----------------------------//

    public MoneyBudget money = MoneyBudget.StandardBuget;

    public double assets = 0; //TODO: Calculate the current market cash value of all structures, ships, etc.
    public double MoneyTotal { get { return money.moneyTotal; } }

    public IdentityModel(IdentityModel _owner = null)
    {
        dateCreated = new Dated(GameManager.instance.data.date.time);
        lastUpdated = GameManager.instance.data.date.time;
        spriteColor = Random.ColorHSV(.5f, 1f, .5f, 1f);
        spriteColor.a = 1;

        if (_owner != null)
        {
            owner = new ModelRef<IdentityModel>(_owner);
            if (_owner.entities == null)
            {
                _owner.entities = new ModelRefs<IdentityModel>();
            }
            _owner.entities.Add(this);
        }
    }

    public virtual void Update()
    {
        deltaTime = GameManager.instance.data.date.time - lastUpdated;
        lastUpdated = GameManager.instance.data.date.time;
        money.UpdateMoney();

        foreach (string structureId in structureIds)
        {
            var owned = GameManager.instance.locations[structureId] as Structure;

            owned.Update();

            if ((owned).DeleteStructure)
            {
                owned.GetReferenceBody().Structures.Remove(owned);
                owned.GetReferenceBody().PositionEntityIds.Remove(owned.GetId());
                structureIds.Remove(structureId);
                GameManager.instance.locations.Remove(owned.GetId());
                break;
            }
        }
    }

    
    public List<Ship> GetOwnedShips()
    {
        var ships = new List<Ship>();
        foreach (string shipId in ownedShipIds)
        {
            ships.Add(GameManager.instance.data.ships.Find(x => x.GetId() == shipId));
        }
        return ships;
    }

    public List<Structure> GetStructures()
    {
        var structure = new List<Structure>();
        foreach (string item in structureIds)
        {
            structure.Add(GameManager.instance.locations[item] as Structure);
        }
        return structure;
    }
    public List<Contract> GetContracts()
    {
        List<Contract> contracts = new List<Contract>();
        foreach (string contractId in this.contracts)
        {
            contracts.Add(GameManager.instance.contracts[contractId]);
        }
        return contracts;
    }

    public List<ItemBlueprint> GetOwnedBlueprints()
    {
        List<ItemBlueprint> list = new List<ItemBlueprint>();
        foreach (string id in this.ownedBlueprintIds)
        {
            list.Add(GameManager.instance.data.blueprintsModel.Model.GetItem(id));
        }
        return list;
    }

    public List<Contract> GetConstructionContracts()
    {
        List<Contract> contracts = new List<Contract>();
        foreach (Structure structure in GetStructures())
        {
            SupplyStructure productionStructure = structure as SupplyStructure;
            if (productionStructure != null)
            {
                if (productionStructure.structureUpgradeContractId != null)
                {
                    contracts.Add(GameManager.instance.contracts[productionStructure.structureUpgradeContractId]);
                }
            }
            
        }
        return contracts;
    }
    public void SetLocation(string _referenceId)
    {
        referenceId = _referenceId;
        AddKnownSolarBodyId(referenceId);
    }

    public void AddStructure(string structureId, string referenceId)
    {
        if (!structureIds.Contains(structureId))
        {
            structureIds.Add(structureId);
        }
        AddKnownSolarBodyId(referenceId);
    }

    public void AddKnownSolarBodyId(string body)
    {
        if (!knownSolarBodyIds.Contains(body) && body != null && body.Contains("SolarBody"))
        {
            knownSolarBodyIds.Add(body);
        }
        
    }



    //Deals with Company Aquisition and Management
    public void AddSubEntity(IdentityModel identity)
    {
        identity.owner = new ModelRef<IdentityModel>(this);
        if (this.entities == null)
            entities = new ModelRefs<IdentityModel>();
        entities.Add(identity);

    }
}



public enum IdentityState
{
    Initial,
    Explore,
    Grow
}
