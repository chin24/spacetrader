﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeControl;

public class GovernmentModel : IdentityModel {

    public List<string> leaders = new List<string>();
    public List<string> starsId = new List<string>();
    public ModelRefs<CompanyModel> localCompanies = new ModelRefs<CompanyModel>();
    public ModelRefs<CompanyModel> licensedCompanies = new ModelRefs<CompanyModel>();
    public ModelRefs<CompanyModel> knownCompanies = new ModelRefs<CompanyModel>();
    public ModelRefs<CompanyModel> bannedCompanies = new ModelRefs<CompanyModel>();
    public ModelRefs<GovernmentModel> enemyGovernments = new ModelRefs<GovernmentModel>();
    public ModelRefs<GovernmentModel> alliedGovernments = new ModelRefs<GovernmentModel>();
    public ModelRefs<GovernmentModel> knownGovernments = new ModelRefs<GovernmentModel>();
    public List<string> postedContractIds = new List<string>();
    public List<string> allBlueprintIds = new List<string>();

    //----------------Commerce-----------------------------//
    public Dictionary<string, double> rawResourcesAmount = new Dictionary<string, double>();
    public GovernmentModel() {
    }

    public GovernmentModel(string _name)
    {
        name = _name;
        money.StartingBalance(100000000);
        GameManager.instance.data.governments.Add(this);

        var body = FindGovernmentStar(this);
        //TODO: create solar and plantary ownership

        this.referenceId = body.GetId();

        body.Population(1);
        body.companies.Add(this);

        Creature leader = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, body.GetId(), GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);
        leaders.Add(leader.GetId());

        //Create company to manage blueprints related to the research
        Creature manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, body.GetId(), GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);
        //Create Ship Companies
        new CompanyModel(name + " Cargo Ship Branch", "Cargo Ship", body, this, manager, this);
        manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, body.GetId(), GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);
        new CompanyModel(name + " Combat Ship Branch", "Combat Ship", body, this, manager, this);
        manager = new Creature(GameManager.instance.names.GenerateMaleFirstName() + " " + GameManager.instance.names.GenerateWorldName(), 100000, body.GetId(), GameManager.instance.data.date, new Dated(30 * Dated.Year), CreatureType.Human);
        new CompanyModel(name + " Distribution Center", "Distribution Center", body, this, manager, this);

        new DistributionCenter(this, body);

    }


    public override void Update()
    {
        base.Update();

        foreach (string solarId in knownSolarBodyIds)
        {
            SolarBody solarBody = GameManager.instance.data.getSolarBody(solarId);

            solarBody.GetGamePosition(GameManager.instance.data.date.time);
        }
    }
    /// <summary>
    /// Returns the id of the contract with with the smallest overall cost
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public string GetCheapestContractId(string itemId, string destinationId, double itemAmount)
    {
        var items = postedContractIds.FindAll(x => GameManager.instance.contracts[x].itemId == itemId && GameManager.instance.contracts[x].contractState == ContractState.Initial);

        double totalCost = double.PositiveInfinity;
        string cheapestContractId = "";
        for (int i = 0; i < items.Count; i++)
        {
            Contract contract = GameManager.instance.contracts[items[i]];
            double distance = Vector3d.Distance(GameManager.instance.locations[contract.originId].SystemPosition, GameManager.instance.locations[destinationId].SystemPosition);
            contract.CalculateCost(distance,1, itemAmount); //TODO: take into account item size
            double cost = contract.cost;
            if (cost < totalCost)
            {
                totalCost = cost;
                cheapestContractId = contract.id;
            }
        }

        return cheapestContractId;
    }

    #region "Commerce"

    public List<Contract> GetPostedContracts()
    {
        List<Contract> contracts = new List<Contract>();
        foreach (string contractId in postedContractIds)
        {
            contracts.Add(GameManager.instance.contracts[contractId]);
        }
        return contracts;
    }

    /// <summary>
    /// Finds a space in the galaxy for a new government,and then sets the stars as that government's space
    /// </summary>
    /// <param name="model">The government model to set</param>
    /// <returns></returns>
    private SolarBody FindGovernmentStar(GovernmentModel model)
    {
        for (int index = 0; index < GameManager.instance.data.stars.Count; index++)
        {
            if ( (GameManager.instance.data.stars[index].owner == null || GameManager.instance.data.stars[index].owner.Model == null) && GameManager.instance.data.stars[index].Satelites.Count > 0)
            {
                bool freeSpace = true;
                
                //TODO: add a way to stop governments from spawning too close to each other

                if (freeSpace) //Found spot
                {
                    foreach (SolarBody body in GameManager.instance.data.stars[index].Satelites)
                    {
                        if (body.GetSolarSubType() == SolarSubTypes.EarthLike)
                        {
                            //Check to see if it has some of all raw materials
                            
                            List<RawResourceBlueprint> raws = new List<RawResourceBlueprint>( GameManager.instance.data.rawResources.Model.rawResources);
                            foreach(SolarBody solar in GameManager.instance.data.stars[index].GetAllSolarBodies())
                            {
                                foreach (RawResource raw in solar.GetRawResources())
                                {
                                    raws.RemoveAll( x => x.id == raw.GetId());
                                }
                                if (raws.Count == 0)
                                    break;
                            }
                            if (raws.Count != 0)
                                break;

                            //Government owns all initial bodies and resources in solarsystem;
                            GameManager.instance.data.stars[index].owner = new ModelRef<IdentityModel>(this);

                            rawResourcesAmount = new Dictionary<string, double>();
                            foreach (SolarBody solar in GameManager.instance.data.stars[index].GetAllSolarBodies())
                            {
                                solar.owner = new ModelRef<IdentityModel>(this); 

                                //Give ownership of rawresource to the government
                                foreach (RawResource raw in solar.GetRawResources())
                                {
                                    raw.owner = new ModelRef<IdentityModel>(this);
                                    if (rawResourcesAmount.ContainsKey(raw.GetId()))
                                    {
                                        rawResourcesAmount[raw.GetId()] += raw.GetAmount();
                                    }
                                    else
                                        rawResourcesAmount.Add(raw.GetId(), raw.GetAmount());
                                }
                            }

                            model.starsId.Add(GameManager.instance.data.stars[index].GetId());
                            model.referenceId = body.GetId();
                            body.Population(1);
                            body.companies.Add(model);
                            return body;
                        }

                    }
                }
            }
        }

        throw new System.Exception("Could not find solarsytem with all resources and earthlike planet");
    }

    #endregion
}

