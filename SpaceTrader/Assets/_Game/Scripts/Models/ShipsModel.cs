﻿using UnityEngine;
using System.Collections;
using CodeControl;
using System.Collections.Generic;

public class ShipsModel: Model
{
    public List<Ship> ships { get; private set; }
    public ShipsModel() { ships = new List<Ship>(); }
}
